<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRestaurantIdToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('slides', function (Blueprint $table) {
            $table->bigInteger('restaurant_id')->unsigned()->nullable()->after('id');
            $table->foreign('restaurant_id')->references('id')->on('user')->onDelete('cascade');
            $table->bigInteger('offer_id')->unsigned()->nullable();
            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropForeign('restaurant_id');
            $table->dropColumn('restaurant_id');
            $table->dropForeign('offer_id');
            $table->dropColumn('offer_id');
        });
    }
}
