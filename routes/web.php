<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Nexmo\Laravel\Facade\Nexmo;

Route::get('/', function () {
    return view('land_page');
});

Route::get('/app_terms', function () {
    return view('app_terms');
});
Route::get('/app_privacy', function () {
    return view('app_privacy');
});
Route::get('/contact', function () {
    return view('contact');
})->name('contact_view');

Route::post('/contact_us',[
        'uses' => 'dashboard\SettingController@ContactUs',
        'as'   => 'post.contact_us'
    ]);

Route::group(['prefix' => 'dashboard'], function () {

    Route::get('people/{name}/{surname?}', function ($name, $surname) {
        return 'Hello ' . $name . ' ' . $surname;
    });

    Route::get('logout', 'dashboard\AuthController@logout')->name('dashboard_logout');
    Route::group(['middleware' => 'guest'], function () {
        Route::get('login', 'dashboard\AuthController@login')->name('dashboard_login');
        Route::post('login_post', 'dashboard\AuthController@login_post');
    });
    Route::group(['middleware' => 'admin'], function () {
        Route::get('/', 'dashboard\HomeController@index')->name('dashboard_home');

        Route::group(['prefix' => 'auth'], function () {
            Route::get('profile', 'dashboard\AuthController@profile')->name('admin_profile');
            Route::get('change_password', 'dashboard\AuthController@change_password')->name('change_password');
            Route::post('update', 'dashboard\AuthController@update')->name('update_profile');
            Route::post('update_password', 'dashboard\AuthController@update_password')->name('update_password');
            Route::get('logout', 'dashboard\AuthController@logout')->name('dashboard_logout');
        });

        Route::group(['prefix' => 'setting'], function () {
            Route::get('/', 'dashboard\SettingController@index')->name('setting')->middleware('perms:setting');
            Route::post('update', 'dashboard\SettingController@update')->name('setting_update')->middleware('perms:setting');
            Route::get('pages', 'dashboard\SettingController@pages')->name('setting_pages')->middleware('perms:setting');
        });

        Route::group(['prefix' => 'country'], function () {
            Route::get('/', 'dashboard\CountryController@index')->name('countries')->middleware('perms:countries');
            Route::get('create', 'dashboard\CountryController@create')->name('country_create')->middleware('perms:country_create');
            Route::post('store', 'dashboard\CountryController@store')->name('country_store')->middleware('perms:country_create');
            Route::get('edit/{id?}', 'dashboard\CountryController@edit')->name('country_edit')->middleware('perms:country_edit');
            Route::post('update', 'dashboard\CountryController@update')->name('country_update')->middleware('perms:country_edit');
            Route::get('delete/{id?}', 'dashboard\CountryController@delete')->name('country_delete')->middleware('perms:country_delete');
        });

        Route::group(['prefix' => 'city'], function () {
            Route::get('/', 'dashboard\CityController@index')->name('cities')->middleware('perms:cities');
            Route::get('create', 'dashboard\CityController@create')->name('city_create')->middleware('perms:city_create');
            Route::post('store', 'dashboard\CityController@store')->name('city_store')->middleware('perms:city_create');
            Route::get('edit/{id?}', 'dashboard\CityController@edit')->name('city_edit')->middleware('perms:city_edit');
            Route::post('update', 'dashboard\CityController@update')->name('city_update')->middleware('perms:city_edit');
            Route::get('delete/{id?}', 'dashboard\CityController@delete')->name('city_delete')->middleware('perms:city_delete');
        });

        Route::group(['prefix' => 'administration'], function () {
            // Groups
            Route::get('group', 'dashboard\AdministrationController@group')->name('administration_groups')->middleware('perms:administration');
            Route::get('group/create', 'dashboard\AdministrationController@group_create')->name('administration_group_create')->middleware('perms:administration');
            Route::post('group/store', 'dashboard\AdministrationController@group_store')->name('administration_group_store')->middleware('perms:administration');
            Route::get('group/edit/{id?}', 'dashboard\AdministrationController@group_edit')->name('administration_group_edit')->middleware('perms:administration');
            Route::post('group/update', 'dashboard\AdministrationController@group_update')->name('administration_group_update')->middleware('perms:administration');
            Route::get('group/delete/{id?}', 'dashboard\AdministrationController@group_delete')->name('administration_group_delete')->middleware('perms:administration');
            // Admins
            Route::get('admins', 'dashboard\AdministrationController@admins')->name('admins')->middleware('perms:administration');
            Route::get('admin/create', 'dashboard\AdministrationController@admin_create')->name('admin_create')->middleware('perms:administration');
            Route::post('admin/store', 'dashboard\AdministrationController@admin_store')->name('admin_store')->middleware('perms:administration');
            Route::get('admin/edit/{id?}', 'dashboard\AdministrationController@admin_edit')->name('admin_edit')->middleware('perms:administration');
            Route::post('admin/update', 'dashboard\AdministrationController@admin_update')->name('admin_update')->middleware('perms:administration');
            Route::get('admin/delete/{id?}', 'dashboard\AdministrationController@admin_delete')->name('admin_delete')->middleware('perms:administration');
        });

        Route::group(['prefix' => 'nationality'], function () {
            Route::get('/', 'dashboard\NationalitiesController@index')->name('nationality')->middleware('perms:nationality');
            Route::get('create', 'dashboard\NationalitiesController@create')->name('nationality_create')->middleware('perms:nationality');
            Route::post('store', 'dashboard\NationalitiesController@store')->name('nationality_store')->middleware('perms:nationality');
            Route::get('edit/{id?}', 'dashboard\NationalitiesController@edit')->name('nationality_edit')->middleware('perms:nationality');
            Route::post('update', 'dashboard\NationalitiesController@update')->name('nationality_update')->middleware('perms:nationality');
            Route::get('delete/{id?}', 'dashboard\NationalitiesController@delete')->name('nationality_delete')->middleware('perms:nationality');
        });

        Route::group(['prefix' => 'user'], function () {
            Route::get('/', 'dashboard\UserController@index')->name('users')->middleware('perms:users');
            Route::get('create', 'dashboard\UserController@create')->name('create_user')->middleware('perms:create_user');
            Route::post('store', 'dashboard\UserController@store')->name('user_store')->middleware('perms:create_user');
            Route::get('edit/{id?}', 'dashboard\UserController@edit')->name('user_edit')->middleware('perms:user_edit');
            Route::PUT('update', 'dashboard\UserController@update')->name('user_update')->middleware('perms:user_edit');
            Route::get('delete/{id?}', 'dashboard\UserController@delete')->name('user_delete')->middleware('perms:user_delete');
            Route::get('trashed', 'dashboard\UserController@trashed')->name('user_trashed')->middleware('perms:user_trashed');
            Route::get('restore/{id?}', 'dashboard\UserController@restore')->name('user_restore')->middleware('perms:user_trashed');
            Route::get('force_delete/{id?}', 'dashboard\UserController@force_delete')->name('user_force_delete')->middleware('perms:user_trashed');
            Route::get('profile/{id?}', 'dashboard\UserController@profile')->name('user_profile')->middleware('perms:user_profile');
            Route::post('wallet', 'dashboard\UserController@wallet')->name('user_wallet')->middleware('perms:user_wallet');
            Route::get('live_search_4_driver/{id?}', 'dashboard\UserController@live_search_4_driver')->name('live_search_4_driver')->middleware('perms:user_profile');

        });
            //----------------------- restuatants ------------------------
        Route::group(['prefix' => 'restaurant'], function () {
                Route::get('/', 'dashboard\RestaurantController@index')->name('restaurants')->middleware('perms:restaurants');
                Route::get('create', 'dashboard\RestaurantController@create')->name('create_restaurant')->middleware('perms:create_restaurant');
                Route::post('store', 'dashboard\RestaurantController@store')->name('restaurant_store')->middleware('perms:create_restaurant');
                Route::get('edit/{id?}', 'dashboard\RestaurantController@edit')->name('restaurant_edit')->middleware('perms:restaurant_edit');
                Route::PUT('update', 'dashboard\RestaurantController@update')->name('restaurant_update')->middleware('perms:restaurant_edit');
                Route::get('delete/{id?}', 'dashboard\RestaurantController@delete')->name('restaurant_delete')->middleware('perms:restaurant_delete');
                Route::get('profile/{id?}', 'dashboard\RestaurantController@profile')->name('restaurant_admin_profile')->middleware('perms:restaurant_admin_profile');
            });


        Route::group(['prefix' => 'ad'], function () {
            Route::get('/', 'dashboard\AdController@index')->name('ads')->middleware('perms:ads');
            Route::get('delete/{id?}', 'dashboard\AdController@delete')->name('ad_delete')->middleware('perms:ad_delete');
            Route::get('ad/{id?}', 'dashboard\AdController@ad')->name('ad_details')->middleware('perms:ad_details');
        });

        Route::group(['prefix' => 'bank_account'], function () {
            Route::get('/', 'dashboard\BankAccountController@index')->name('bank_account')->middleware('perms:bank_account');
            Route::get('create', 'dashboard\BankAccountController@create')->name('bank_account_create')->middleware('perms:bank_account_create');
            Route::post('store', 'dashboard\BankAccountController@store')->name('bank_account_store')->middleware('perms:bank_account_create');
            Route::get('edit/{id?}', 'dashboard\BankAccountController@edit')->name('bank_account_edit')->middleware('perms:bank_account_edit');
            Route::post('update', 'dashboard\BankAccountController@update')->name('bank_account_update')->middleware('perms:bank_account_edit');
            Route::get('delete/{id?}', 'dashboard\BankAccountController@delete')->name('bank_account_delete')->middleware('perms:bank_account_delete');
        });

        Route::group(['prefix' => 'contacts'], function () {
            Route::get('/', 'dashboard\UserMessageController@contacts')->name('contacts')->middleware('perms:contacts');
            Route::get('delete/{id?}', 'dashboard\UserMessageController@delete_contact')->name('delete_contact')->middleware('perms:contact_delete');
        });

        Route::group(['prefix' => 'reports'], function () {
            Route::get('/', 'dashboard\SettingController@reports')->name('reports')->middleware('perms:reports');
            Route::get('delete/{id?}', 'dashboard\SettingController@delete_report')->name('delete_report')->middleware('perms:report_delete');
        });



        Route::group(['prefix' => 'category'], function () {
            Route::get('/', 'dashboard\CategoryController@index')->name('categories')->middleware('perms:categories');
            Route::get('create', 'dashboard\CategoryController@create')->name('category_create')->middleware('perms:category_create');
            Route::post('store', 'dashboard\CategoryController@store')->name('category_store')->middleware('perms:category_create');
            Route::get('edit/{id?}', 'dashboard\CategoryController@edit')->name('category_edit')->middleware('perms:category_edit');
            Route::post('update', 'dashboard\CategoryController@update')->name('category_update')->middleware('perms:category_edit');
            Route::get('delete/{id?}', 'dashboard\CategoryController@delete')->name('category_delete')->middleware('perms:category_delete');
        });

        Route::group(['prefix' => 'service'], function () {
            Route::get('/', 'dashboard\ServiceController@index')->name('services')->middleware('perms:services');
            Route::get('create', 'dashboard\ServiceController@create')->name('service_create')->middleware('perms:service_create');
            Route::post('store', 'dashboard\ServiceController@store')->name('service_store')->middleware('perms:service_create');
            Route::get('edit/{id?}', 'dashboard\ServiceController@edit')->name('service_edit')->middleware('perms:service_edit');
            Route::post('update', 'dashboard\ServiceController@update')->name('service_update')->middleware('perms:service_edit');
            Route::get('delete/{id?}', 'dashboard\ServiceController@delete')->name('service_delete')->middleware('perms:service_delete');
        });

        Route::group(['prefix' => 'option'], function () {
            Route::get('/', 'dashboard\OptionController@index')->name('options')->middleware('perms:options');
            Route::get('create', 'dashboard\OptionController@create')->name('option_create')->middleware('perms:option_create');
            Route::post('store', 'dashboard\OptionController@store')->name('option_store')->middleware('perms:option_create');
            Route::get('edit/{id?}', 'dashboard\OptionController@edit')->name('option_edit')->middleware('perms:option_edit');
            Route::post('update', 'dashboard\OptionController@update')->name('option_update')->middleware('perms:option_edit');
            Route::get('delete/{id?}', 'dashboard\OptionController@delete')->name('option_delete')->middleware('perms:option_delete');
        });

        // Route::group(['prefix' => 'type'], function () {
        //     Route::get('/', 'dashboard\TypeController@index')->name('types')->middleware('perms:types');
        //     Route::get('create', 'dashboard\TypeController@create')->name('type_create')->middleware('perms:type_create');
        //     Route::post('store', 'dashboard\TypeController@store')->name('type_store')->middleware('perms:type_create');
        //     Route::get('edit/{id?}', 'dashboard\TypeController@edit')->name('type_edit')->middleware('perms:type_edit');
        //     Route::post('update', 'dashboard\TypeController@update')->name('type_update')->middleware('perms:type_edit');
        //     Route::get('delete/{id?}', 'dashboard\TypeController@delete')->name('type_delete')->middleware('perms:type_delete');
        // });

        Route::group(['prefix' => 'type'], function () {
            Route::get('/', 'dashboard\UserMessageController@index')->name('types')->middleware('perms:types');
            Route::get('create', 'dashboard\UserMessageController@create')->name('type_create')->middleware('perms:type_create');
            Route::post('store', 'dashboard\UserMessageController@store')->name('type_store')->middleware('perms:type_create');
            Route::get('edit/{id?}', 'dashboard\UserMessageController@edit')->name('type_edit')->middleware('perms:type_edit');
            Route::post('update', 'dashboard\UserMessageController@update')->name('type_update')->middleware('perms:type_edit');
            Route::get('delete/{id?}', 'dashboard\UserMessageController@delete')->name('type_delete')->middleware('perms:type_delete');
        });
        

        Route::group(['prefix' => 'car'], function () {
            Route::get('/', 'dashboard\CarController@index')->name('cars')->middleware('perms:cars');
            Route::get('create', 'dashboard\CarController@create')->name('car_create')->middleware('perms:car_create');
            Route::post('store', 'dashboard\CarController@store')->name('car_store')->middleware('perms:car_create');
            Route::get('edit/{id?}', 'dashboard\CarController@edit')->name('car_edit')->middleware('perms:car_edit');
            Route::post('update', 'dashboard\CarController@update')->name('car_update')->middleware('perms:car_edit');
            Route::get('delete/{id?}', 'dashboard\CarController@delete')->name('car_delete')->middleware('perms:car_delete');
        });

        Route::group(['prefix' => 'offer'], function () {
            Route::get('/', 'dashboard\OfferController@index')->name('offers')->middleware('perms:offers');
            Route::get('create', 'dashboard\OfferController@create')->name('offer_create')->middleware('perms:offer_create');
            Route::post('store', 'dashboard\OfferController@store')->name('offer_store')->middleware('perms:offer_create');
            Route::get('edit/{id?}', 'dashboard\OfferController@edit')->name('offer_edit')->middleware('perms:offer_edit');
            Route::post('update', 'dashboard\OfferController@update')->name('offer_update')->middleware('perms:offer_edit');
            Route::get('delete/{id?}', 'dashboard\OfferController@delete')->name('offer_delete')->middleware('perms:offer_delete');
        });

        Route::group(['prefix' => 'news'], function () {
            Route::get('/', 'dashboard\NewsController@index')->name('news')->middleware('perms:news');
            Route::get('create', 'dashboard\NewsController@create')->name('news_create')->middleware('perms:news_create');
            Route::post('store', 'dashboard\NewsController@store')->name('news_store')->middleware('perms:news_create');
            Route::get('edit/{id?}', 'dashboard\NewsController@edit')->name('news_edit')->middleware('perms:news_edit');
            Route::post('update', 'dashboard\NewsController@update')->name('news_update')->middleware('perms:news_edit');
            Route::get('delete/{id?}', 'dashboard\NewsController@delete')->name('news_delete')->middleware('perms:news_delete');
        });
        
        Route::group(['prefix' => 'bunches'], function () {
            Route::get('/', 'dashboard\BunchController@index')->name('bunches')->middleware('perms:bunches');
            Route::get('create', 'dashboard\BunchController@create')->name('bunches_create')->middleware('perms:bunches_create');
            Route::post('store', 'dashboard\BunchController@store')->name('bunches_store')->middleware('perms:bunches_create');
            Route::get('edit/{id?}', 'dashboard\BunchController@edit')->name('bunches_edit')->middleware('perms:bunches_edit');
            Route::post('update', 'dashboard\BunchController@update')->name('bunches_update')->middleware('perms:bunches_edit');
            Route::get('delete/{id?}', 'dashboard\BunchController@delete')->name('bunches_delete')->middleware('perms:bunches_delete');
            Route::get('bunch/{id?}', 'dashboard\BunchController@bunch_view')->name('bunch_view')->middleware('perms:bunch_view');
        });
        
        Route::group(['prefix' => 'experiences'], function () {
            Route::get('/', 'dashboard\ExperienceController@index')->name('experiences')->middleware('perms:experiences');
            Route::get('create', 'dashboard\ExperienceController@create')->name('experience_create')->middleware('perms:experience_create');
            Route::post('store', 'dashboard\ExperienceController@store')->name('experience_store')->middleware('perms:experience_create');
            Route::get('edit/{id?}', 'dashboard\ExperienceController@edit')->name('experience_edit')->middleware('perms:experience_edit');
            Route::post('update', 'dashboard\ExperienceController@update')->name('experience_update')->middleware('perms:experience_edit');
            Route::get('delete/{id?}', 'dashboard\ExperienceController@delete')->name('experience_delete')->middleware('perms:experience_delete');
        });

        Route::group(['prefix' => 'slides'], function () {
            
            Route::get('/', 'dashboard\SlideController@index')->name('slides')->middleware('perms:slides');
            Route::get('create', 'dashboard\SlideController@create')->name('slide_create')->middleware('perms:slide_create');
            Route::post('store', 'dashboard\SlideController@store')->name('slide_store')->middleware('perms:slide_create');
            Route::get('edit/{id?}', 'dashboard\SlideController@edit')->name('slide_edit')->middleware('perms:slide_edit');
            Route::post('update', 'dashboard\SlideController@update')->name('slide_update')->middleware('perms:slide_update');
            Route::get('delete/{id?}', 'dashboard\SlideController@delete')->name('slide_delete')->middleware('perms:slide_delete');
        });

        Route::group(['prefix' => 'dish'], function () {
            Route::get('/', 'dashboard\DishController@index')->name('dishes')->middleware('perms:dishes');
            Route::get('dish/{id}', 'dashboard\DishController@show')->name('dishes_view')->middleware('perms:dishes_view');
            Route::get('delete/{id?}', 'dashboard\DishController@delete')->name('dishes_delete')->middleware('perms:dishes_delete');
        });

        Route::group(['prefix' => 'reservations'], function () {
            Route::get('/', 'dashboard\ReservationController@index')->name('reservations')->middleware('perms:reservations');
            Route::get('reservation/{id}', 'dashboard\ReservationController@show')->name('reservation_view')->middleware('perms:reservation_view');
            Route::get('delete/{id?}', 'dashboard\ReservationController@delete')->name('reservation_delete')->middleware('perms:reservation_delete');
        });

        

    });
});

Route::group(['prefix' => 'restaurant'], function () {

    Route::get('people/{name}/{surname?}', function ($name, $surname) {
        return 'Hello ' . $name . ' ' . $surname;
    });

    Route::get('logout', 'restaurant\AuthController@logout')->name('restaurant_logout');
//    Route::group(['middleware' => 'guest'], function () {
        Route::get('login', 'restaurant\AuthController@login')->name('restaurant_login');
        Route::post('login_post', 'restaurant\AuthController@login_restaurant_post')->name('login_restaurant_post');
//    });


    Route::group(['middleware' => 'restaurant'], function () {
        Route::get('/', 'restaurant\HomeController@index')->name('restaurant_home');

        Route::group(['prefix' => 'auth'], function () {
            Route::get('profile', 'restaurant\AuthController@profile')->name('restaurant_profile');
            Route::get('change_password', 'restaurant\AuthController@change_password')->name('change_restaurant_password');
            Route::post('update', 'restaurant\AuthController@update')->name('update_restaurant_profile');
            Route::post('update_password', 'restaurant\AuthController@update_password')->name('restaurant_password');
            Route::get('logout', 'restaurant\AuthController@logout')->name('restaurant_logout');
        });

        Route::group(['prefix' => 'dishes'], function () {
            Route::get('/', 'restaurant\DishController@index')->name('restaurant_dishes');
            Route::get('create', 'restaurant\DishController@create')->name('restaurant_dishes_create');
            Route::post('store', 'restaurant\DishController@store')->name('restaurant_dishes_store');
            Route::get('edit/{id?}', 'restaurant\DishController@edit')->name('restaurant_dishes_edit');
            Route::post('update', 'restaurant\DishController@update')->name('restaurant_dishes_update');
            Route::get('delete/{id?}', 'restaurant\DishController@delete')->name('restaurant_dishes_delete');
            Route::get('dish/{id}', 'restaurant\DishController@show')->name('restaurant_dishes_view');
        });



        Route::group(['prefix' => 'offer'], function () {
            Route::get('/', 'restaurant\OfferController@index')->name('restaurant_offers');
            Route::get('create', 'restaurant\OfferController@create')->name('restaurant_offer_create');
            Route::post('store', 'restaurant\OfferController@store')->name('restaurant_offer_store');
            Route::get('edit/{id?}', 'restaurant\OfferController@edit')->name('restaurant_offer_edit');
            Route::post('update', 'restaurant\OfferController@update')->name('restaurant_offer_update');
            Route::get('delete/{id?}', 'restaurant\OfferController@delete')->name('restaurant_offer_delete');
        });

        Route::group(['prefix' => 'reservations'], function () {
            Route::get('/', 'restaurant\ReservationController@index')->name('restaurant_reservations');
            Route::get('reservation/{id}', 'restaurant\ReservationController@show')->name('restaurant_reservation_view');
            Route::get('delete/{id?}', 'restaurant\ReservationController@delete')->name('restaurant_reservation_delete');
            Route::post('update', 'restaurant\ReservationController@update')->name('restaurant_reservation_update');
        });
    });
});
