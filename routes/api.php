<?php

use Illuminate\Http\Request;


//    ----------------------Home--------------------------
    Route::any('restaurant/home', 'HomeController@RestaurantHome');
    Route::any('user/home', 'HomeController@UserHome');
//=============== for auth route========================
Route::any('social_login', 'AuthController@social');
Route::any('user_signUp', 'AuthController@signUpUser');
Route::any('restaurant_signUp', 'AuthController@signUpRestaurant');
Route::any('restaurant_signUp_ios', 'AuthController@signUpRestaurantIos');
Route::any('signin', 'AuthController@signin');
Route::any('active_account', 'AuthController@ActiveAccount');
Route::any('cities', 'AuthController@cities');
Route::any('categories', 'AuthController@categories');
Route::any('options', 'AuthController@options');
Route::any('payments', 'AuthController@payments');
Route::any('bunches', 'AuthController@bunches');




Route::any('home', 'HomeController@Home');
Route::any('privacy', 'SettingsController@Privacy');
Route::any('terms', 'SettingsController@Terms');
Route::any('aboutApp', 'SettingsController@aboutApp');
Route::any('contacts', 'SettingsController@ContactUs');
Route::any('banks', 'BankController@getBanks');
//    -------------contacts====================
Route::any('contact_us', 'ContactController@contact_us');
Route::any('types', 'ContactController@types');
//================   suggestion ========================
Route::any('store/suggestion', 'SuggestionController@store');
Route::any('show/suggestions', 'SuggestionController@show');

//================  experience_level ===================
Route::any('experience_level', 'RateController@experiences_level');

//================restaurnt data========================

Route::any('restaurant', 'RestaurantController@Restaurant');
Route::any('all_restaurants_with_offers', 'HomeController@all_restaurants_with_offers');
Route::any('all_restaurants_with_out_offers', 'HomeController@all_restaurants_with_out_offers');
Route::any('search', 'SearchController@search');
Route::any('filter', 'SearchController@filter');
Route::any('filter_ios', 'SearchController@filter_ios');
Route::any('menu', 'RestaurantController@Menu');
Route::any('rates', 'RestaurantController@Rates');

Route::any('forgot_password', 'AuthController@postForgotPassword');
Route::any('change_password', 'AuthController@postChangePassword');


Route::group(['middleware' => ['jwt.auth']], function () {


    Route::any('user/profile', 'api\UserControllerController@profile');
    Route::any('user/update', 'UserControllerController@updateProfile');
    Route::any('user/update/ios', 'UserControllerController@updateProfileIos');
    Route::any('logout', 'AuthController@Logout');

//    -------------------

//    --------------------------
    Route::any('like', 'FavController@like');
    Route::any('MyLikes', 'FavController@getMyLikes');
//    ---------------------
    Route::any('store/reservation', 'ReservationController@store');
    Route::any('reservations', 'ReservationController@reservations');
    Route::any('reservation/details', 'ReservationController@reservationDetails');
    Route::any('rejected/reservations', 'ReservationController@RejectedReservations');
    Route::any('accepted/reservations', 'ReservationController@AcceptedReservations');
    Route::any('finished/reservations', 'ReservationController@finishedReservations');


    // ----------------CLIENT REERVATIONS----------
    Route::any('client/reservations', 'ReservationController@clientReservations');
    Route::any('client/rejected/reservations', 'ReservationController@clientRejectedReservations');
    Route::any('client/accepted/reservations', 'ReservationController@clientAcceptedReservations');
    Route::any('client/finished/reservations', 'ReservationController@clientFinishedReservations');
    //---------------------------------------------

    Route::post('accept/reservation', 'ReservationController@accept');
    Route::post('reject/reservation', 'ReservationController@reject');
    Route::post('cancel/reservation', 'ReservationController@cancel_reservation');
    Route::post('finish/reservation', 'ReservationController@finishByProvider');

    Route::any('notification/my_notifications', 'NotificationController@my_notifications');
    Route::any('notification/delete_notification', 'NotificationController@delete_notifications');


//    -------------dishes====================
    Route::any('store/dish', 'DishController@store');
    Route::any('dish', 'DishController@Dish');
    Route::any('update/dish', 'DishController@DishUpdate');
    Route::any('delete/dish', 'DishController@DishDelete');

//    -------------offers====================
    Route::any('store/offer', 'OfferController@store');
    Route::any('offers', 'OfferController@offers');
    Route::any('update/offer', 'OfferController@OfferUpdate');
    Route::any('delete/offer', 'OfferController@OfferDelete');
    Route::any('offer', 'OfferController@Offer');
    Route::post('AppRate', 'RateController@AppRate');
    Route::post('restaurant/rate', 'RateController@RestaurantRate');
    Route::get('my/rates', 'RateController@MyRates');
    Route::get('my/restaurant/rates', 'RateController@MyRestaurantRates');

});
