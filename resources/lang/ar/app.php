<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Language Lines
    |--------------------------------------------------------------------------
 */

    'auth' => [
        'wrong_code' => 'الكود المدخل خاطئ',
        'code_message' => 'كود التحقق الخاص بك هو ',
        'user_not_found' => 'هذا المستخدم غير موجود بالنظام',
        'wrong_code' => 'الكود المدخل خاطئ',
    ],

    'required' => [
        'code_required' => 'كود التفعيل مطلوب',
        'mobile_required' => 'رقم الجوال مطلوب',
        'name_required' => 'الاسم مطلوب',
        'new_password_required' => 'كلمة المرور الجديدة مطلوبة',
        'device_type_required' => 'device_type لم تقم بإرسال',
    ],

    'user' => [
        'user_id_required' => 'user_id مطلوب',
        'user_not_found' => 'هذا المستخدم غير موجود بالنظام',
    ],

    'post' => [
        'post_id_required' => 'post_id مطلوب',
        'post_not_found' => 'هذا المنشور غير موجود بالنظام',
    ],

    'hashtag' => [
        'hashtag_id_required' => 'hashtag_id مطلوب',
        'hashtag_not_found' => 'هذا الهاشتاج غير موجود بالنظام',
    ],

    'comment' => [
        'comment_id_required' => 'comment_id مطلوب',
        'comment_not_found' => 'هذا التعليق غير موجود بالنظام',
    ],

    'chat' => [
        'deleted_file' => 'ملف محذوف',
        'attachment_picture' => 'مرفق صورة',
        'attachment_voice' => 'مرفق ملف صوتي',
    ],

    'messages' => [
        'success_login' => 'تم تسجيل الدخول بنجاح',
        'failed_login' => 'بيانات تسجيل دخول خاطئة',
        'banned_message' => 'تم حظرك من قبل الادارة',
        'deactivation_message' => 'لم تقوم بتفعيل جوالك حتى الان',
        'success_register' => 'تم تسجيل المستخدم بنجاح .',
        'success_logout' => 'تم تسجيل الخروج بنجاح',
        'added_successfully' => 'تمت الإضافة بنجاح',
        'updated_successfully' => 'تمت التعديل بنجاح',
        'deleted_successfully' => 'تم الحذف بنجاح',
        'activated_successfully' => 'تم التفعيل بنجاح',
        'sent_successfully' => 'تم الارسال بنجاح',
        'not_allowed_to_put_services_from_different_providers_or_categories' => 'غير مسموح لك بوضع خدمات من اسرتين مختلفتين أو من قسمين مختلفين في نفس الطلب',
        'not_allowed_to_modify' => 'غير مسموح لك بتعديل البيانات',
        'not_allowed_to_delete' => 'غير مسموح لك بحذف البيانات',
        'something_went_wrong_please_try_again' => 'حدث خطأ ما. أعد المحاولة من فضلك',
        'banned_account' => 'حساب محظور',
        'deactivated_account' => 'حساب غير مفعل',
        'contactSent' => 'تم ارسال رسالتك بنجاح',
        'AdDeleted' => 'تم حذف الاعلان بنجاح',
        'rate' => 'تم ارسال  التقييم  بنجاح',
        'DishAdded' => 'تم اضافه الطبق بنجاح',
        'DishDeleted' => 'تم حذف الطبق ',
        'DishUpdated' => 'تم تعديل بيانات الطبق ',
        'SuccessfullyReserved' => 'تم الحجز بنجاح',
        'ReservationAccepted'=>'تم قبول طلب الحجز بنجاح',
        'ReservationRejected'=>'تم رفض طلب الحجز',
        'OfferAdded'=>'تم اضافه العرض بنجاح',
        'OfferUpdated'=>'تم تعديل العرض بنجاح',
        'OfferDeleted'=>'تم حذف العرض بنجاح',
        'wait_admin_to_active_account'=>'تم اضافه بياناتك بنجاح وبانظار موافقه الاداره',
        'MaxReservationsNum'=>'لقد قمت بالحد الاقصى من الحجوزات اليوم',
        'LastReservations'=>'لا يمكنك الحجز الان يرجى الانتظار 30 دقيقه على الاقل',
        'CanceledFromClient'=>'تم الغاء الحجز',
        'ReservationFinished'=>'تم انهاء الحجز',
    ],
];
