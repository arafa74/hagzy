<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Language Lines
    |--------------------------------------------------------------------------    
 */

    'auth' => [                
        'wrong_code' => 'The input code is wrong',
        'code_message' => 'Your activation code is ',
        'user_not_found' => 'this user not found on system',
        'wrong_code' => 'The input code is wrong',
    ],

    'required' => [
        'code_required' => 'the activation code is required',
        'mobile_required' => 'mobile is required',
        'name_required' => 'name is required',
        'new_password_required' => 'new password is required',
        'device_type_required' => 'device type is required',
    ],

    'user' => [
        'user_id_required' => 'user_id required',
        'receiver_id_required' => 'receiver_id required',
        'user_not_found' => 'this user not found on system',
    ],

    'post' => [
        'post_id_required' => 'post_id required',
        'post_not_found' => 'this post not found on system',
    ],

    'hashtag' => [
        'hashtag_id_required' => 'hashtag_id required',
        'hashtag_not_found' => 'this hashtag not found on system',
    ],

    'comment' => [
        'comment_id_required' => 'comment_id required',
        'comment_not_found' => 'this comment not found on system',
    ],

    'chat' => [
        'deleted_file' => 'Deleted file',
        'attachment_picture' => 'attachment picture',
        'attachment_voice' => 'attachment voice',
    ],

    'messages' => [
        'success_login' => ' Signin Successfully',
        'failed_login' => ' Login Failed',
        'banned_message' => 'Your Account Has Banned By Admin',
        'deactivation_message' => 'Your Mobile Not Activated Yet',
        'success_register' => ' User Registered Successfully',
        'success_logout' => ' Signout Successfully',
        'added_successfully' => 'Added successfully',
        'updated_successfully' => 'Updated successfully',
        'deleted_successfully' => 'Deleted successfully',
        'activated_successfully' => 'Activated successfully',
        'sent_successfully' => 'Sent successfully',
        'not_allowed_to_put_services_from_different_providers_or_categories' => 'You are not allowed to put services from other providers or categories in the same order',
        'not_allowed_to_modify' => 'You are not allowed to modify the data',
        'not_allowed_to_delete' => 'You are not allowed to delete the data',
        'something_went_wrong_please_try_again' => 'Something went wrong. Please try again',
        'banned_account' => 'Banned Account',
        'deactivated_account' => 'Deactivated Account',
        'contactSent' => 'your message sent successfully',
        'AdDeleted' => 'your advertise deleted successfully',
        'rate' => 'rate added successfully',
        'DishAdded' => 'Dish Added successfully',
        'DishDeleted' => 'Dish Deleted Successfully',
        'DishUpdated' => 'Dish Updated Successfully',
        'SuccessfullyReserved' => 'reservation Added successfully',
        'ReservationAccepted'=>'reservation accepted successfully',
        'ReservationRejected'=>'reservation rejected successfully',
        'OfferAdded'=>'offer added successfully',
        'OfferUpdated'=>'offer updated successfully',
        'OfferDeleted'=>'offer deleted successfully',
        'wait_admin_to_active_account'=>'added successfully and wait admin to active account',
        'MaxReservationsNum'=>'you made maximun number of reservations this day',
        'LastReservations'=>'your next reservation should be after 30 minutes',
        'CanceledFromClient'=>'you cancel your reservation ',
        'ReservationFinished'=>'reservation finished  ',
    ],
];
