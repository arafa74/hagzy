@extends('site.parts.layout')
@section('content')
<!-- start page content -->
<section class="page-content">
    <!-- start login -->
    <div class="login-block">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-login wow fadeIn" data-wow-duration="2s">
                        <h3 class="tilte">
                            {{ trans('site.Hello') }}
                        </h3>
                        <h3 class="tilte">
                            <span class="title-icon"><img src="{{ url('public/site_assets') }}/images/icon/car.png"></span>
                            {{ trans('site.SignUp') }}
                        </h3>
                        <div id="alert-not-found" class="alert alert-danger hide-register-alert ">
                            <ul class="list-unstyled">

                            </ul>
                        </div>
                        <form action="" method="post" id="registerForm" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="first-row">
                                <div class="input-block">
                                    <input type="text" name="full_name" placeholder="{{ trans('site.name') }}" class="form-inpt" required="">
                                </div>
                                <div class="input-block">
                                    <input type="number" name="mobile" placeholder="{{ trans('site.mobile') }}" class="form-inpt" required="">
                                </div>
                            </div>
                            <div class="input-block">
                                <input type="email" name="email" placeholder="{{ trans('site.email') }}" class="form-inpt" required="">
                            </div>
                            <div class="input-block">
                                <input type="password" name="password" placeholder="{{ trans('site.password') }}" class="form-inpt input-password"
                                       required="">
                                <i class="fas fa-eye img-eyes1"></i>
                            </div>
                            <div class="input-block">
                                <input type="password" name="confirm_password" placeholder="{{ trans('site.confirm_password') }}"
                                       class="form-inpt input-password" required="">
                                <i class="fas fa-eye img-eyes1"></i>
                            </div>
                            <button type="submit" class="btn">{{ trans('site.SignUp') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="right-img wow fadeIn" data-wow-duration="2s">
            <img src="{{ url('public/site_assets') }}/images/bg/01.png" alt="lzoom img">
        </div>
    </div>
    <!-- end about -->
</section>
<!-- end page content -->
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $(".hide-register-alert").css('display', 'none');

            $("#registerForm").on('submit', function (e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                e.preventDefault();
                $.ajax({
                    url: '{{ route('SignUp') }}',
                    type: 'POST',
                    data: $(this).serialize(),
                    success: function (data) {
                        if ($.isEmptyObject(data.error)) {
                            window.location.href = "{{ route('home')  }}";
                        } else {
                            console.log(data.error);
                            printErrorMsg(data.error);
                        }
                    }
                });

            });

            function printErrorMsg(msg) {
                $(".hide-register-alert").css('display', 'block');
                $(".hide-register-alert").find("ul").html('');
                $.each(msg, function (key, value) {
                    $(".hide-register-alert").find("ul").append('<li>' + value + '</li>');
                });
                $(".hide-register-alert").delay(3000).fadeOut();
            }
        });
    </script>
@endsection