<!doctype html>
<html lang="ar" dir="rtl">

<head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- CSS -->
    <link rel="stylesheet" href="{{ url('public/site_assets') }}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('public/site_assets') }}/css/bootstrap-rtl.min.css">
    <link rel="stylesheet" href="{{ url('public/site_assets') }}/css/slick.css">
    <link rel="stylesheet" href="{{ url('public/site_assets') }}/css/slick-theme.css">
    <link rel="stylesheet" href="{{ url('public/site_assets') }}/css/select2.min.css">
    <link rel="stylesheet" href="{{ url('public/site_assets') }}/css/animate.min.css">
    <link rel="stylesheet" href="{{ url('public/site_assets') }}/css/jquery.fancybox.min.css">
    {{--<link rel="stylesheet" href="{{ url('public/site_assets') }}/css/style.css">--}}
    @if(\Illuminate\Support\Facades\App::isLocale('en'))

        <link rel="stylesheet" href="{{ url('public/site_assets') }}/css/style-ltr.css">

    @else

        <link rel="stylesheet" href="{{ url('public/site_assets') }}/css/style.css">

    @endif

    <link rel="shortcut icon" href="{{ url('public/site_assets') }}/images/fav.png" type="image/x-icon">
    <title>{{ trans('site.SiteName') }}</title>

    <!--[if lt IE 9]>
    <script src="{{ url('public/site_assets') }}/js/html5shiv.min.js"></script>
    <script src="{{ url('public/site_assets') }}/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div id="preloader-first">
    <div class="preloader-first">
        <div class="loader-first">
            <div class="dot-first"></div>
            <div class="dot-first"></div>
            <div class="dot-first"></div>
            <div class="dot-first"></div>
            <div class="dot-first"></div>
        </div>
    </div>
</div>
<!-- start header -->
<div class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="right-side">
                    <a href="{{ route('home') }}">
                        <img src="{{ url('public/site_assets') }}/images/logo.png" alt="logo" class="img-fluid">
                    </a>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="left-side">
                    <div class="accordion" id="accordionExample">
                        <div class="accortion-block">
                            @php
                                $localLang  = \Illuminate\Support\Facades\App::getLocale();
                            @endphp
                            @if($localLang == 'en')
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                <img src="{{ url('public/site_assets') }}/images/icon/us-flag.png" alt="lang-icon">
                                <span>
                                         English
                                </span>
                                <span class="down">
                                        <i class="fas fa-chevron-down"></i>
                                    </span>
                            </button>
                            @else
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">
                                    <img src="{{ url('public/site_assets') }}/images/icon/ar.png" alt="lang-icon">
                                    <span>
                                        العربيه
                                </span>
                                    <span class="down">
                                        <i class="fas fa-chevron-down"></i>
                                    </span>
                                </button>

                            @endif
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                 data-parent="#accordionExample">
                                <div class="accortion-content">
                                    <ul class="list-unstyled">
                                        <li>
                                            @php
                                                $localLangEN  = \Illuminate\Support\Facades\App::isLocale('ar');
                                            @endphp
                                            @if($localLangEN)
                                                <a href="{{url('lang/en')}}" class="btn btn-link center" data-toggle="collapse"
                                                   data-target="#collapseOne" aria-expanded="true"
                                                   aria-controls="collapseOne">
                                                    <span>English</span>
                                                </a>
                                            @else
                                                <a href="{{url('lang/ar')}}" class="btn btn-link center" data-toggle="collapse"
                                                   data-target="#collapseOne" aria-expanded="true"
                                                   aria-controls="collapseOne">
                                                    <span>العربية</span>
                                                </a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="accortion-block">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                    aria-expanded="false" aria-controls="collapseTwo">
                                <img src="{{ url('public/site_assets') }}/images/icon/user.png" alt="user-icon" class="user-icon">
                                <span>
                                @if(!Auth::check())
                                    {{ trans('site.LoginRegister') }}
                                @else
                                    {{ trans('site.Profile') }}
                                @endif

                                </span>
                                <span class="down">
                                        <i class="fas fa-chevron-down"></i>
                                    </span>
                            </button>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                 data-parent="#accordionExample">
                                <div class="accortion-content">
                                    <ul class="list-unstyled">
                                        @if(Auth::check())
                                            <!-- after login -->
                                                <li>
                                                    <a class="btn btn-link center" data-toggle="collapse"
                                                       data-target="#collapseOne" aria-expanded="true"
                                                       aria-controls="collapseOne" href="{{ url('update_profile') }}">
                                                        <span>{{ trans('site.Profile') }}</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="btn btn-link center" data-toggle="collapse"
                                                       data-target="#collapseOne" aria-expanded="true"
                                                       aria-controls="collapseOne" href="{{ url('site/logout') }}">
                                                        <span>{{ trans('site.Logout') }}</span>
                                                    </a>
                                                </li>

                                        @else
                                                <li>
                                                    <a class="btn btn-link center" data-toggle="collapse"
                                                       data-target="#collapseOne" aria-expanded="true"
                                                       aria-controls="collapseOne" href="{{ route('sign_up') }}">
                                                        <span>{{ trans('site.create_account') }}</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="btn btn-link center" data-toggle="collapse"
                                                       data-target="#collapseOne" aria-expanded="true"
                                                       aria-controls="collapseOne" href="{{ route('sign_in') }}">
                                                        <span> {{ trans('site.login') }}</span>
                                                    </a>
                                                </li>
                                       @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-menu">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{ url('public/site_assets') }}/images/logo.png" alt="logo">
            </a>
            <button class="navbar-toggler menu-wrapper" type="button" data-toggle="collapse"
                    data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="hamburger-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item @if(Route::currentRouteName()=== 'home') active @endif">
                        <a class="nav-link" href="{{ route('home') }}"> {{ trans('site.Home') }}
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>

                    <li class="nav-item @if(Route::currentRouteName()=== 'marks') active @endif">
                        <a class="nav-link" href="{{ url('marks') }}">{{ trans('site.CarMarks') }}</a>
                    </li>

                    <li class="nav-item @if(Route::currentRouteName()=== 'gallery') active @endif">
                        <a class="nav-link" href="{{ url('gallery') }}">{{ trans('site.CarShowroom') }}</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#">{{ trans('site.Harag') }}</a>
                    </li>

                    <li class="nav-item @if(Route::currentRouteName()=== 'contact_us') active @endif">
                        <a class="nav-link" href="{{ url('ContactUs') }}">{{ trans('site.ContactUs') }}</a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
</div>
<!-- end header -->

@yield('content')

<!-- start footer -->
<section class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="right-block">
                    <div class="main-links">
                        <h3 class="tilte">
                            <span class="title-icon"><img src="{{ url('public/site_assets') }}/images/icon/car.png"></span>
                            {{ trans('site.main_links') }}
                        </h3>
                        <ul class="list-unstyled">
                            <li><a href="{{ route('home') }}" class="wow fadeInUp">{{ trans('site.Home') }}</a></li>
                            <li><a href="{{ url('marks') }}" class="wow fadeInUp">{{ trans('site.CarMarks') }}
                                    </a></li>

                            <li><a href="#" class="wow fadeInUp">{{ trans('site.Harag') }}</a></li>
                            <li><a href="{{ url('ContactUs') }}" class="wow fadeInUp">{{ trans('site.ContactUs') }}</a></li>
                        </ul>
                    </div>
                    <div class=" secondary-link">
                        <h3 class="tilte">
                            <span class="title-icon"><img src="{{ url('public/site_assets') }}/images/icon/car.png"></span>
                            {{ trans('site.other_links') }}
                        </h3>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('/about/us') }}" class="wow fadeInUp">{{ trans('site.About') }}</a></li>
                            <li><a href="{{ url('/privacy') }}" class="wow fadeInUp">{{ trans('site.Privacy') }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="center-block">
                    <div class="top-logo wow fadeIn">
                        <img src="{{ url('public/site_assets') }}/images/logo.png" alt="footer image" class="img-fluid">
                    </div>
                    <div class="description wow fadeIn">
                        <p>
                            @if(\Illuminate\Support\Facades\App::getLocale() == "ar")
                                {{ SETTING_VALUE('APP_DESC_AR') }}
                            @else
                                {{ SETTING_VALUE('APP_DESC_EN') }}
                            @endif
                        </p>
                    </div>
                    <div class="social-links">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <!-- <li>
                                    <a href="#">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="left-block">
                    <h3 class="tilte">
                        <span class="title-icon"><img src="{{ url('public/site_assets') }}/images/icon/car.png"></span>
                        {{ trans('site.FollowUs') }}
                    </h3>
                    <form action="">
                        <textarea name="message" class="message-box"></textarea>
                        <button type="submit" class="btn-send">{{ trans('site.Submit') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right">
        <div class="container">
            <p class="reserved-site">{{ trans('site.CopyRights') }}</p>
            <p class="company-owner">تصميم وتطوير شركة<a href="https://www.rmal.com.sa" target="_blank"> رمال الأودية</a></p>
        </div>
    </div>
</section>
<!-- end footer -->

<!-- JavaScript -->
<script src="{{ url('public/site_assets') }}/js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="{{ url('public/site_assets') }}/js/popper.min.js" type="text/javascript"></script>
<script src="{{ url('public/site_assets') }}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ url('public/site_assets') }}/js/all-fontawesome.min.js" type="text/javascript"></script>
<script src="{{ url('public/site_assets') }}/js/slick.js" type="text/javascript"></script>
<script src="{{ url('public/site_assets') }}/js/select2.min.js" type="text/javascript"></script>
<script src="{{ url('public/site_assets') }}/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
<script src="{{ url('public/site_assets') }}/js/wow.min.js"></script>
<script src="{{ url('public/site_assets') }}/js/jquery.fancybox.min.js"></script>
<script src="{{ url('public/site_assets') }}/js/main.js" type="text/javascript"></script>
<script src="{{ url('public/site_assets') }}/js/map.js" type="text/javascript"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDx--BILIM2LjZBxVMjGYVb8YqrY-Vk_Yk&callback=initMap">
</script>

@yield('script')
</body>

</html>