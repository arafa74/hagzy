@extends('site.parts.layout')
@section('content')

    <!-- start page content -->
    <section class="page-content">
        <!-- start installment -->
        <div class="installment-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="brand-slider">
                            <h3 class="tilte">{{ $car->name }}</h3>
                            {{--<div class="top-overlay">--}}
                                {{--<div class="social-content">--}}
                                    {{--<div class="links">--}}
                                        {{--<a href="#">--}}
                                            {{--<i class="fab fa-facebook-f"></i>--}}
                                        {{--</a>--}}
                                        {{--<a href="#">--}}
                                            {{--<i class="fab fa-twitter"></i>--}}
                                        {{--</a>--}}
                                        {{--<a href="#">--}}
                                            {{--<i class="fab fa-facebook-f"></i>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            @if(count($images) > 0)
                                <div class="product slider">
                                    @foreach($images as $img)
                                    <div class="new-img">
                                        <img src="{{ asset('storage/app/cars/200x150/' . $img->image) }}" class="img-fluid" alt="new-img">
                                        <div class="bottom-overlay">
                                            <a href="{{ asset('storage/app/cars/200x150/' . $img->image) }}" class="group fancybox fullscreen"
                                               data-fancybox="gallery">
                                                <img src="{{ asset('storage/app/cars/200x150/' . $img->image) }}" class="img-fluid" alt="full screen">
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                @foreach($images as $img)
                                <div class="product-list slider">
                                    <div class="all-new">
                                        <img src="{{ asset('storage/app/cars/50x50/' . $img->image) }}" class="img-fluid" alt="new-img">
                                    </div>
                                </div>
                                @endforeach
                            @else
                                <div class="new-img">
                                    <img src="{{ asset('storage/app/cars/default.png') }}" class="img-fluid" alt="new-img">
                                    <div class="bottom-overlay">
                                        <a href="{{ asset('storage/app/cars/default.png') }}" class="group fancybox fullscreen"
                                           data-fancybox="gallery">
                                            <img src="{{ asset('storage/app/cars/default.png') }}" class="img-fluid" alt="full screen">
                                        </a>
                                    </div>
                                </div>
                            @endif
                        </div>
                        @if(count($options) > 0)
                            <div class="brand-info wow fadeInUp">
                                <h3 class="title">{{ trans('site.options') }}  {{ $car->name }}</h3>
                                <ul class="list-unstyled">
                                    @foreach($options as $option)
                                    <li>{{ $option->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <div class="ads-block">
                            <ul class="social-media list-unstyled">
                                <li class="wow fadeIn">
                                    <a href="tel:+">
                                        <i class="fas fa-phone-volume"></i>
                                    </a>
                                </li>
                                <li class="wow fadeIn">
                                    <a href="mailto:info@example.com?subject=subject&cc=cc@example.com">
                                        <i class="fas fa-envelope"></i>
                                    </a>
                                </li>
                                <li class="wow fadeIn">
                                    <a href="https://api.whatsapp.com/send?phone=15551234567" target="_blank">
                                        <i class="fab fa-whatsapp"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="ads-area wow fadeIn">
                                <img src="images/bg/ads.png" alt="advertise" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end installment -->
        <!-- start more brands -->
        @if(count($similar_cars) > 0)
        <div class="more-brands">
            <div class="container">
                <h3 class="tilte">
                    <span class="title-icon"><img src="{{ asset('public/site_assets/') }}/images/icon/car.png"></span>
                    {{ trans('site.newCats') }}
                </h3>
                <div class="row">
                    @foreach($similar_cars as $car)
                    <div class="col-md-3">
                        <a href="available-years.html">
                            <div class="brand-card">
                                <div class="brand-img">
                                    @php
                                        $images_count = App\Image::where('car_id' , $car->id)->count() ;
                                    @endphp
                                    @if($images_count >= 1)
                                        <img src="{{ asset('storage/app/cars/200x150/' . App\Image::where('car_id' , $car->id)->first()->image) }}" class="img-fluid" alt="slider1">
                                    @else
                                        <img src="{{ asset('storage/app/cars/default.png') }}" class="img-fluid" alt="slider1">
                                    @endif
                                </div>
                            </div>
                            <div class="brand-title">
                                <h4>{{ $car->name }}</h4>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        <!-- end more brands -->
    </section>
    <!-- end page content -->
@endsection