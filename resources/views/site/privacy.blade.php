@extends('site.parts.layout')
@section('content')
    <section class="page-content">
        <!-- start about page -->
        <div class="about-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="about-content">
                            <h3 class="about-title">
                                <span class="title-icon wow fadeIn"><img src="{{ url('public/site_assets') }}/images/icon/car-about.png"></span>
                                {{ trans('site.AboutSite') }}
                            </h3>
                            <p class="about-description wow fadeIn">
                                @if(\Illuminate\Support\Facades\App::getLocale() == "ar")
                                    {{ SETTING_VALUE('PRIVACY_POLICY_AR') }}
                                @else
                                    {{ SETTING_VALUE('PRIVACY_POLICY_EN') }}
                                @endif

                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="about-img wow fadeIn" data-wow-duration="1s">
                <img src="{{ url('public/site_assets') }}/images/bg/03.png" alt="about" class="img-fluid">
            </div>
        </div>
        <!-- end about page -->
    </section>
    <!-- end page content -->
@endsection