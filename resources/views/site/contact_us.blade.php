@extends('site.parts.layout')
@section('content')
<!-- start page content -->
<section class="page-content">
    <!-- start about -->
    <div class="about-block">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-slider slider wow fadeIn" data-wow-duration="2s">
                    @foreach($slides as $slide)
                        <div class="item">
                            <img src="{{ url('storage/app/slides/200x150/' . $slide->image) }}" class="img-fluid" alt="slider1">
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-contact wow fadeIn" data-wow-duration="2s">
                        <h3 class="tilte">
                            <span class="title-icon"><img src="{{ url('public/site_assets') }}/images/icon/car.png"></span>
                            {{ trans('site.for_complaint') }}
                        </h3>
                        <div id="alert-not-found" class="alert alert-danger hide-register-alert ">
                            <ul class="list-unstyled">

                            </ul>
                        </div>
                        <form action="" method="post" id="registerForm" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="input-block">
                                <input type="text" name="title" placeholder="{{ trans('site.title_complaint') }}" class="form-control"
                                >
                            </div>
                            <div class="form-control">
                            <textarea name="details" class="message-box form-control"
                                      placeholder="{{ trans('site.complaint') }}"></textarea>
                                      </div>
                                      
                            <button type="submit" class="btn btn-send">{{ trans('site.Submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end about -->
    <!-- start map -->
    <div class="map-block">
        <div id="map"></div>
        <div class="Complaints">
            <h3 class="title">
                <span class="icon"><img src="{{ url('public/site_assets') }}/images/icon/ball.png" alt="ball"></span>
                {{ trans('site.for_complaint') }}
            </h3>
            <ul class="list-unstyled">
                <li>
                    <span class="icon"><img src="{{ url('public/site_assets') }}/images/icon/phone.png" alt="phone"></span>
                    <span class="info">
                            {{ SETTING_VALUE('MOBILE') }}</br>

                        </span>
                </li>
                <li>
                    <span class="icon"><img src="{{ url('public/site_assets') }}/images/icon/message.png" alt="message"></span>
                    <span class="info">
                            {{ SETTING_VALUE('FORMAL_EMAIL') }}</br>

                        </span>
                </li>
            </ul>
        </div>
    </div>
    <!-- end map -->
</section>
<!-- end page content -->
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(".hide-register-alert").css('display', 'none');

            $("#registerForm").on('submit', function (e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                e.preventDefault();
                $.ajax({
                    url: '{{ route('post.contact_us') }}',
                    type: 'POST',
                    data: $(this).serialize(),
                    success: function (data) {
                        if ($.isEmptyObject(data.error)) {
                            window.location.href = "{{ route('contact_us')  }}";
                        } else {
                            console.log(data.error);
                            printErrorMsg(data.error);
                        }
                    }
                });

            });

            function printErrorMsg(msg) {
                $(".hide-register-alert").css('display', 'block');
                $(".hide-register-alert").find("ul").html('');
                $.each(msg, function (key, value) {
                    $(".hide-register-alert").find("ul").append('<li>' + value + '</li>');
                });
                $(".hide-register-alert").delay(3000).fadeOut();
            }
        });
    </script>
@endsection