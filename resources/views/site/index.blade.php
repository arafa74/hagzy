@extends('site.parts.layout')
@section('content')
<!-- start page content -->
<section class="page-content">
    <!-- start about -->
    <div class="about-block">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-slider slider wow fadeIn" data-wow-duration="2s">
                        @foreach($slides as $slide)
                            <div class="item">
                                <img src="{{ url('storage/app/slides/200x150/' . $slide->image) }}" class="img-fluid" alt="slider1">
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-rate wow fadeIn" data-wow-duration="2s">
                        <h3 class="tilte">
                            <span class="title-icon"><img src="images/icon/car.png"></span>
                            قيم سيارتك
                        </h3>
                        <form action="">
                            <div class="tab-content">
                                <div class="tab-pane fade show active">
                                    <div class="choices">
                                        <select class="js-example-basic-single" name="carBrand">
                                            <option value="brand">نوع السيارة </option>
                                            <option value="brand1"> براند1 </option>
                                            <option value="brand2"> براند2 </option>
                                            <option value="brand3"> براند3 </option>
                                            <option value="brand4"> براند4 </option>
                                        </select>
                                    </div>
                                    <div class="choices">
                                        <select class="js-example-basic-single" name="modelBrand">
                                            <option value="model"> الموديل </option>
                                            <option value="model1"> موديل1 </option>
                                            <option value="model2"> موديل2 </option>
                                            <option value="model3"> موديل3 </option>
                                            <option value="model4"> موديل4 </option>
                                        </select>
                                    </div>
                                    <div class="choices">
                                        <select class="js-example-basic-single" name="prData">
                                            <option value="date"> سنة الصنيع </option>
                                            <option value="date1"> 2019 </option>
                                            <option value="date2"> 2018 </option>
                                            <option value="date3"> 2017 </option>
                                        </select>
                                    </div>
                                    <button type="button" class="btn next">متابعة</button>
                                </div>
                                <div class="tab-pane">
                                    <div class="choices">
                                        <select class="js-example-basic-single" name="carStructure">
                                            <option value="structure">نوع الهيكل </option>
                                            <option value="structure1"> هيكل1 </option>
                                            <option value="structure2"> هيكل2 </option>
                                            <option value="structure3"> هيكل3 </option>
                                            <option value="structure4"> هيكل4 </option>
                                        </select>
                                    </div>
                                    <div class="choices">
                                        <select class="js-example-basic-single" name="modelEngine">
                                            <option value="engine"> محرك </option>
                                            <option value="engine1"> محرك </option>
                                            <option value="engine2"> محرك </option>
                                            <option value="engine3"> محرك </option>
                                            <option value="engine5"> محرك </option>
                                        </select>
                                    </div>
                                    <div class="choices">
                                        <select class="js-example-basic-single" name="carDistance">
                                            <option value="distance"> 30 كيلو متر </option>
                                            <option value="distance1"> 30 كيلو متر </option>
                                            <option value="distance2"> 30 كيلو متر </option>
                                            <option value="distance3"> 30 كيلو متر </option>
                                        </select>
                                    </div>
                                    <button type="button" class="btn next">متابعة</button>
                                </div>
                                <div class="tab-pane">
                                    <div class="check-block">
                                        <h4 class="check-title">المسافة المقطوعة</h4>
                                        <div class="check-input">
                                            <div class="check-one">
                                                <input type="radio" id="ordinary" name="speed">
                                                <label for="ordinary">عادية</label>
                                            </div>
                                            <div class="check-one">
                                                <input type="radio" id="moderate" name="speed">
                                                <label for="moderate">متوسطة</label>
                                            </div>
                                            <div class="check-one">
                                                <input type="radio" id="high" name="speed">
                                                <label for="high">كبيرة</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="check-block">
                                        <h4 class="check-title">الصبغة</h4>
                                        <div class="check-input">
                                            <div class="check-one">
                                                <input type="radio" id="ordinaryClr" name="color">
                                                <label for="ordinaryClr">عادية</label>
                                            </div>
                                            <div class="check-one">
                                                <input type="radio" id="moderateClr" name="color">
                                                <label for="moderateClr">متوسطة</label>
                                            </div>
                                            <div class="check-one">
                                                <input type="radio" id="highClr" name="color">
                                                <label for="highClr">كبيرة</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="check-block">
                                        <h4 class="check-title">مواصفات خليجى</h4>
                                        <div class="check-input">
                                            <div class="check-one">
                                                <input type="radio" id="acceptFeature" name="feature">
                                                <label for="acceptFeature">نعم</label>
                                            </div>
                                            <div class="check-one">
                                                <input type="radio" id="refuseFeature" name="feature">
                                                <label for="refuseFeature">لا</label>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn next">متابعة</button>
                                </div>
                                <div class="tab-pane">
                                    <div class="check-block">
                                        <h3 class="tilte">
                                            فقط خطوة 1 متبقية ! 268 موعد قد حجز
                                            امن موعدك الآن
                                        </h3>
                                        <div class="choices">
                                            <select class="js-example-basic-single" name="carBrand">
                                                <option value="brand">نوع السيارة </option>
                                                <option value="brand1"> براند1 </option>
                                                <option value="brand2"> براند2 </option>
                                                <option value="brand3"> براند3 </option>
                                                <option value="brand4"> براند4 </option>
                                            </select>
                                        </div>
                                        <div class="choices">
                                            <select class="js-example-basic-single" name="modelBrand">
                                                <option value="model"> الموديل </option>
                                                <option value="model1"> موديل1 </option>
                                                <option value="model2"> موديل2 </option>
                                                <option value="model3"> موديل3 </option>
                                                <option value="model4"> موديل4 </option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="email" placeholder="البريد الالكترونى" class="form-inpt"
                                           required="">
                                    <button type="submit" class="btn">ارسال</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end about -->

    <!-- start news -->
    @if(isset($news[0]))
    <div class="news-block">
        <div class="container">
            <h3 class="tilte">
                <span class="title-icon"><img src="{{ url('public/site_assets') }}/images/icon/car.png"></span>
                {{ trans('site.News') }}
            </h3>
            <div class="news slider slider-for">
                <div class="new-details">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="report-new wow fadeInUp">
                                <h3 class="reoprt-title">{{ trans('site.reports') }}</h3>
                                <h4 class="new-title">{{ $news[0]->title }}</h4>
                                <p class="new-description">
                                    {{ $news[0]->description }}
                                </p>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="new-img wow fadeInUp" data-wow-delay="0.5s">
                                <img src="{{ asset('storage/app/news/200x150/'.$news[0]->image ) }}" class="img-fluid" alt="new-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(count($news) > 0)
            <div class="slider slider-nav">
                @foreach($news as $new)

                <div class="all-new">
                    <div class="content wow fadeInUp" data-wow-delay="0.5s">
                        <div class="new-right">
                            <img src="{{ asset('storage/app/news/200x150/' . $new->image) }}" class="img-fluid" alt="new-img">
                        </div>
                        <div class="new-left">
                            <h4 class="new-title">{{ $new->title }}</h4>
                            <p class="new-description">
                                {{str_limit($new ->description, $limit = 50, $end = '...')}}
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>
    <!-- end news -->
    <!-- start recently -->
    <div class="recently-added">
        <div class="container">
            <h3 class="tilte">
                <span class="title-icon"><img src="{{ url('public/site_assets') }}/images/icon/car.png"></span>
                {{ trans('site.newCars') }}
            </h3>
            <div class="recent-added slider">
                @foreach($last_cars as $car)h
                <div class="item">
                    <div class="content">
                        <a href="{{route('car', [$car->id,$car->name_en])}}">
                            @php
                                $images_count = App\Image::where('car_id' , $car->id)->count() ;
                            @endphp
                            @if($images_count >= 1)
                            <img src="{{ asset('storage/app/cars/200x150/' . App\Image::where('car_id' , $car->id)->first()->image) }}" class="img-fluid" alt="slider1">
                            @else
                                <img src="{{ asset('storage/app/cars/default.png') }}" class="img-fluid" alt="slider1">
                            @endif
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- end new recently -->
</section>
<!-- end page content -->
@endsection