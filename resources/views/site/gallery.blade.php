@extends('site.parts.layout')
@section('content')
    <!-- start page content -->
    <section class="page-content">
        <div class="all-categories">
            <!-- start brands tabs -->
            <div class="tabs-block">
                <div class="container">
                    <ul class="nav nav-pills categories-pills product-pills" id="pills-tab" role="tablist">
                        <li class="nav-item wow fadeIn" data-wow-delay="0.5s">
                            <a class="nav-link active" id="pills-offer-tab" data-toggle="pill" href="#pills-offer"
                               role="tab" aria-controls="pills-offer" aria-selected="true">
                                {{ trans('site.showCars') }}
                            </a>
                        </li>
                        <li class="nav-item wow fadeIn" data-wow-delay="0.5s">
                            <a class="nav-link" id="pills-new-tab" data-toggle="pill" href="#pills-new" role="tab"
                               aria-controls="pills-new" aria-selected="false">
                                {{ trans('site.newCars') }}
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content categories-cards wow fadeInUp" id="pills-tabContent" data-wow-delay="1s">
                        <div class="tab-pane fade show active" id="pills-offer" role="tabpanel"
                             aria-labelledby="pills-offer-tab">
                            <div class="row">
                                @foreach($old_cars as $car)
                                    <div class="col-md-3">
                                    <a href="{{route('car', [$car->id , $car->name_en])}}">
                                        <div class="brand-card">
                                            <div class="brand-img">
                                                @php
                                                    $images_count = App\Image::where('car_id' , $car->id)->count() ;
                                                @endphp
                                                @if($images_count >= 1)
                                                    <img src="{{ asset('storage/app/cars/200x150/' . App\Image::where('car_id' , $car->id)->first()->image) }}" class="img-fluid" alt="slider1">
                                                @else
                                                    <img src="{{ asset('storage/app/cars/default.png') }}" class="img-fluid" alt="slider1">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="brand-title">
                                            <h4>{{ $car->name }}</h4>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-new" role="tabpanel" aria-labelledby="pills-new-tab">
                            <div class="row">
                                @foreach($new_cars as $car)
                                    <div class="col-md-3">
                                        <a href="{{route('car', [$car->id,
                                                  $car->name])}}">
                                            <div class="brand-card">
                                                <div class="brand-img">
                                                    @php
                                                        $images_count = App\Image::where('car_id' , $car->id)->count() ;
                                                    @endphp
                                                    @if($images_count >= 1)
                                                        <img src="{{ asset('storage/app/cars/200x150/' . App\Image::where('car_id' , $car->id)->first()->image) }}" class="img-fluid" alt="slider1">
                                                    @else
                                                        <img src="{{ asset('storage/app/cars/default.png') }}" class="img-fluid" alt="slider1">
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="brand-title">
                                                <h4>{{ $car->name }}</h4>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- end brands tabs -->
        </div>
    </section>
    <!-- end page content -->
@endsection