@extends('site.parts.layout')
@section('content')
<!-- start page content -->
<section class="page-content">
    <div class="all-categories">
        <!-- start brand information -->
        <div class="brand-info">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="brand-card wow zoomIn">
                            <img src="{{ asset('storage/app/categories/200x150/' . $category->image) }}" alt="brand" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <h3 class="brand-title wow fadeIn">{{ $category->name }} </h3>
                        <p class="brand-description wow fadeInUp">
                           {{ $category->desc }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end brand information -->
        <!-- start brands tabs -->
        <div class="tabs-block">
            <div class="container">
                <ul class="nav nav-pills categories-pills" id="pills-tab" role="tablist">
                    <?php $counter = 1; ?>
                    @foreach($types as $type)
                        <li class="nav-item wow fadeIn" data-wow-delay="0.5s">
                            <a class="nav-link @if($counter==1) active @endif" id="pills-sydan-tab{{$type->id}}" data-toggle="pill" href="#pills-sydan{{$type->id}}"
                               role="tab" aria-controls="pills-sydan{{$type->id}}" aria-selected="true">
                                {{ $type->name_ar }}
                            </a>
                        </li>
                            <?php $counter++; ?>
                    @endforeach
                </ul>

                <div class="tab-content categories-cards wow fadeInUp" id="pills-tabContent" data-wow-delay="1s">
                    <?php $counter = 1; ?>
                    @foreach($types as $type)
                        <div class="tab-pane fade @if($counter==1)show active @endif" id="pills-sydan{{$type->id}}" role="tabpanel"
                         aria-labelledby="pills-sydan-tab">
                            <div class="row">
                                @foreach($type->car as $car)
                                <div class="col-md-3">
                                    <a href="{{route('car', [$car->id,
                                                  $car->name_en])}}">
                                        <div class="brand-card">
                                            <div class="brand-img">
                                                <img src="{{asset('public/site_assets/')}}/images/brands/05.png" alt="brand">
                                            </div>
                                        </div>
                                        <div class="brand-title">
                                            @php
                                                $name = 'name_'.app()->getLocale();
                                            @endphp
                                            <h4>{{ $car->$name }}</h4>
                                        </div>
                                    </a>
                                </div>

                                @endforeach
                            </div>
                        </div>
                            <?php $counter++; ?>
                    @endforeach
                </div>

                {{--<div class="more wow fadeInUp">--}}
                    {{--<a href="#" class="more-link">المزيد</a>--}}
                {{--</div>--}}
            </div>
        </div>
        <!-- end brands tabs -->
        @if(isset($offers))
        <!-- start recent offers -->
        <div class="recent-offers">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="offer-img wow fadeInUp">
                            <img src="{{ asset('storage/app/offers/200x150/' . $offers->image) }}" alt="offer" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="offer-detail wow fadeInUp">
                            <h3 class="tilte">
                                <span class="title-icon"><img src="{{ asset('public/site_assets') }}/images/icon/car.png"></span>
                                {{ trans('site.offers') }} {{ $category->name }}
                            </h3>

                            <p class="wow fadeInUp" data-wow-delay="0.4s">
                               {{ $offers->description }}
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <!-- end recent offers -->

        <!-- start news -->
        @if(isset($news[0]))
            <div class="news-block">
                <div class="container">
                    <h3 class="tilte">
                        <span class="title-icon"><img src="{{ url('public/site_assets') }}/images/icon/car.png"></span>
                        {{ trans('site.News') }}
                    </h3>
                    <div class="news slider slider-for">
                        <div class="new-details">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="report-new wow fadeInUp">
                                        <h3 class="reoprt-title">{{ trans('site.reports') }}</h3>
                                        <h4 class="new-title">{{ $news[0]->title }}</h4>
                                        <p class="new-description">
                                            {{ $news[0]->description }}
                                        </p>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="new-img wow fadeInUp" data-wow-delay="0.5s">
                                        <img src="{{ asset('storage/app/news/200x150/'.$news[0]->image ) }}" class="img-fluid" alt="new-img">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(count($news) > 0)
                        <div class="slider slider-nav">
                            @foreach($news as $new)

                                <div class="all-new">
                                    <div class="content wow fadeInUp" data-wow-delay="0.5s">
                                        <div class="new-right">
                                            <img src="{{ asset('storage/app/news/200x150/' . $new->image) }}" class="img-fluid" alt="new-img">
                                        </div>
                                        <div class="new-left">
                                            <h4 class="new-title">{{ $new->title }}</h4>
                                            <p class="new-description">
                                                {{str_limit($new ->description, $limit = 50, $end = '...')}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <!-- end news -->
    </div>
</section>
<!-- end page content -->
@endsection