<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-default">
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user-material">
			<div class="category-content">
				<div class="sidebar-user-material-content">
					<a href="#"><img src="{{ auth()->user()->image_url }}" class="img-circle img-responsive" alt=""></a>
					<h6> {{ auth()->user()->full_name }} </h6>
					<span class="text-size-small"> {{ auth()->user()->admin_group->name_ar }} </span>
				</div>

				<div class="sidebar-user-material-menu">
					<a href="#user-nav" data-toggle="collapse"><span>{{ trans('dash.my_account') }}</span> <i class="caret"></i></a>
				</div>
			</div>

			<div class="navigation-wrapper collapse" id="user-nav">
				<ul class="navigation">
					<li><a href="{{ route('admin_profile') }}"><i class="icon-user-plus"></i> <span> {{ trans('dash.my_profile') }} </span></a></li>
					<li class="divider"></li>
					<li><a href="{{ route('setting') }}"><i class="icon-cog5"></i> <span> {{ trans('dash.settings') }} </span></a></li>
					<li><a href="{{ route('dashboard_logout') }}"><i class="icon-switch2"></i> <span> {{ trans('dash.logout') }} </span></a></li>
				</ul>
			</div>
		</div>
		<!-- /user menu -->


		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">

					<!-- Main -->
					<li class="navigation-header"><span> {{ trans('dash.main') }} </span> <i class="icon-menu" title="Main pages"></i></li>
					<li {{ request()->route()->getName() === 'dashboard_home' ? ' class=active' : '' }} ><a href="{{ route('dashboard_home') }}"><i class="icon-home4"></i> <span> {{ trans('dash.home') }} </span></a></li>

					<li>
						<a href="#"><i class="icon-people"></i> <span> {{ trans('dash.users') }} </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'users' ? ' class=active' : '' }}><a href="{{ route('users') }}">{{ trans('dash.users') }}</a></li>
							<li {{ request()->route()->getName() === 'restaurants' ? ' class=active' : '' }}><a href="{{ route('restaurants') }}">المطاعم (مقدمى الخدمات)</a></li>
{{--							<li {{ request()->route()->getName() === 'create_user' ? ' class=active' : '' }}><a href="{{ route('create_user') }}">{{ trans('dash.add_new_users') }}</a></li>--}}
						</ul>
                    </li>

					{{--<li>--}}
						{{--<a href="#"><i class="icon-people"></i> <span> {{ trans('الاعلانات') }} </span></a>--}}
						{{--<ul>--}}
							{{--<li {{ request()->route()->getName() === 'ads' ? ' class=active' : '' }}><a href="{{ route('ads') }}">{{ trans('عرض') }}</a></li>--}}
						{{--</ul>--}}
                    {{--</li>--}}


					<li>
						<a href="#"><i class="icon-city"></i> <span> اقسام الاكل </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'categories' ? ' class=active' : '' }}><a href=" {{ route('categories') }} " > عرض</a></li>
							<li {{ request()->route()->getName() === 'category_create' ? ' class=active' : '' }}><a href=" {{ route('category_create') }} " > اضافه جديد </a></li>
						</ul>
					</li>

						{{--<li>--}}
							{{--<a href="#"><i class="icon-city"></i> <span> {{ trans('dash.types') }} </span></a>--}}
							{{--<ul>--}}
								{{--<li {{ request()->route()->getName() === 'types' ? ' class=active' : '' }}><a href=" {{ route('types') }} " > {{ trans('dash.types') }} </a></li>--}}
								{{--<li {{ request()->route()->getName() === 'type_create' ? ' class=active' : '' }}><a href=" {{ route('type_create') }} " > اضافه انواع جديده للسيارات </a></li>--}}
							{{--</ul>--}}
						{{--</li>--}}


					<li>
                        <a href="#"><i class="icon-city"></i> <span> {{ trans('dash.options') }} </span></a>
                        <ul>
                            <li {{ request()->route()->getName() === 'options' ? ' class=active' : '' }}><a href=" {{ route('options') }} " > {{ trans('dash.options') }} </a></li>
                            <li {{ request()->route()->getName() === 'option_create' ? ' class=active' : '' }}><a href=" {{ route('option_create') }} " > اضافه جديد </a></li>
                        </ul>
                    </li>

					<li>
						<a href="#"><i class="icon-city"></i> <span> {{ trans('الاطباق') }} </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'dishes' ? ' class=active' : '' }}><a href=" {{ route('dishes') }} " > عرض </a></li>
						</ul>
					</li>

					<li>
						<a href="#"><i class="icon-city"></i> <span> العروض </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'offers' ? ' class=active' : '' }}><a href=" {{ route('offers') }} " > العروض </a></li>
						</ul>
					</li>

					<li>
						<a href="#"><i class="icon-city"></i> <span>الحجوزات </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'reservations' ? ' class=active' : '' }}><a href=" {{ route('reservations') }} " >  عرض  </a></li>
						</ul>
					</li>

					<li>
						<a href="#"><i class="icon-city"></i> <span>معرض الصور</span></a>
						<ul>
							<li {{ request()->route()->getName() === 'slides' ? ' class=active' : '' }}><a href=" {{ route('slides') }} " >  عرض  </a></li>
						</ul>
					</li>

					<!-- /main -->

					<!-- others -->
					<li class="navigation-header"><span> {{ trans('dash.others') }} </span> <i class="icon-menu" title=" {{ trans('dash.others') }} "></i></li>

                    <li>
						<a href="#"><i class="icon-fire"></i> <span> {{ trans('dash.administration') }} </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'administration_groups' ? ' class=active' : '' }}><a href="{{ route('administration_groups') }}"> {{ trans('dash.administration_groups') }} </a></li>
							<li {{ request()->route()->getName() === 'admins' ? ' class=active' : '' }}><a href="{{ route('admins') }}"> {{ trans('dash.admins') }} </a></li>
						</ul>
					</li>



					<li>
						<a href="#"><i class="icon-rocket"></i> <span> {{ trans('dash.nationalities') }} </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'nationality' ? ' class=active' : '' }}><a href="{{ route('nationality') }}"> {{ trans('dash.nationalities') }} </a></li>
							<li {{ request()->route()->getName() === 'nationality_create' ? ' class=active' : '' }}><a href="{{ route('nationality_create') }}"> {{ trans('dash.create_new_nationalities') }} </a></li>
						</ul>
                    </li>

					<li>
						<a href="#"><i class="icon-rocket"></i> <span> الشكاوى والمقترحات </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'contacts' ? ' class=active' : '' }}><a href="{{ route('contacts') }}"> عرض </a></li>
						</ul>
					</li>


					<li {{ request()->route()->getName() === 'setting' ? ' class=active' : '' }} ><a href="{{ route('setting') }}"><i class="icon-wrench3"></i> <span> {{ trans('dash.settings') }} </span></a></li>



					<!-- /others -->

				</ul>
			</div>
		</div>
		<!-- /main navigation -->

	</div>
</div>
<!-- /main sidebar -->
