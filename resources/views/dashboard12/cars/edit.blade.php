@extends('dashboard.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>

    <!-- /theme JS files -->

@endsection

@section('content')

<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <form action="{{ route('car_update') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ Form::token() }}
            <input type="hidden" name="car_id" value="{{ $car->id }}" />
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('تعديل سياره') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.name_ar') }}</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="name_ar" placeholder="{{ trans('dash.name_ar') }}" value="{{ $car->name_ar }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.name_en') }}</label>
                        <div class="col-lg-9">
                            <input type="text" name="name_en" class="form-control" placeholder="{{ trans('dash.name_en') }}" value="{{ $car->name_en }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3"> نبذه عن السياره بالعربيه <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="5" cols="5" name="description_ar" class="form-control" required="required" placeholder="{{ trans('dash.app_desc_ar') }}"> {{ $car->description_ar }} </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">  نبذه عن السياره بالانجليزيه <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="5" cols="5" name="description_en" class="form-control" required="required" placeholder="{{ trans('dash.app_desc_ar') }}"> {{ $car->description_en }} </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> {{ trans('dash.category') }} </label>
                        <div class="col-lg-9">
                            <select name="category_id" class="select-border-color border-warning" >
                                <optgroup label="{{ trans('اختر القسم') }}">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" @if($car->category_id == $category->id) selected @endif> {{ $category->name_ar.' - '.$category->name_en }} </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> {{ trans('dash.category') }} </label>
                        <div class="col-lg-9">
                            <select name="type_id" class="select-border-color border-warning" >
                                <optgroup label="{{ trans('اختر القسم') }}">
                                    @foreach ($types as $type)
                                        <option value="{{ $type->id }}" @if($car->type_id == $type->id) selected @endif> {{ $type->name_ar.' - '.$type->name_en }} </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                            <label class="col-lg-3 control-label"> حذف صور السياره</label>
                            <div class="col-lg-9">
                            @foreach($car->image as $image)
                                <div class="col-md-3">
                                    <img style="width: 100%;height:200px;" src="{{ Request::root() }}/storage/app/cars/200x150/{{$image->image}}" alt="" class="img-responsive">
                                    <input value="{{$image->id}}" name="image_id[]" type="checkbox" class="form-control" style="position: absolute;top: -6px;right: 15px;width: 19px;">
                                </div>
                            @endforeach
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-3 control-label"> اضافه صور للسياره</label>
                        <div class="col-lg-9">
                            <input type="file" class="file-styled" name="images[]" multiple>
                            <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                        </div>
                    </div>

                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title"> {{ trans('dash.options') }} </h5>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                </ul>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="row">
                                @foreach($options as $option)
                                    @if(App\CarOption::where(['car_id' => $car->id , 'option_id' => $option->id ])->exists())
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3"> {{ $option->name_ar }} </label>
                                            <div class="col-lg-9">
                                                <div class="checkbox checkbox-switchery switchery-xs">
                                                    <label>
                                                        <input type="checkbox" name="options[]" class="switchery" value="{{ $option->id }}" checked>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> {{ $option->name_ar }} </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            <input type="checkbox" name="options[]" class="switchery" value="{{ $option->id }}">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>


                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.update_and_forword_2_list') }} " />
                        <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.update_and_come_back') }} " />
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->

    </div>

    </div>





@endsection
