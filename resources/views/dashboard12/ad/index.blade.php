@extends('dashboard.layout')

@section('script')

    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/user_pages_team.js"></script>

    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/datatables_advanced.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')


<div class="panel panel-flat tb_padd">
    <div class="panel-heading">
        <h5 class="panel-title"> {{ trans('الاعلانات') }} </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    {{--<a class="btn btn-primary" href="{{ route('create_user') }}"> {{ trans('dash.add_new_users') }} </a>--}}
    <center>
        {{ $ads->links() }}
    </center>
    <table class="table table-condensed table-hover datatable-highlight">
        <thead>
        <tr>
            <th class="text-center"> # </th>
            <th class="text-center"> {{ trans('عنوان الاعلان') }} </th>
            <th class="text-center"> {{ trans('تاريخ البدايه') }} </th>
            <th class="text-center"> {{ trans('تاريخ الانتهاء') }} </th>
            <th class="text-center"> {{ trans('الخدمه') }} </th>
            <th class="text-center"> {{ trans('التصنيف') }} </th>
            <th class="text-center"> {{ trans('تاريخ النشر') }} </th>
        </tr>
        </thead>
        <tbody>
            @forelse($ads as $ad)
                <tr id="row_{{ $ad->id }}">
                    <td> {{ $ad->id }} </td>
                    <td> {{ str_limit($ad->title,30,'...') }} </td>
                    <td> {{ $ad->start_date }} </td>
                    <td> {{ $ad->start_date }} </td>
                    <td> {{ $ad->service->name_ar }} </td>
                    <td> {{ $ad->category->name_ar }} </td>
                    <td> {{ $ad->created_at->format('Y-m-d H:i') }} </td>
                    <td class="text-center">
                        {{--<a href="{{ route('order_show',['id' => $ad->id ]) }}" class="btn btn-primary"> <i class="icon-touch"></i> </a>--}}
                    </td>
                    <td class="text-center">
                        <a href="{{ route('ad_details',['id' => $ad->id ]) }}" class="btn btn-primary"> <i class="icon-touch"></i> </a>
                        {{--<a href="#" class="btn btn-success" data-popup="tooltip" title="" data-toggle="modal" data-target="#call_{{$user->id}}" data-original-title="Call"><i class="icon-phone2"></i></a>--}}
                        {{--<a href="{{ route('user_edit',['id' => $user->id ]) }}" class="btn btn-primary"> <i class="icon-pencil3"></i> </a>--}}
                        <a onclick="sweet_delete( ' {{ route('ad_delete').'/'.$ad->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $ad->id }} )" class="btn btn-danger" > <i class="icon-database-remove"></i> </a>
                    </td>
                    </td>
                </tr>

            <!-- Phone call modal -->

            <!-- /phone call modal -->
            @empty
            @endforelse
            
        </tbody>
    </table>
    <center>
        {{ $ads->links() }}
    </center>
    <br>
</div>


@endsection
