@extends('dashboard.layout')

@section('script')
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/user_pages_team.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"> {{ trans('dash.client_details') }} </h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <ul class="nav nav-tabs nav-tabs-highlight">
                    <li class="active"><a href="#details-tab" data-toggle="tab"><i class="icon-menu7 position-left"></i> {{ trans('dash.user_profile') }} </a></li>
                    <li><a href="#finished_orders" data-toggle="tab"><i class="icon-mention position-left"></i> {{ trans('dash.finished_orders') }} </a></li>
                    <li><a href="#prices" data-toggle="tab"><i class="icon-mention position-left"></i> {{ trans('dash.accounting') }} </a></li>
                </ul>
                <div class="tabbable nav-tabs-vertical nav-tabs-left">

                    <div class="tab-content">

                        <div class="tab-pane active has-padding" id="details-tab">
                            <div class="row">


                                <div class="col-lg-6">
                                    <div class="content-group">
                                        <div class="panel-body bg-blue border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
                                            <div class="content-group-sm">
                                                <h5 class="text-semibold no-margin-bottom">
                                                    {{ @$user->full_name }}
                                                </h5>
                                                <span class="display-block"> {{ trans('dash.delivery') }} </span>
                                            </div>
                                            <a href="{{ @$user->imageurlorg }}" data-popup="lightbox">
                                                <img src=" {{ @$user->imageurl }} " class="img-circle" alt="">
                                            </a>

                                            <ul class="list-inline no-margin-bottom">
                                                <li><a href="tel:{{@$user->mobile}}" class="btn bg-blue-700 btn-rounded btn-icon"><i class="icon-mobile3"></i></a></li>
                                                <li><a href="mailto:{{ @$user->email }}" class="btn bg-blue-700 btn-rounded btn-icon"><i class="icon-envelop4"></i></a></li>
                                            </ul>
                                        </div>

                                        <div class="panel panel-body no-border-top no-border-radius-top">
                                            <div class="form-group mt-5">
                                                <label class="text-semibold"> {{ trans('dash.full_name') }} </label>
                                                <span class="pull-right-sm"> {{ @$user->full_name }} </span>
                                            </div>

                                            <div class="form-group">
                                                <label class="text-semibold"> {{ trans('dash.email') }} </label>
                                                <span class="pull-right-sm"><a href="mailto:{{@$user->email}}"> {{@$user->email }} </a></span>
                                            </div>

                                            <div class="form-group">
                                                <label class="text-semibold"> {{ trans('dash.mobile') }} </label>
                                                <span class="pull-right-sm"> <a href="tel:{{@$user->mobile}}"> {{@$user->mobile}} </a> </span>
                                            </div>

                                            <div class="form-group">
                                                <label class="text-semibold"> {{ trans('dash.city') }} </label>
                                                <span class="pull-right-sm"> @if(@$user->city_id) {{ @$user->city->name_ar }} @else {{ trans('dash.no_selected') }} @endif </span>
                                            </div>



                                      
                                            <div class="form-group">
                                                <label class="text-semibold"> {{ trans('dash.created_at') }} </label>
                                                <span class="pull-right-sm"> {{ @$user->created_at->format('Y-m-d') }} </span>
                                            </div>

                                            <div class="form-group no-margin-bottom">
                                                <label class="text-semibold"> {{ trans('dash.time') }} </label>
                                                <span class="pull-right-sm"> {{ @$user->created_at->format('H:i') }} </span>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <table class="table table-hover">
                                        <tr>
                                            <td> {{ trans('dash.wallet') }} </td>
                                            <td> {{ @$user->wallet }} {{ trans('dash.sr') }} </td>
                                        </tr>
                                    </table>
                                    <br> <br>
                                    <form method="POST" action="{{ route('user_wallet') }}" style="border:1px solid black;padding:20px 30px">
                                        {{ Form::token() }}
                                        <input type="hidden" name="user_id" value="{{ @$user->id }}" />
                                        <label> {{ trans('dash.add_balance_2_wallet') }} </label>
                                        <input type="number" class="form-control" name="balance" required />
                                        <br>
                                        <center>
                                            <input type="submit" value="{{ trans('dash.add_now') }}" class="btn btn-primary" />
                                        </center>
                                    </form>

                                    <br><br>
                                    <form method="post" action="{{ route('send_notification_single') }}" style="border:1px solid black;padding:20px 30px">
                                        {{ Form::token() }}
                                        <input type="hidden" name="user_id" value="{{ @$user->id }}" />
                                        <label> {{ trans('dash.title') }} </label>
                                        <input type="text" required class="form-control" name="title" />
                                        <br>
                                        <label> {{ trans('dash.notify') }} </label>
                                        <textarea class="form-control"  name="notification" required></textarea>
                                        <br>
                                        <center>
                                            <button type="submit" class="btn btn-primary"> {{ trans('dash.do_send_notification_now') }} <i class="icon-arrow-left13 position-right"></i></button>
                                        </center>
                                    </form>


                                </div>

                            </div>
                        </div>

                        <div class="tab-pane has-padding" id="finished_orders">
                              <table class="table table-condensed table-hover datatable-highlight">
                                <thead>
                                    <tr>
                                        <th class="text-center"> # </th>
                                        <th class="text-center"> {{ trans('dash.client_name') }} </th>
                                        <th class="text-center"> {{ trans('dash.place_name') }} </th>
                                        <th class="text-center"> {{ trans('dash.order_price') }} </th>
                                        <th class="text-center"> {{ trans('dash.delivery_amout') }} </th>
                                        <th class="text-center"> {{ trans('dash.app_amout') }} </th>
                                        <th class="text-center"> {{ trans('dash.stars') }} </th>
                                        <th class="text-center"> {{ trans('dash.created_at') }} </th>
                                        <th class="text-center"> {{ trans('dash.actions') }} </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="row_5">
                                       <td>lorem ipsum</td>
                                       <td>lorem ipsum</td>
                                       <td>lorem ipsum</td>
                                       <td>lorem ipsum</td>
                                       <td>lorem ipsum</td>
                                       <td>lorem ipsum</td>
                                       <td>lorem ipsum</td>
                                       <td>lorem ipsum</td>
                                       <td>lorem ipsum</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <div class="tab-pane has-padding" id="prices">
                            <div class="row">


                                    <div class="panel panel-body ">
                                        <div class="col-lg-6">
                                            <h3> {{ trans('dash.client_outlay_on_app') }} </h3>
                                            <table class="table table-hover table-bordered ">
                                                <tr>
                                                    <td> {{ trans('dash.daily_outlay') }} </td>
                                                    <td>  {{ @$client_outlay_day }} {{ trans('dash.sr') }} </td>
                                                </tr>
                                                <tr>
                                                    <td> {{ trans('dash.weekly_outlay') }} </td>
                                                    <td>  {{ @$client_outlay_week }} {{ trans('dash.sr') }} </td>
                                                </tr>
                                                <tr>
                                                    <td> {{ trans('dash.monthly_outlay') }} </td>
                                                    <td>  {{ @$client_outlay_month }} {{ trans('dash.sr') }} </td>
                                                </tr>
                                                <tr>
                                                    <td> {{ trans('dash.yearly_outlay') }} </td>
                                                    <td>  {{ @$client_outlay_year }} {{ trans('dash.sr') }} </td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="col-lg-6">
                                            <h3> {{ trans('dash.profit_app_from_client') }} </h3>
                                            <table class="table table-hover table-bordered ">
                                                <tr>
                                                    <td> {{ trans('dash.earn_daily') }} </td>
                                                    <td>  {{ @$app_profit_day }} {{ trans('dash.sr') }} </td>
                                                </tr>
                                                <tr>
                                                    <td> {{ trans('dash.earn_weekly') }} </td>
                                                    <td>  {{ @$app_profit_week }} {{ trans('dash.sr') }} </td>
                                                </tr>
                                                <tr>
                                                    <td> {{ trans('dash.earn_monthly') }} </td>
                                                    <td>  {{ @$app_profit_month }} {{ trans('dash.sr') }} </td>
                                                </tr>
                                                <tr>
                                                    <td> {{ trans('dash.earn_yearly') }} </td>
                                                    <td>  {{ @$app_profit_year }} {{ trans('dash.sr') }} </td>
                                                </tr>
                                            </table>
                                        </div>

                                    </div>
                                </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
