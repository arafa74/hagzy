<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
     <meta charset="utf-8">
  <meta name="author" content="saudi table">
  <meta name="description" content="description content">
  <meta name="keywords" content="content">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Fav icon-->
  <link rel="shortcut icon" href="http://sauditable.com/table/public/assets/images/logo.png">
  <!-- style CSS-->
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/bootstrap.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/animate.min.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/owl.theme.default.min.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/validation.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/style.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/rtl.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/responsive.css">

    <title> Saudi Table </title>
</head>

<body class="home-style three rtl" data-spy="scroll" data-target=".navbar" data-offset="80">
    <!-- Preloader-->
    <div class="loader-wrapper">
        <div class="loader"></div>
    </div>
    <!-- Preloader end-->

    <!-- Nav Start-->
    <nav class="navbar navbar-expand-lg navbar-light theme-nav fixed-top navbar-page">
        <div class="container">
            <a class="navbar-brand" href="index.html">
               <img src="http://sauditable.com/table/public/assets/images/logo.png" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse default-nav" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto" id="mymenu">
                    <li class="nav-item">
                        <a class="nav-link active" href="http://sauditable.com/table">الرئيسية</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://sauditable.com/table">من نحن</a>
                    </li>
                    <!--<li class="nav-item">-->
                    <!--    <a class="nav-link" href="http://sauditable.com/table">المميزات</a>-->
                    <!--</li>-->
                    <li class="nav-item">
                        <a class="nav-link" href="http://sauditable.com/table">اللقطات</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://sauditable.com/table/app_terms">الشروط والأحكام</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">سياسة الخصوصية</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://sauditable.com/table">تحميل</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://sauditable.com/table/contact">تواصل معنا</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Nav end-->

<!-- privacy Section start-->
<section class="privacy-page">
    <div class="container">
        <h3 class="main-title"> سياسة الخصوصية لموقع وتطبيق (SAUDITABLE)</h3>
        <div class="all-content">
            <p class="description">
                مرحبًا بكم في تطبيق "سعودي تيبل".
                يعمل التطبيق كمنصة إلكترونية يستطيع من خلالها العملاء حجز طاولات المطاعم أون لاين، فنتيح من ناحية
                للمطاعم التسجيل بالتطبيق وتوفير الطاولات الخاصة بهم للحجز، ونتيح من ناحية أخرى للعملاء باستعراض المطاعم
                المسجلة لدينا واختيار وحجز الطاولات المتاحة بها، ونقدم خدمات التطبيق وفق الشروط والأحكام الآتية:

            </p>
            <p class="description"> 1- تهدف سياسة الخصوصية هذه إلى مساعدتك على فهم ماهية البيانات التي نقوم بجمعها ومعالجتها عنك، وأسباب جمعها ومعالجتها، وحدود ذلك، وحدود مشاركتها وضوابط الإفصاح عنها، وطريقة تحديثها وحذفها.</p>

            <p class="description">2-عند استخدامك خدماتنا فانك تثق في تطبيقنا وتحملنا مسئولية حماية بياناتك، ونحن ندرك حجم هذه المسئولية ونعمل بجدية على حماية بياناتك ونمنحك التحكم فيها طبقًا لممارسات هذه السياسة.</p>

            <p class="description"> 3- في نطاق هذه السياسة، تشير كلمة "التطبيق" أو "سعودي تيبل" أو "نحن" أو "نا الفاعلين" إلى تطبيق "سعودي تيبل Sauditable". كما يشير "أنت" أو "ضمائر المخاطب" إلى أي شخص يقوم بزيارة أو استخدام التطبيق أو أي من خدماته بأي شكل من الأشكال. </p>

            <p class="description"> 4- ) تسري هذه السياسة "سياسة الخصوصية" على التطبيق وكافة الخدمات المقدمة من خلاله وكافة الخدمات أو التطبيقات الأخرى التابعة لنا والتي تشاركنا تقديم الخدمات، وتعد جزء لا يتجزأ من الشروط والأحكام.</p>

            <p class="description"> 5- يرجى قراءة هذه السياسة جيدًا قبل استخدام التطبيق، وتحتوى السياسة على البنود الآتية:</p>
            <p>أولاً: البيانات التي نجمعها عنك</p>
            <p>ثانيًا: ملفات تعريف الارتباط (سياسة الكوكيز)</p>
            <p>ثالثًا: حدود استخدامنا لبياناتك</p>
            <p>رابعًا: حماية بيانات المستخدم</p>
            <p>خامسًا: مشاركة بياناتك</p>
            <p>سادسًا: التزامات الأطراف (المطعم، العميل)</p>
            <p>سابعًا: التعديلات</p>
            <p>ثامنًا: الاتصال بنا</p>
            <hr>

            <p class="description"> أولاً: البيانات التي نجمعها عنك</p>
            <p>قد نقوم بجمع واستخدام ومعالجة البيانات التالية عنك:</p>
            <p class="description"> 1- البيانات التي تقدمها لنا عند قيامك بالتسجيل عبر التطبيق، ويشمل ذلك على سبيل المثال لا الحصر الاسم ورقم الجوال والبريد الالكتروني وغيرها من البيانات.</p>
            <p class="description"> 2- البيانات التي يقدمها لنا العميل في استمارة حجز طاولات المطاعم أون لاين من خلالنا.</p>
            <p class="description"> 3- البيانات التي تقدمها لنا عند الإبلاغ عن مشكلة متعلقة باستخدام التطبيق أو مع المطعم أو العميل.</p>
            <p class="description"> 4- بيانات الدفع الخاصة بالمطعم اللازمة لسداد الاشتراكات والتي تشمل البيانات الشخصية أو البيانات التجارية والمعلومات المصرفية وحساب المطعم في فيزا.</p>
            <p class="description"> 5- البيانات التي تقدمها لنا عند حدوث مشكلة متعلقة بعملية الدفع الالكتروني للمبالغ المستحقة عليك.</p>
            <p class="description"> 6- البيانات التي تقدمها لنا عند الاتصال بنا أو عند الاتصال بالدعم الفني الخاص بنا.</p>
            <p class="description"> 7- البيانات التي نرى أنها ضرورية للتحقق من هويتك أو للتأكد من صحة وقانونية أي عمليات تقوم بها عبر التطبيق، بما في ذلك صورة من جواز سفرك، أو صورة من الهوية الوطنية الخاصة بك.</p>
            <p class="description"> 8- نحتفظ بكافة البيانات المتعلقة بتعاملاتك أو أنشطتك التي تقوم بها عبر التطبيق.</p>
            <p class="description"> 9- نحتفظ بالبيانات التي قمت بتقديمها لنا عند إجاباتك على الأسئلة أو الاستبيانات عبر التطبيق.</p>
            <p class="description"> 10- أنت تعلم وتقر بالموافقة أننا قد نجمع بياناتك في سجلاتنا الالكترونية أو سجلاتنا الورقية وفقًا لما نراه مناسبًا.</p>
            <br>

            <p class="description">ثانيًا: ملفات تعريف الارتباط</p>
            <p>ملفات تعريف الارتباط "الكوكيز" هي ملفات نصية صغيرة يتم تخزينها على جهازك بمجرد زيارتك للتطبيق، وهي لا تظهر في صورة برنامج ولا تحمل فيروسات أو تقنيات تجسس على الإطلاق، وتستخدم هذه الملفات للأغراض الآتية:</p>
            <p class="description">1- لتمكينك من استخدام خدماتنا على النحو الأمثل، وتقديم الأفضل لك دائمًا</p>
            <p class="description">2- لإنشاء حسابك أو ملفك الشخصي الذي تتواصل معنا من خلاله عبر التطبيق.</p>
            <p class="description">3- لمعالجة البيانات التي تقدمها عبر خدماتنا كالتأكد من أن بريدك الالكتروني أو رقم هاتفك نشط وصالح للاستخدام ومملوك لك.</p>
            <p class="description">4- لتمكين المطعم من الاشتراك في خدماتنا واختيار الباقة المناسبة له، وتقديم العروض على الاشتراكات.</p>
            <p class="description">5- لتمكين العميل من تقديم طلبات حجز الطاولات أون لاين من خلال التطبيق.</p>
            <p class="description">6- لتلقي الأسئلة والشكاوى والاستفسارات منك، والرد عليها.</p>
            <p class="description">7- لتلقي تعليقاتك وتقييمك للخدمة التي تلقيتها من خلال تطبيقنا، والرد عليها.</p>
            <p class="description">8- لمعالجة إجاباتك على الاستبيانات المطروحة عبر التطبيق والتي شاركت في الإجابة عليها.</p>
            <p class="description">9- لتزويدك بالمعلومات عن الخدمات التي تطلبها عبر تطبيقنا</p>
            <p class="description">10- لتزويدك بالمعلومات عن الخدمات التي نعتقد أنها تثير اهتمامك بما في ذلك العروض الخاصة بنا.</p>
            <p class="description">11- لأغراض العمل الداخلية مثل تحسين خدماتنا.</p>
            <p class="description">12- لتخصيص المحتوى والتوصيات والإعلانات التي نقدمها نحن والأطراف الثالثة لك.</p>
            <p class="description">13-لإدارة ومعالجة المسابقات والعروض الترويجية. </p>
            <p class="description">14- للاتصال بك بشأن الاتصالات الحكومية، ووفقًا لتقديرنا الخاص، بخصوص التغييرات في سياسة الخصوصية أو شروط الاستخدام أو أي من سياستنا الأخرى.</p>
            <p class="description">15- للامتثال للالتزامات التنظيمية والقانونية.</p>
            <p class="description">16- للأغراض التي يتم الكشف عنها في الوقت الذي تقدم فيه معلوماتك، بموافقتك، وطبقًا لسياسة الخصوصية هذه</p>
            <p class="description">17- نستخدم بياناتك لأغراض تنفيذ شروطنا وأحكامنا ودفع المبالغ المستحقة عليك.</p>
            <p class="description">18- نستخدم بياناتك لحل المشكلات ومنع الأنشطة غير القانونية بما في ذلك عمليات الاحتيال والقرصنة.</p>
            <p class="description">19- نستخدم بياناتك لتزويدك بكل جديد عن خدماتنا أو تعديلها أو تحسينها بما في ذلك التواصل معك الكترونيًا عبر رسائل البريد الالكتروني أو هاتفيًا عبر الاتصال برقم الهاتف الخاص بك.</p>
            <br>

            <p class="description">رابعًا: حماية بيانات المستخدم</p>
            <p class="description">1- يتعهد التطبيق بالاحتفاظ ببياناتك الشخصية أو المصرفية للمدة التي تقتضيها طبيعة المعاملة التي تقوم بها عبر التطبيق.</p>
            <p class="description">2- يتعهد التطبيق بعدم التعامل على بياناتك الشخصية لأغراض غير مصرح أو غير مسموح بها بمقابل أو بدون مقابل مع أي جهة أخرى إلا إذا كان ذلك مطلوبًا منا أو مصرحًا به بموجب الأنظمة والتعليمات ذات العلاقة، أو بموافقة كتابية مسبقة من المستخدم الذي تتعلق به المعلومات.</p>
            <p class="description">3- يتعهد التطبيق بالحفاظ على السجلات التي تحتوي على البيانات الشخصية للمستخدم أو أي سجلات للاتصالات الالكترونية، تكون في عهدتنا أو تحت سيطرتنا أو مع وكلائنا أو منسوبينا.</p>
            <p class="description">4- يتعهد التطبيق باتخاذ الخطوات المعقولة، لضمان أن البيانات الشخصية للمستخدم، والسجلات ذات الصلة، محمية بطريقة أمنية تناسب أهميتها.</p>
            <p class="description">5- نتعهد بحماية خصوصيتك في جميع الأوقات، وأننا لن نبيع أبدًا بياناتك الشخصية، أو نسمح بتداولها مع الغير</p>
            <br>

            <p class="description"> خامسًا: مشاركة بياناتك</p>
            <p class="description">1- أنت تعلم وتوافق على أن الانترنت ليس وسيلة آمنة، وأن سرية بياناتك الشخصية لا يمكن أن تكون مضمونة بشكل كامل.</p>
            <p class="description">2- نحن من جانبنا نتعهد في حدود المسموح به قانونًا بعدم الكشف عن بياناتك السرية أو استخدامها بشكل يتعارض وحقك في الخصوصية.</p>
            <p class="description">3- أنت تمنحنا الحق في السماح للموظفين لدينا بالتعامل مع بياناتكم الشخصية في حدود تقديم الخدمات.</p>
            <p class="description">4- يجوز لنا الكشف عن بيانات الشخصية لأي عضو في مجموعتنا؛ وهو ما يعني فروعنا ومؤسساتنا.</p>
            <p class="description">5- قد نكشف بيانات الشخصية لأطراف ثالثة:</p>

            <p>●	في حال قيامنا ببيع التطبيق أو أي أصول خاصة بنا، وسنقوم بالكشف عن بياناتك للمشتري الجديد.</p>
            <p>●	إذا كنا مطالبين بالإفصاح عن بياناتك الشخصية أو مشاركتها من أجل الامتثال لأي التزام قانوني أو من أجل فرض أو تطبيق شروطنا وأحكامنا أو أي اتفاقية أخرى.</p>
            <p>●	في حالة صدور حكم قضائي أو قرار من الجهات القضائية المختصة يلزمنا بذلك</p>
            <p>●	إذا كنا مطالبين بالإفصاح عن بياناتك الشخصية أو مشاركتها من أجل حماية الحقوق والملكية أو سلامة تطبيق "سعودي تيبل" أو موظفينا أو عملائنا أو الغير.</p>
            <p>●	ويشمل هذا الإفصاح تبادل البيانات مع الشركات والهيئات الأخرى للحماية من مخاطر الائتمان والاحتيال.</p>
            <br>

            <p class="description"> سادسًا: التزامات الأطراف (المطعم، العميل)</p>
            <p class="description">1- يلتزم أطراف هذه الاتفاقية بالمحافظة على سرية وخصوصية بياناتهم الشخصية، ويقرون وعلى مسئوليتهم الشخصية بأن إفصاح أيًا منهم عن البيانات الشخصية للطرف الآخر يكون على مسئوليته دون أي تدخل منا، ودون أي مسئولية علينا.</p>
            <p class="description">2- يعلم العميل ويوافق على أن التطبيق لا يسيطر إلا على البيانات التي يقوم بجمعها بنفسه أو التي يقوم العميل بتزويدنا بها، ولا نملك أي سيطرة على أي بيانات يتم تقديمها من العميل للمطاعم، لذلك يجب على العميل عدم الإفصاح للمطاعم عن أي بيانات شخصية غير لازمة لتقديم الخدمات، كما أن تقديم أي بيانات للمطاعم بشكل مباشر يتم على مسئولية العميل دون أدنى مسئولية قانونية أو تضامنية على التطبيق.</p>
            <p class="description">3- تلتزم المطاعم بالمحافظة على كافة البيانات التي يحصلون عليها عن العملاء سواء من خلالنا أو بواسطة العملاء أنفسهم، ويقر المطعم بعدم استغلال هذه البيانات في أي عمليات بيع أو تواصل أو تسويق مباشر أو غير مباشر أو أي استغلال للبيانات من أي نوع دون الحصول على موافقة العميل صاحب البيانات، وفي حالة إخلال المطعم بهذه الالتزامات فإنه يتحمل كافة المسئوليات والمطالبات والتعويضات دون أي مسئولية علينا.</p>
            <p class="description">4- يتعهد الأطراف بالمحافظة على أسرار التطبيق، ويُسأل كلاً منهم في مواجهتنا عن أي إخلال بسرية التطبيق وعملياته.</p>
            <p class="description">5- أنت تقر بتجنب إتباع أي وسيلة من شأنها أن تساعد على جمع البيانات عن المستخدمين الآخرين، بما في ذلك البريد الالكتروني وأرقام الجوال ووسائل الاتصال الأخرى الخاصة بهم.</p>
            <br>

            <p class="description">سابعًا: التعديلات</p>
            <p class="description">1- يحق لنا تعديل سياسة الخصوصية الخاصة بنا في أي وقت، وسنقوم بنشر إشعار عام عبر التطبيق بالتعديلات الجديدة، كما سنقوم بتحديث "تاريخ أخر تحديث" أعلى هذه الوثيقة.</p>
            <p class="description">2- يعد استمرارك في استخدام التطبيق بعد تحديث هذه السياسة موافقة صريحة منك على هذه التعديلات وقبولاً قانونيًا منك بالممارسات والأحكام الجديدة.</p>
            <p class="description">3- إذا كنت غير موافق على التعديلات والتحديثات الجديدة يجب عليك التوقف فورًا عن استخدام التطبيق.</p>
            <p class="description">4- يرجى مراجعة سياسة الخصوصية من حين لآخر للاطلاع على أحكامها وتحديثاتها.</p>
            <br>

            <p class="description">ثامنًا: الاتصال بنا</p>
            <p>تستطيع في أي وقت الاتصال بنا من خلال:</p>
            <p class="desription">الريد الالكتروني: <a href="#">email@email.com</a></p>
            <p class="desription">	رقم الهاتف: <a href="#">01112131415</a></p>

        </div>
    </div>
</section>
    <!-- privacy Section End-->


    <!-- Tap on Top-->
    <div class="tap-top">
        <div><i class="fa fa-angle-double-up"></i></div>
    </div>
    <!-- Tap on Ends-->

    <!-- Footer Section start-->
    <div class="copyright-section index-footer">
        <p>جميع الحقوق محفوظة لسنه 2019</p>
    </div>
    <!-- Footer Section End-->

    <!-- js files-->
   <script src="http://sauditable.com/table/public/assets/js/jquery-3.3.1.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/popper.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/bootstrap.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/owl.carousel.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/tilt.jquery.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/jquery.validate.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/additional-methods.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/contact.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/script.js"></script>
</body>

</html>