<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
     <meta charset="utf-8">
  <meta name="author" content="saudi table">
  <meta name="description" content="description content">
  <meta name="keywords" content="content">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Fav icon-->
  <link rel="shortcut icon" href="http://sauditable.com/table/public/assets/images/logo.png">
  <!-- style CSS-->
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/bootstrap.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/animate.min.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/owl.theme.default.min.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/validation.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/style.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/rtl.css">
  <link rel="stylesheet" href="http://sauditable.com/table/public/assets/css/responsive.css">

    <title> Saudi Table </title>
</head>

<body class="home-style three rtl" data-spy="scroll" data-target=".navbar" data-offset="80">
    <!-- Preloader-->
    <div class="loader-wrapper">
        <div class="loader"></div>
    </div>
    <!-- Preloader end-->

    <!-- Nav Start-->
    <nav class="navbar navbar-expand-lg navbar-light theme-nav fixed-top navbar-page">
        <div class="container">
            <a class="navbar-brand" href="{{ url('') }}">
               <img src="http://sauditable.com/table/public/assets/images/logo.png" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse default-nav" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto" id="mymenu">
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ url('') }}">الرئيسية</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('') }}">من نحن</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('') }}">المميزات</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('') }}">اللقطات</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('app_terms') }}">الشروط والأحكام</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('app_privacy') }}">سياسة الخصوصية</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('') }}">تحميل</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/contact') }}">تواصل معنا</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Nav end-->

<!-- privacy Section start-->
<section class="privacy-page">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="main-title"> تواصل معنا</h3>
                <div class="section-contact">
                    <div class="colors o-auto">
                      <a href="tel:009660507726718">0507726718<i class="fa fa-phone"></i> </a><br>
                      <a href="https://api.whatsapp.com/send?phone=009660507726718">0507726718<i class="fa fa-whatsapp"></i> </a><br>
                      <a href="mailto:Info@sauditable.com">Info@sauditable.com<i class="fa fa-envelope"></i> </a><br>
                      <a href="#">الرياض، المملكة العربية السعودية<i class="fa fa-map-marker"></i> </a>
                    </div>
                    <div class="social">
                        <ul>
                            <li><a href="https://twitter.com/sauditable?s=08"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/sauditable1?r=nametag"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h3 class="main-title"> تواصل مع الاداره</h3>
                <div class="section-contact">
                    <div id="alert-not-found" class="alert alert-danger hide-register-alert ">
                            <ul class="list-unstyled">

                            </ul>
                        </div>
                    <form action="" method="post" id="registerForm" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="input-block">
                                <input type="text" name="title" placeholder="العنوان" class="form-control"
                                >
                            </div>
                            <textarea name="details" class="message-box form-control"
                                      placeholder="الرساله" rows="5"></textarea>
                            <button type="submit" class="btn-send btn">ارسال</button>
                        </form>
                </div>
            </div>
        </div>
    </div>
</section>


    <!-- privacy Section End-->


    <!-- Tap on Top-->
    <div class="tap-top">
        <div><i class="fa fa-angle-double-up"></i></div>
    </div>
    <!-- Tap on Ends-->

    <!-- Footer Section start-->
    <div class="copyright-section index-footer">
        <p>جميع الحقوق محفوظة لسنه 2019</p>
    </div>
    <!-- Footer Section End-->

    <!-- js files-->
   <script src="http://sauditable.com/table/public/assets/js/jquery-3.3.1.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/popper.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/bootstrap.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/owl.carousel.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/tilt.jquery.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/jquery.validate.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/additional-methods.min.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/contact.js"></script>
  <script src="http://sauditable.com/table/public/assets/js/script.js"></script>
  
 
    <script>
        $(document).ready(function () {
            $(".hide-register-alert").css('display', 'none');

            $("#registerForm").on('submit', function (e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                e.preventDefault();
                $.ajax({
                    url: '{{ route('post.contact_us') }}',
                    type: 'POST',
                    data: $(this).serialize(),
                    success: function (data) {
                        if ($.isEmptyObject(data.error)) {
                            window.location.href = "{{ url('contact')  }}";
                        } else {
                            console.log(data.error);
                            printErrorMsg(data.error);
                        }
                    }
                });

            });

            function printErrorMsg(msg) {
                $(".hide-register-alert").css('display', 'block');
                $(".hide-register-alert").find("ul").html('');
                $.each(msg, function (key, value) {
                    $(".hide-register-alert").find("ul").append('<li>' + value + '</li>');
                });
                $(".hide-register-alert").delay(3000).fadeOut();
            }
        });
    </script>

  
</body>

</html>