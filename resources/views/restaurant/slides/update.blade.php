@extends('dashboard.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>

    <!-- /theme JS files -->

@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">

            <!-- Basic layout-->
            <form action="{{ route('slide_update') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                {{ Form::token() }}

                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"> {{ trans('تعديل معرض الصور') }} </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"> حذف صور من المعرض</label>
                            <div class="col-lg-9">
                                @foreach($slides as $image)
                                    <div class="col-md-3">
                                        <img style="width: 100%;height:200px;" src="{{ Request::root() }}/storage/app/slides/200x150/{{$image->image}}" alt="" class="img-responsive">
                                        <input value="{{$image->id}}" name="image_id[]" type="checkbox" class="form-control" style="position: absolute;top: -6px;right: 15px;width: 19px;">
                                    </div>
                                @endforeach
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3 control-label"> اضافه صور للمعرض</label>
                            <div class="col-lg-9">
                                <input type="file" class="file-styled" name="images[]" multiple>
                                <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                            </div>
                        </div>
                        </div>


                        <div class="text-right">
                            <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.update_and_forword_2_list') }} " />
                            <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.update_and_come_back') }} " />
                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->

        </div>

    </div>





@endsection
