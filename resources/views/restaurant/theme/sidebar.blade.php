<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-default">
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user-material">
			<div class="category-content">
				<div class="sidebar-user-material-content">
					<a href="#"><img src="{{ auth()->user()->image_url }}" class="img-circle img-responsive" alt=""></a>
					<h6> {{ auth()->user()->full_name }} </h6>
					{{--<span class="text-size-small"> {{ auth()->user()->full_name }} </span>--}}
				</div>

				<div class="sidebar-user-material-menu">
					<a href="#user-nav" data-toggle="collapse"><span>{{ trans('dash.my_account') }}</span> <i class="caret"></i></a>
				</div>
			</div>

			<div class="navigation-wrapper collapse" id="user-nav">
				<ul class="navigation">
					<li><a href="{{ route('restaurant_profile') }}"><i class="icon-user-plus"></i> <span> {{ trans('dash.my_profile') }} </span></a></li>
					<li class="divider"></li>
					<li><a href="{{ route('restaurant_logout') }}"><i class="icon-switch2"></i> <span> {{ trans('dash.logout') }} </span></a></li>
				</ul>
			</div>
		</div>
		<!-- /user menu -->


		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">

					<!-- Main -->
					<li class="navigation-header"><span> {{ trans('dash.main') }} </span> <i class="icon-menu" title="Main pages"></i></li>
					<li {{ request()->route()->getName() === 'restaurant_home' ? ' class=active' : '' }} ><a href="{{ route('restaurant_home') }}"><i class="icon-home4"></i> <span> {{ trans('dash.home') }} </span></a></li>



					<li>
						<a href="#"><i class="icon-city"></i> <span> {{ trans('الاطباق') }} </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'restaurant_dishes' ? ' class=active' : '' }}><a href=" {{ route('restaurant_dishes') }} " > عرض </a></li>
							<li {{ request()->route()->getName() === 'restaurant_dishes_create' ? ' class=active' : '' }}><a href=" {{ route('restaurant_dishes_create') }} " > اضافه جديد </a></li>
						</ul>
					</li>

					<li>
						<a href="#"><i class="icon-city"></i> <span> العروض </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'restaurant_offers' ? ' class=active' : '' }}><a href=" {{ route('restaurant_offers') }} " > العروض </a></li>
							<li {{ request()->route()->getName() === 'restaurant_offer_create' ? ' class=active' : '' }}><a href=" {{ route('restaurant_offer_create') }} " > اضافه عرض جديد </a></li>
						</ul>
					</li>

					<li>
						<a href="#"><i class="icon-city"></i> <span>الحجوزات </span></a>
						<ul>
							<li {{ request()->route()->getName() === 'restaurant_reservations' ? ' class=active' : '' }}><a href=" {{ route('restaurant_reservations') }} " >  عرض  </a></li>
						</ul>
					</li>

					<!-- /main -->






					<!-- /others -->

				</ul>
			</div>
		</div>
		<!-- /main navigation -->

	</div>
</div>
<!-- /main sidebar -->
