@extends('restaurant.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/inputs/touchspin.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/switch.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_validation.js"></script>

    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
    <!-- /theme JS files -->
@endsection

@section('content')
    <!-- Account settings -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title"> {{ trans('dash.change_password') }} </h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <form action="{{ route('update_restaurant_profile') }}" method="post" >
                {{ Form::token() }}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label> {{ trans('dash.old_password') }} </label>
                            <input type="password"  name="old_password" class="form-control" placeholder="{{ trans('dash.old_password') }}" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label> {{ trans('dash.new_password') }} </label>
                            <input type="password" name="password" placeholder="{{ trans('dash.new_password') }}" class="form-control" required>
                        </div>

                        <div class="col-md-6">
                            <label> {{ trans('dash.confirm_new_password') }} </label>
                            <input type="password" name="password_confirmation" placeholder="{{ trans('dash.confirm_new_password') }}" class="form-control" required>
                        </div>
                    </div>
                </div>


                <div class="text-right">
                    <button type="submit" class="btn btn-primary"> {{ trans('dash.yalla_change_password') }} <i class="icon-arrow-left13 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /account settings -->
@endsection
