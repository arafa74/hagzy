@extends('restaurant.layout')

@section('script')


    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/form_select2.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <ul class="nav nav-tabs nav-tabs-highlight">
                    <li class="active"><a href="#details-tab" data-toggle="tab"><i class="icon-menu7 position-left"></i> تفاصيل الحجز </a></li>
                </ul>
                <div class="tabbable nav-tabs-vertical nav-tabs-left">

                    <div class="tab-content">
                        <div class="tab-pane active has-padding" id="details-tab">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> تفاصيل الحجز </h5>
                                            <div class="heading-elements">
                                                <ul class="icons-list">
                                                    <li><a data-action="collapse"></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                            <table class="table table-hover table-bordered table-hover">
                                                <tr class="success">
                                                    <td> حاله الحجز </td>
                                                    <td> {{ $reservation->status }} </td>
                                                </tr>
                                                <tr class="success">
                                                    <td> اسم المطعم </td>
                                                    <td> {{ $reservation->restaurant->full_name }} </td>
                                                </tr>
                                                <tr class="success">
                                                    <td> اسم العميل </td>
                                                    <td> {{ $reservation->user->full_name }} </td>
                                                </tr>
                                                <tr class="success">
                                                    <td> عدد الافراد </td>
                                                    <td> {{$reservation->clientsNum}} </td>
                                                </tr>
                                                <tr class="success">
                                                    <td> اسم عميل  </td>
                                                    @if($reservation->name)
                                                        <td> {{$reservation->name}}  </td>
                                                    @else
                                                        <td> {{$reservation->user->full_name}}  </td>
                                                    @endif
                                                </tr>

                                                <tr class="success">
                                                    <td> البريد الالكترونى  </td>
                                                    @if($reservation->email)
                                                        <td> {{$reservation->email}}  </td>
                                                    @else
                                                        <td> {{$reservation->user->email}}  </td>
                                                    @endif
                                                </tr>

                                                <tr class="success">
                                                    <td> رقم الجوال  </td>
                                                    @if($reservation->mobile)
                                                        <td> {{$reservation->mobile}}  </td>
                                                    @else
                                                        <td> {{$reservation->user->mobile}}  </td>
                                                    @endif
                                                </tr>

                                                <tr class="success">
                                                    <td> {{ trans('dash.date') }} </td>
                                                    <td> {{ $reservation->created_at->format('Y-m-d') }} </td>
                                                </tr>
                                                <tr class="success">
                                                    <td> {{ trans('dash.time') }} </td>
                                                    <td> {{ $reservation->created_at->format('H:i') }} </td>
                                                </tr>
                                            </table>
                                            <br>
                                            @if($reservation->status == "wait")
                                            <center>
                                                <a onclick="sweet_confirm_pay()" id="confirm_pay_btn" href="#" class="btn btn-success"> {{ trans('dash.do_reservation_confirm') }} </a>
                                            </center>
                                            <br> <br>
                                            <center>
                                                <a onclick="sweet_confirm_refuse()" id="confirm_refuse_btn" href="#" class="btn btn-danger"> {{ trans('dash.do_reservation_refuse') }} </a>
                                            </center>
                                            <br>
                                            @endif
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> قبول ورفض الحجز </h5>
                                            <div class="heading-elements">
                                                <ul class="icons-list">
                                                    <li><a data-action="collapse"></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                            <form action="{{ route("restaurant_reservation_update") }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                {{ Form::token() }}
                                                <input type="hidden" name="reservation_id" value="{{ $reservation->id }}">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label display-block "> حاله الحجز </label>
                                                    <div class="col-lg-9">
                                                        <select name="status" class="select-border-color border-warning" >
                                                            <optgroup label="{{ trans('اختر ') }}">
                                                                    <option value="accepted"> قبول </option>
                                                                    <option value="canceled"> رفض </option>

                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="text-right">
                                                    <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.added_and_forward_to_list') }} " />
                                                    <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.added_and_come_back') }} " />
                                                </div>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
