@extends('dashboard.layout')

@section('script')
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/user_pages_team.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title"> {{ trans('بيانات المستخدم') }} </h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <ul class="nav nav-tabs nav-tabs-highlight">
                        <li class="active"><a href="#details-tab" data-toggle="tab"><i class="icon-menu7 position-left"></i> {{ trans('البيانات') }} </a></li>
                    </ul>
                    <div class="tabbable nav-tabs-vertical nav-tabs-left">

                        <div class="tab-content">

                            <div class="tab-pane active has-padding" id="details-tab">
                                <div class="row">


                                    <div class="col-lg-6">
                                        <div class="content-group">
                                            <div class="panel-body bg-blue border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
                                                <div class="content-group-sm">
                                                    <h5 class="text-semibold no-margin-bottom">
                                                        {{ $ad->title }}
                                                    </h5>
                                                    <span class="display-block"> {{ trans('صور الاعلان') }} </span>
                                                </div>

                                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

                                                    <div class="carousel-inner">
                                                        @foreach($ad->ad_image as $image)
                                                        <div class="carousel-item active">
                                                            <img class="d-block w-100" src="{{ url('storage/app/ad_images/50x50') .'/'.$image->image }}" alt="First slide">
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </div>

                                                {{--<a href="{{ $ad->imageurlorg }}" data-popup="lightbox">--}}
                                                    {{--<img src=" {{ $ad->imageurl }} " class="img-circle" alt="">--}}
                                                {{--</a>--}}

                                                <ul class="list-inline no-margin-bottom">
                                                    <li><a href="tel:{{$ad->user->mobile}}" class="btn bg-blue-700 btn-rounded btn-icon"><i class="icon-mobile3"></i></a></li>
                                                    <li><a href="mailto:{{ $ad->user->email }}" class="btn bg-blue-700 btn-rounded btn-icon"><i class="icon-envelop4"></i></a></li>
                                                </ul>
                                            </div>

                                            <div class="panel panel-body no-border-top no-border-radius-top">
                                                <div class="form-group mt-5">
                                                    <label class="text-semibold"> {{ trans('العنوان') }} </label>
                                                    <span class="pull-right-sm"> {{ $ad->title }} </span>
                                                </div>

                                                <div class="form-group">
                                                    <label class="text-semibold"> {{ trans('التفاصيل') }} </label>
                                                    <span class="pull-right-sm"> {{$ad->description }} </span>
                                                </div>

                                                <div class="form-group">
                                                    <label class="text-semibold"> {{ trans('الخدمه') }} </label>
                                                    <span class="pull-right-sm"> {{$ad->service->name_ar }} </span>
                                                </div>

                                                <div class="form-group">
                                                    <label class="text-semibold"> {{ trans('القسم') }} </label>
                                                    <span class="pull-right-sm"> {{$ad->category->name_ar }} </span>
                                                </div>

                                                <div class="form-group">
                                                    <label class="text-semibold"> {{ trans('تاريخ البدايه') }} </label>
                                                    <span class="pull-right-sm"> {{$ad->start_date}} </a> </span>
                                                </div>

                                                <div class="form-group">
                                                    <label class="text-semibold"> {{ trans('تاريخ النهايه') }} </label>
                                                    <span class="pull-right-sm"> {{ $ad->end_date }}</span>
                                                </div>

                                                <div class="form-group">
                                                    <label class="text-semibold"> {{ trans('dash.created_at') }} </label>
                                                    <span class="pull-right-sm"> {{ $ad->created_at->format('Y-m-d') }} </span>
                                                </div>

                                                <div class="form-group no-margin-bottom">
                                                    <label class="text-semibold"> {{ trans('وقت التسجيل') }} </label>
                                                    <span class="pull-right-sm"> {{ $ad->created_at->format('H:i') }} </span>
                                                </div>

                                            </div>
                                            <br> <Br>
                                            {{--<a href="{{ route('user_edit',['id' => $ad->id ]) }}" target="_blank" class="btn btn-primary">{{ trans('dash.edit') }} <i class="icon-pencil3"></i></a>--}}
                                            {{--<a onclick="sweet_delete( ' {{ route('ad_delete').'/'.$ad->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $ad->id }} )" name="forward" class="btn btn-danger" > {{ trans('dash.delete') }} <i class="icon-database-remove"></i> </a>--}}

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
