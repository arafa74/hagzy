@extends('restaurant.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/form_layouts.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/form_select2.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/ui/ripple.min.js"></script>

    <!-- /theme JS files -->

@endsection

@section('content')

<div class="row">
    <div class="col-md-6">

        <!-- Basic layout-->
        <form action="{{ route('restaurant_offer_store') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ Form::token() }}
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('اضافه جديد') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-3"> تفاصيل <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="5" cols="5" name="description" class="form-control" required="required">  </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">نسبه الخصم <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input rows="5" cols="5" name="discount" class="form-control" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">عدد الافراد <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input rows="5" cols="5" name="personNum"  class="form-control" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">بدايه العرض <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input rows="5" cols="5" name="from"  class="form-control" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">نهايه العرض <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input rows="5" cols="5" name="to" class="form-control" required="required">
                        </div>
                    </div>

                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.add_forword_2_places') }} " />
                        <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.add_and_come_back') }} " />
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->

    </div>




@endsection
