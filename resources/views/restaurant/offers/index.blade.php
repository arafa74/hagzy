@extends('restaurant.layout')

@section('script')
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/datatables_advanced.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')


<div class="panel panel-flat tb_padd">
    <div class="panel-heading">
        <h5 class="panel-title"> العروض </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>


    <table class="table table-condensed table-hover datatable-highlight">
        <thead>
            <tr>
                <th class="text-center"> {{ trans('تفاصيل العرض') }} </th>
                <th class="text-center"> عدد الافراد </th>
                <th class="text-center"> المطعم </th>
                <th class="text-center"> تاريخ بدايه العرض </th>
                <th class="text-center"> تاريخ نهايه العرض </th>
                <th class="text-center"> {{ trans('dash.actions') }} </th>
            </tr>
        </thead>
        <tbody>
            @forelse($offers as $offer)
            <tr id="row_{{ $offer->id }}">
                <td> {{ str_limit($offer->description , 50) }} </td>
                <td> {{ str_limit($offer->personNum , 50) }} </td>
                <td> {{ $offer->user->full_name }} </td>
                <td> {{ $offer->from }} </td>
                <td> {{ $offer->to }} </td>
                <td class="text-center">
                    <a href="{{ route('restaurant_offer_edit',['id' => $offer->id ]) }}" class="btn btn-primary"> <i class="icon-pencil3"></i> </a>
                    <a onclick="sweet_delete( ' {{ route('restaurant_offer_delete').'/'.$offer->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $offer->id }} )" class="btn btn-danger" > <i class="icon-database-remove"></i> </a>
                </td>
            </tr>

            @empty
            @endforelse

        </tbody>
    </table>
</div>

<script>
    function sweet_delete($url,$message,$user_id)
    {
        $( "#row_"+$user_id ).css('background-color','#000000').css('color','white');
        swal({
        title: $message,
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url:$url
            });
            swal({
                title: "{{ trans('alert') }}",
                text: "{{ trans('dash.deleted_successfully') }}",
                icon: "success",
                timer:1000
            });
            $( "#row_"+$user_id ).hide(1000);
        }else{
            $( "#row_"+$user_id ).removeAttr('style');
        }
        });
    }
</script>

@endsection
