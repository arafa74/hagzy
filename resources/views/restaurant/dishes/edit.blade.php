@extends('restaurant.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_select2.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>

    <!-- /theme JS files -->

@endsection

@section('content')

<div class="row">
    <div class="col-md-6">

        <!-- Basic layout-->
        <form action="{{ route('restaurant_dishes_update') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ Form::token() }}
            <input type="hidden" name="dish_id" value="{{ $dish->id }}" />
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"> تعديل بيانات طبق </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">


                    <div class="form-group">
                        <label class="col-lg-3 control-label">اسم الطبق</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="name" placeholder="اسم الطبق" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">السعر</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="price" placeholder="السعر" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">الوصف</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="description" placeholder="الوصف" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> اخترالتصنيف </label>
                        <div class="col-lg-9">
                            <select name="category_id" class="select-border-color border-warning" >
                                <optgroup label="اختر التصنيف">
                                    @foreach ($categories as $category)
                                        @if($index->$category)
                                        @endif
                                        <option value="{{ $category->id }}" @if($category->country_id == $category->id) selected @endif> {{ $category->name_ar.' - '.$category->name_en }} </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"> {{ trans('dash.image') }}</label>
                        <div class="col-lg-6">
                            <input type="file" class="file-styled" name="image" >
                        </div>
                        <div class="col-lg-3">
                            <a href="{{ $dish->imageurlorg }}" data-popup="lightbox">
                                <img src="{{ $dish->imageurl }}" height="80" width="90" />
                            </a>
                        </div>
                    </div>



                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.update_and_forword_2_list') }} " />
                        <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.update_and_come_back') }} " />
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->

    </div>

</div>

</div>

@endsection
