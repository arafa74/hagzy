@extends('restaurant.layout')

@section('script')
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/user_pages_team.js"></script>

    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/datatables_advanced.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')


<div class="panel panel-flat tb_padd">
    <div class="panel-heading">
        <h5 class="panel-title"> {{ trans('الاطباق والوجبات') }} </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>
    
    <table class="table table-condensed table-hover datatable-highlight">
        <thead>
            <tr>
                <th class="text-center"> # </th>
                <th class="text-center"> اسم المطعم </th>
                <th class="text-center"> {{ trans('اسم الطبق') }} </th>
                <th class="text-center"> السعر </th>
                <th class="text-center"> التصنيف </th>
                <th class="text-center"> {{ trans('dash.image') }} </th>
                <th class="text-center"> {{ trans('dash.actions') }} </th>
            </tr>
        </thead>
        <tbody>
            @forelse($dishes as $dish)
            <tr id="row_{{ $dish->id }}">
                <td> {{ $dish->id }} </td>
                <td> {{ $dish->user->full_name }} </td>
                <td> {{ $dish->name }} </td>
                <td> {{ $dish->price }} </td>
                <td> {{ $dish->category->name_ar }} </td>
                <td>
                    <a href="{{ $dish->imageurlorg }}" data-popup="lightbox">
                        <img src="{{ $dish->imageurl }}" height="100" width="100" />
                    </a>
                </td>
                <td class="text-center">
                    <a href="{{ route('restaurant_dishes_view',['id' => $dish->id ]) }}" class="btn btn-primary"> <i class="icon-touch"></i> </a>
                    <a href="{{ route('restaurant_dishes_edit',['id' => $dish->id ]) }}" class="btn btn-primary"> <i class="icon-pencil"></i> </a>
                    <a onclick="sweet_delete( ' {{ route('restaurant_dishes_delete').'/'.$dish->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $dish->id }} )" class="btn btn-danger" > <i class="icon-database-remove"></i> </a>
                </td>
            </tr>

            @empty
            @endforelse

        </tbody>
    </table>
</div>

@endsection
