@extends('restaurant.layout')

@section('script')
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/user_pages_team.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <ul class="nav nav-tabs nav-tabs-highlight">
                    <li class="active"><a href="#details-tab" data-toggle="tab"><i class="icon-menu7 position-left"></i> تفاصيل الطبق </a></li>
                </ul>
                <div class="tabbable nav-tabs-vertical nav-tabs-left">

                    <div class="tab-content">

                        <div class="tab-pane active has-padding" id="details-tab">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> تفاصيل الطبق </h5>
                                            <div class="heading-elements">
                                                <ul class="icons-list">
                                                    <li><a data-action="collapse"></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                            <table class="table table-hover table-bordered table-hover">
                                                <tr class="success">
                                                    <td> اسم الطبق </td>
                                                    <td> {{ $dish->name }} </td>
                                                </tr>
                                                <tr class="success">
                                                    <td> اسم المطعم </td>
                                                    <td> {{ $dish->user->full_name }} </td>
                                                </tr>
                                                <tr class="success">
                                                    <td> وصف الطبق </td>
                                                    <td> {{$dish->description}} </td>
                                                </tr>

                                                <tr class="success">
                                                    <td> سعر الطبق </td>
                                                    <td> {{$dish->price}} </td>
                                                </tr>
                                                <tr class="danger">
                                                    <td> {{ trans('dash.image') }} </td>
                                                    <td>
                                                        <a href="{{ $dish->imageurlorg }}" data-popup="lightbox">
                                                            <img src="{{ $dish->imageurl }}" height="100" width="100" />
                                                        </a>
                                                    </td>
                                                </tr>

                                            </table>
                                            <br>
                                            @if($dish->status == "wait")
                                            <center>
                                                <a onclick="sweet_confirm_pay()" id="confirm_pay_btn" href="#" class="btn btn-success"> {{ trans('dash.do_dish_confirm') }} </a>
                                            </center>
                                            <br> <br>
                                            <center>
                                                <a onclick="sweet_confirm_refuse()" id="confirm_refuse_btn" href="#" class="btn btn-danger"> {{ trans('dash.do_dish_refuse') }} </a>
                                            </center>
                                            <br>
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
