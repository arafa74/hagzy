@extends('dashboard.layout')

@section('script')
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/datatables_advanced.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')

<div class="panel panel-flat tb_padd">
    <div class="panel-heading">
        <h5 class="panel-title"> {{ $table_name }} </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <a class="btn btn-primary" href="{{ route('create_user') }}"> {{ trans('dash.add_new_users') }} </a>

    <table class="table table-bordered table-hover datatable-highlight">
        <thead>
            <tr>
                <th class="text-center"> {{ trans('dash.full_name') }} </th>
                <th class="text-center"> {{ trans('dash.email') }} </th>
                <th class="text-center"> {{ trans('dash.image') }} </th>
                <th class="text-center"> {{ trans('dash.city') }} </th>
                <th class="text-center"> {{ trans('dash.created_at') }} </th>
                <th class="text-center"> {{ trans('dash.actions') }} </th>
            </tr>
        </thead>
        <tbody>
            @forelse($users as $user)
            <tr id="row_{{ $user->id }}">
                <td> {{ $user->full_name }} </td>
                <td> {{ $user->email}} </td>
                <td> <img width="100px" src="{{ $user->imageurl }}" /> </td>
                <td> @if($user->city_id != null) {{$user->city->name_ar }} @endif </td>
                <td> {{ $user->created_at->format('Y-m-d H:i') }} </td>
                <td class="text-center" >
                    <a onclick="sweet_delete( ' {{ route('user_force_delete').'/'.$user->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $user->id }} ,'warning','delete' ,'{{ trans('dash.deleted_successfully') }}')" class="btn btn-danger" title="Force Delete"> <i class="icon-database-remove"></i> </a>
                    <a onclick="sweet_delete( ' {{ route('user_restore').'/'.$user->id }} ' , '{{ trans('dash.restore_confirm_message') }}' ,{{ $user->id }} ,'info' , 'restore','{{ trans('dash.restored_successfully') }}' )" class="btn btn-success" title="restore"> <i class="icon-forward"></i> </a>
                </td>
            </tr>
            @empty
            @endforelse

        </tbody>
    </table>
</div>

<script>
    function sweet_delete($url,$message,$user_id,$icon,$status,$restore_msg)
    {
        if($status == "delete"){
            $( "#row_"+$user_id ).css('background-color','red').css('color','white');
        }else{
            $( "#row_"+$user_id ).css('background-color','rgb(25, 62, 255)').css('color','white');
        }
        swal({
        title: $message,
        {{--  text: "Once deleted, you will not be able to recover this imaginary file!",  --}}
        icon: $icon,
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url:$url
            });
            swal({
                title: "{{ trans('alert') }}",
                text: $restore_msg,
                icon: "success",
                timer:1000
            });
            $( "#row_"+$user_id ).hide(1000);
        }else{
            $( "#row_"+$user_id ).removeAttr('style');
        }
        });
    }
</script>


@endsection
