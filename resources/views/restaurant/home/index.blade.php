@extends('restaurant.layout')

@section('script')
	<!-- Theme JS files -->

    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/user_pages_team.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/') }}/js/core/app.js"></script>


    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
	<!-- /theme JS files -->
@endsection

@section('content')



<!-- Dashboard content -->
<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-flat cards" style="">

                <div class="panel-body" >
                    <!-- Quick stats boxes -->
                    <div class="row">
                        <div class="col-lg-4">
                            <a href="#">
                                <div class="panel bg-grey-400" style="background-color: #9b59b6;color: #fff;">
                                    <div class="panel-body">
                                        <div class="heading-elements">
                                            <span class="heading-text badge bg-grey-800"> {{ trans('dash.active') }} </span>
                                        </div>
                                        <h3 class="no-margin"> {{ $reservations_counter }} </h3>
                                        <h5> الحجوزات </h5>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-4">
                            <a href="#">
                                <div class="panel bg-grey-400" style="background-color: #4A148C;color: #fff;">
                                    <div class="panel-body">
                                        <div class="heading-elements">
                                            <span class="heading-text badge bg-grey-800"> {{ trans('dash.active') }} </span>
                                        </div>
                                        <h3 class="no-margin"> {{ $Dishes_counter }} </h3>
                                        <h5> الاطباق </h5>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-4">
                            <a href="#">
                                <div class="panel bg-grey-400" style="background-color: #8f5536;color: #fff;">
                                    <div class="panel-body">
                                        <div class="heading-elements">
                                            <span class="heading-text badge bg-grey-800"> {{ trans('dash.active') }} </span>
                                        </div>
                                        <h3 class="no-margin"> {{ $offers_counter }} </h3>
                                        <h5> العروض </h5>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-4">
                            <a href="#">
                                <div class="panel bg-grey-400" style="background-color: #50fefe;color: #fff;">
                                    <div class="panel-body">
                                        <div class="heading-elements">
                                            <span class="heading-text badge bg-grey-800"> {{ trans('dash.active') }} </span>
                                        </div>
                                        <h3 class="no-margin"> {{ $rates_counter }} </h3>
                                        <h5> التقيمات</h5>
                                    </div>
                                </div>
                            </a>
                        </div>



                    </div>
                    <!-- /quick stats boxes -->
                </div>
        </div>


    </div>

    <div class="col-lg-12">


    </div>
</div>
<!-- /dashboard content -->


@endsection

