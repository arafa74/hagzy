@extends('dashboard.layout')

@section('script')

    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/user_pages_team.js"></script>

    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/datatables_advanced.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')


    <div class="panel panel-flat tb_padd">
        <div class="panel-heading">
            <h5 class="panel-title">  </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <table class="table table-condensed table-hover datatable-highlight">
            <thead>
            <tr>
                <th class="text-center"> # </th>
                <th class="text-center"> {{ trans('اسم المطعم') }} </th>
                <th class="text-center"> {{ trans('اسم العميل') }} </th>
                <th class="text-center"> {{ trans('تاريخ الحجز') }} </th>
                <th class="text-center"> {{ trans('عدد الافراد') }} </th>
                <th class="text-center"> {{ trans('حاله الحجز') }} </th>
                <th class="text-center"> {{ trans('الاعدادات') }} </th>
            </tr>
            </thead>
            <tbody>
            @forelse($reservations as $reservation)
                <tr id="row_{{ $reservation->id }}">
                    <td> {{ $reservation->id }} </td>
                    <td> {{ str_limit($reservation->restaurant->full_name,30,'...') }} </td>
                    <td> {{ $reservation->user->full_name }} </td>
                    <td> {{ $reservation->reservation_day }} </td>
                    <td> {{ $reservation->clientsNum }} </td>
                    <td> {{ $reservation->status }} </td>
                    <td class="text-center">
                        <a href="{{ route('reservation_view',['id' => $reservation->id ]) }}" class="btn btn-primary"> <i class="icon-touch"></i> </a>
                        <a onclick="sweet_delete( ' {{ route('reservation_delete').'/'.$reservation->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $reservation->id }} )" class="btn btn-danger" > <i class="icon-database-remove"></i> </a>
                    </td>
                </tr>

            @empty
            @endforelse

            </tbody>
        </table>
        <br>
    </div>


@endsection
