@extends('dashboard.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_select2.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>

    <!-- /theme JS files -->

@endsection

@section('content')

<div class="row">
    <div class="col-md-6">

        <!-- Basic layout-->
        <form action="{{ route('offer_update') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ Form::token() }}
            <input type="hidden" name="offer_id" value="{{ $offer->id }}" />
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('تعديل عرض') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-3"> تفاصيل العرض باللغه العربيه<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="5" cols="5" name="description_ar" class="form-control" required="required"> {{ $offer->description_ar }} </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3"> تفاصيل العرض باللغه الانجليزيه <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="5" cols="5" name="description_en" class="form-control" required="required"> {{ $offer->description_en }} </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> {{ trans('dash.category') }} </label>
                        <div class="col-lg-9">
                            <select name="category_id" class="select-border-color border-warning" >
                                <optgroup label="{{ trans('اختر القسم') }}">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" @if($offer->category_id == $category->id) selected @endif> {{ $category->name_ar.' - '.$category->name_en }} </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"> {{ trans('dash.image') }}</label>
                        <div class="col-lg-9">
                            <input type="file" class="file-styled" name="image">
                            <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                        </div>
                    </div>


                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.update_and_forword_2_list') }} " />
                        <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.update_and_come_back') }} " />
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->

    </div>
    <div class="col-md-6">
        <div class="panel panel-flat">

            <div class="panel-heading">
                <h5 class="panel-title"> {{ "المضاف حديثا" }} </h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

                <table class="table table-bordered table-hover">
                    <tr class="text-center">
                        <th> {{ trans('تفاصيل بالعربيه') }} </th>
                        <th> {{ trans('تفاصيل بالانجليزيه') }} </th>
                        <th> {{ trans('dash.category') }} </th>
                    </tr>
                    @foreach($latest_offers as $offer)
                        <tr>
                            <td> {{ $offer->description_ar }} </td>
                            <td> {{ $offer->description_en }} </td>
                            <td> {{ $offer->category->name_ar }} </td>
                        </tr>
                    @endforeach
                </table>
            </div>



        </div>
    </div>
    </div>





@endsection
