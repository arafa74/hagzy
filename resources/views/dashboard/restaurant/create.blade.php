@extends('dashboard.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_layouts.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_select2.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
    
    <!-- /theme JS files -->
    
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <form action="{{ route('restaurant_store') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ Form::token() }}
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('dash.add_new_users') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.full_name') }}</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="full_name" value="{{ old('full_name') }}" placeholder="{{ trans('dash.full_name') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.email') }}</label>
                        <div class="col-lg-9">
                            <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="{{ trans('dash.email') }}" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.mobile') }}</label>
                        <div class="col-lg-9">
                            <input type="text" name="mobile" value="{{ old('mobile') }}" class="form-control" placeholder="{{ trans('dash.mobile') }}" >
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-3 control-label"> {{ trans('dash.image') }}</label>
                        <div class="col-lg-9">
                            <input type="file" class="file-styled" name="image">
                            <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> اختر الباقه </label>
                        <div class="col-lg-9">
                            <select name="bunch_id" class="select-border-color border-warning" >

                                @foreach ($bunches as $bunch)
                                    <option value="{{ $bunch->id }}"> {{ $bunch->name_ar }} </option>
                                @endforeach
                        </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> اختر المدينه </label>
                        <div class="col-lg-9">
                            <select name="city_id" class="select-border-color border-warning" >

                                @foreach ($cities as $city)
                                    <option value="{{ $city->id }}"> {{ $city->name_ar }} </option>
                                @endforeach
                        </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"> {{ trans('dash.password') }} </label>
                        <div class="col-lg-9">
                            <input type="password" class="form-control" name="password" placeholder=" {{ trans('dash.password') }} " required />
                        </div>
                    </div>

                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title"> {{ trans('dash.options') }} </h5>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                </ul>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="row">
                                @foreach($options as $option)
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3"> {{ $option->name_ar }} </label>
                                            <div class="col-lg-9">
                                                <div class="checkbox checkbox-switchery switchery-xs">
                                                    <label>
                                                        <input type="checkbox" name="options[]" class="switchery" value="{{ $option->id }}" >
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title"> {{ trans('dash.categories') }} </h5>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                </ul>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="row">
                                @foreach($categories as $category)
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3"> {{ $category->name_ar }} </label>
                                            <div class="col-lg-9">
                                                <div class="checkbox checkbox-switchery switchery-xs">
                                                    <label>
                                                        <input type="checkbox" name="categories[]" class="switchery" value="{{ $category->id }}" >
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>


                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.added_and_forward_to_list') }} " />
                        <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.add_and_come_back') }} " />
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->

    </div>



</div>



@endsection
