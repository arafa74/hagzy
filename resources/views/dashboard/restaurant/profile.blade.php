@extends('dashboard.layout')

@section('script')
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/user_pages_team.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"> {{ trans('بيانات المستخدم') }} </h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <ul class="nav nav-tabs nav-tabs-highlight">
                    <li class="active"><a href="#details-tab" data-toggle="tab"><i class="icon-menu7 position-left"></i> {{ trans('البيانات') }} </a></li>
                    <li><a href="#finished_orders" data-toggle="tab"><i class="icon-mention position-left"></i> {{ trans('حجوزات المستخدم') }} </a></li>
                    <li><a href="#prices" data-toggle="tab"><i class="icon-mention position-left"></i> {{ trans(' التقييمات') }} </a></li>
                </ul>
                <div class="tabbable nav-tabs-vertical nav-tabs-left">

                    <div class="tab-content">
                        
                        <div class="tab-pane active has-padding" id="details-tab">
                            <div class="row">


                                <div class="col-lg-6">
                                    <div class="content-group">
                                        <div class="panel-body bg-blue border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
                                            <div class="content-group-sm">
                                                <h5 class="text-semibold no-margin-bottom">
                                                    {{ $user->full_name }}
                                                </h5>
                                                <span class="display-block"> {{ trans('dash.delivery') }} </span>
                                            </div>
                                            <a href="{{ $user->imageurlorg }}" data-popup="lightbox">
                                                <img src=" {{ $user->imageurl }} " class="img-circle" alt="">
                                            </a>

                                            <ul class="list-inline no-margin-bottom">
                                                <li><a href="tel:{{$user->mobile}}" class="btn bg-blue-700 btn-rounded btn-icon"><i class="icon-mobile3"></i></a></li>
                                                <li><a href="mailto:{{ $user->email }}" class="btn bg-blue-700 btn-rounded btn-icon"><i class="icon-envelop4"></i></a></li>
                                            </ul>
                                        </div>

                                        <div class="panel panel-body no-border-top no-border-radius-top">
                                            <div class="form-group mt-5">
                                                <label class="text-semibold"> {{ trans('dash.full_name') }} </label>
                                                <span class="pull-right-sm"> {{ $user->full_name }} </span>
                                            </div>

                                            <div class="form-group">
                                                <label class="text-semibold"> {{ trans('dash.email') }} </label>
                                                <span class="pull-right-sm"><a href="mailto:{{$user->email}}"> {{$user->email }} </a></span>
                                            </div>

                                            <div class="form-group">
                                                <label class="text-semibold"> {{ trans('dash.mobile') }} </label>
                                                <span class="pull-right-sm"> <a href="tel:{{$user->mobile}}"> {{$user->mobile}} </a> </span>
                                            </div>

                                            <div class="form-group">
                                                <label class="text-semibold"> {{ trans('dash.city') }} </label>
                                                <span class="pull-right-sm"> @if($user->city_id) {{ $user->city->name_ar }} @else {{ trans('dash.no_selected') }} @endif </span>
                                            </div>

                                            <div class="form-group">
                                                <label class="text-semibold"> عدد الحجوزات </label>
                                                <span class="pull-right-sm"> {{ $ads_count }} </span>
                                            </div>

                                            <div class="form-group">
                                                <label class="text-semibold"> عدد الاعجابات </label>
                                                <span class="pull-right-sm"> {{ $user_favs_count }} </span>
                                            </div>

                                            <div class="form-group">
                                                <label class="text-semibold"> عدد التقيمات </label>
                                                <span class="pull-right-sm"> {{ $user_rates_count }} </span>
                                            </div>



                                            
                                            <div class="form-group">
                                                <label class="text-semibold"> {{ trans('dash.created_at') }} </label>
                                                <span class="pull-right-sm"> {{ $user->created_at->format('Y-m-d') }} </span>
                                            </div>

                                            <div class="form-group no-margin-bottom">
                                                <label class="text-semibold"> {{ trans('وقت التسجيل') }} </label>
                                                <span class="pull-right-sm"> {{ $user->created_at->format('H:i') }} </span>
                                            </div>

                                        </div>


                                        <br> <Br>
                                        <a href="{{ route('user_edit',['id' => $user->id ]) }}" target="_blank" class="btn btn-primary">{{ trans('dash.edit') }} <i class="icon-pencil3"></i></a>
                                        <a onclick="sweet_delete( ' {{ route('user_delete').'/'.$user->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $user->id }} )" class="btn btn-danger" > {{ trans('dash.delete') }} <i class="icon-database-remove"></i> </a>

                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    {{--<table class="table table-hover">--}}
                                        {{--<tr>--}}
                                            {{--<td> {{ trans('dash.wallet') }} </td>--}}
                                            {{--<td> {{ $user->wallet }} {{ trans('dash.sr') }} </td>--}}
                                        {{--</tr>--}}
                                    {{--</table>--}}
                                    {{--<br> <br>--}}
                                    {{--<form method="POST" action="{{ route('user_wallet') }}" style="border:1px solid black;padding:20px 30px">--}}
                                        {{--{{ Form::token() }}--}}
                                        {{--<input type="hidden" name="user_id" value="{{ $user->id }}" />--}}
                                        {{--<label> {{ trans('dash.add_balance_2_wallet') }} </label>--}}
                                        {{--<input type="number" class="form-control" name="balance" required /> --}}
                                        {{--<br>--}}
                                        {{--<center>--}}
                                            {{--<input type="submit" value="{{ trans('dash.add_now') }}" class="btn btn-primary" />--}}
                                        {{--</center>--}}
                                    {{--</form>--}}

                                    <br><br>
                                    {{--<form method="post" action="{{ route('send_notification_single') }}" style="border:1px solid black;padding:20px 30px">--}}
                                        {{--{{ Form::token() }}--}}
                                        {{--<input type="hidden" name="user_id" value="{{ $user->id }}" />--}}
                                        {{--<label> {{ trans('dash.title') }} </label>--}}
                                        {{--<input type="text" required class="form-control" name="title" />--}}
                                        {{--<br>--}}
                                        {{--<label> {{ trans('dash.notify') }} </label>--}}
                                        {{--<textarea class="form-control"  name="notification" required></textarea>--}}
                                        {{--<br>--}}
                                        {{--<center>--}}
                                            {{--<button type="submit" class="btn btn-primary"> {{ trans('dash.do_send_notification_now') }} <i class="icon-arrow-left13 position-right"></i></button>--}}
                                        {{--</center>--}}
                                    {{--</form>--}}


                                </div>
                                
                            </div>
                        </div>

                        <div class="tab-pane has-padding" id="finished_orders">
                              <table class="table table-condensed table-hover datatable-highlight">
                                <thead>
                                    <tr>
                                        <th class="text-center"> # </th>
                                        <th class="text-center"> {{ trans('اسم المطعم') }} </th>
                                        <th class="text-center"> {{ trans('اسم العميل') }} </th>
                                        <th class="text-center"> {{ trans('تاريخ الحجز') }} </th>
                                        <th class="text-center"> {{ trans('عدد الافراد') }} </th>
                                        <th class="text-center"> {{ trans('تاريخ الحجز') }} </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($user_reservations as $reservation)
                                    <tr id="row_{{ $reservation->id }}">
                                        <td> {{ $reservation->id }} </td>
                                        <td> {{ str_limit($reservation->restaurant->full_name,30,'...') }} </td>
                                        <td> {{ $reservation->user->full_name }} </td>
                                        <td> {{ $reservation->reservation_day }} </td>
                                        <td> {{ $reservation->clientsNum }} </td>
                                        <td class="text-center">
                                            <a href="{{ route('reservation_view',['id' => $reservation->id ]) }}" class="btn btn-primary"> <i class="icon-touch"></i> </a>
                                            <a onclick="sweet_delete( ' {{ route('reservation_delete').'/'.$reservation->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $reservation->id }} )" class="btn btn-danger" > <i class="icon-database-remove"></i> </a>
                                        </td>
                                    </tr>

                                    @empty
                                    @endforelse

                                </tbody>
                            </table>
                        </div>

                        <div class="tab-pane has-padding" id="prices">
                            <table class="table table-condensed table-hover datatable-highlight">
                                <thead>
                                <tr>
                                    <th class="text-center"> # </th>
                                    <th class="text-center"> {{ trans('اسم المطعم') }} </th>
                                    <th class="text-center"> {{ trans('العميل') }} </th>
                                    <th class="text-center"> {{ trans('التعليق') }} </th>
                                    <th class="text-center"> {{ trans('التقييم') }} </th>
                                    <th class="text-center"> {{ trans('منذ ') }} </th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($user_rates as $rate)
                                    <tr id="row_{{ $rate->id }}">
                                        <td> {{ $rate->id }} </td>
                                        <td> {{ str_limit($rate->restaurant->full_name,30,'...') }} </td>
                                        <td> {{ str_limit($rate->user->full_name,30,'...') }} </td>
                                        <td> {{ $rate->comment }} </td>
                                        <td> {{ $rate->rate }} </td>
                                        <td> {{ $rate->created_at->format('Y-m-d H:i') }} </td>

                                    </tr>
                                @empty
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
