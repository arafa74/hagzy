@extends('dashboard.layout')

@section('script')
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/datatables_advanced.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')


<div class="panel panel-flat tb_padd">
    <div class="panel-heading">
        <h5 class="panel-title"> {{ trans('الشكاوى والمقترحات') }} </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>
<!-- 
    <div class="form-group row">
										<label class="col-lg-3 col-form-label">Your state:</label>
										<div class="col-lg-9">
											<select class="form-control form-control-select2" data-fouc>
												<optgroup label="Alaskan/Hawaiian Time Zone">
													<option value="AK">Alaska</option>
													<option value="HI">Hawaii</option>
												</optgroup>
												<optgroup label="Pacific Time Zone">
													<option value="CA">California</option>
													<option value="NV">Nevada</option>
													<option value="WA">Washington</option>
												</optgroup>
												<optgroup label="Mountain Time Zone">
													<option value="AZ">Arizona</option>
													<option value="CO">Colorado</option>
													<option value="WY">Wyoming</option>
												</optgroup>
												<optgroup label="Central Time Zone">
													<option value="AL">Alabama</option>
													<option value="AR">Arkansas</option>
													<option value="KY">Kentucky</option>
												</optgroup>
												<optgroup label="Eastern Time Zone">
													<option value="CT">Connecticut</option>
													<option value="DE">Delaware</option>
													<option value="FL">Florida</option>
												</optgroup>
											</select>
										</div>
									</div> -->



    <table class="table table-condensed table-hover datatable-highlight">
        <thead>
            <tr>
                <th class="text-center"> # </th>
                <th class="text-center"> {{ trans('الرساله') }} </th>
                <th class="text-center"> {{ trans('نوع الشكوى') }} </th>
                <th class="text-center"> {{ trans('dash.created_at') }} </th>
                <th class="text-center"> {{ trans('dash.actions') }} </th>
            </tr>
        </thead>
        <tbody>
            @forelse($contacts as $contact)
                <tr id="row_{{ $contact->id }}">
                    <td> {{ $loop->iteration }} </td>
                    <td> {{ $contact->details }} </td>
                    <td> {{ App\Type::where('id' , $contact->type_id)->first()->name_ar }} </td>
                    <td> {{ $contact->created_at->format('Y-m-d H:i') }} </td>
                    <td class="text-center">
                        <a onclick="sweet_delete( ' {{ route('delete_contact').'/'.$contact->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $contact->id }} )" class="btn btn-danger" > <i class="icon-database-remove"></i> </a>
                    </td>
                </tr>
            @empty
            @endforelse

        </tbody>
    </table>
</div>

<script>
    function sweet_delete($url,$message,$user_id)
    {
        $( "#row_"+$user_id ).css('background-color','#000000').css('color','white');
        swal({
        title: $message,
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url:$url
            });
            swal({
                title: "{{ trans('alert') }}",
                text: "{{ trans('dash.deleted_successfully') }}",
                icon: "success",
                timer:1000
            });
            $( "#row_"+$user_id ).hide(1000);
        }else{
            $( "#row_"+$user_id ).removeAttr('style');
        }
        });
    }
</script>

@endsection
