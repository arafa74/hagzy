@extends('dashboard.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/form_layouts.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/form_select2.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/ui/ripple.min.js"></script>

    <!-- /theme JS files -->

@endsection

@section('content')

<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <form action="{{ route('slide_store') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ Form::token() }}
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"> اضافه جديد </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">


                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block">  مقدم الخدمه - المطعم </label>
                        <div class="col-lg-9">
                            <select name="restaurant_id" class="select-border-color border-warning" >
                                <optgroup label="ااختر مقدم الخدمه">
                                    @foreach ($restaurants as $user)
                                        <option value="{{ $user->id }}"> {{ $user->full_name }} </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block">  العرض  </label>
                        <div class="col-lg-9">
                            <select name="offer_id" class="select-border-color border-warning" >
                                <optgroup label="اختر العرض">
                                    @foreach ($offers as $offer)
                                        <option value="{{ $offer->id }}"> {{ $offer->description }} </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>

                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block">  الظهور فى الشاشه الرئيسيه  </label>
                        <div class="col-lg-9">
                            <select name="is_home" class="select-border-color border-warning" >
                                <optgroup label="اختر الحاله">
                                        <option value="1"> السماح بالظهور فى الشاشه الرئيسيه </option>
                                        <option value="0"> عدم السماح بالظخهور فى الشاشاه الرئيسيه </option>
                                    
                                </optgroup>
                            </select>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"> {{ trans('dash.image') }}</label>
                        <div class="col-lg-9">
                            <input type="file" class="file-styled" name="image" required>
                            <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                        </div>
                    </div>



                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.added_and_forward_to_list') }} " />
                        <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.added_and_come_back') }} " />
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->

    </div>

</div>

</div>

@endsection
