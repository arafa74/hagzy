@extends('dashboard.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_select2.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>

    <!-- /theme JS files -->

@endsection

@section('content')

<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <form action="{{ route('slide_update') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ Form::token() }}
            <input type="hidden" name="slide_id" value="{{ $slide->id }}" />
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"> تعديل بيانات المعرض </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">


                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> المطعم - مقدمى الخدمات </label>
                        <div class="col-lg-9">
                            <select name="restaurant_id" class="select-border-color border-warning" >
                                <optgroup label="{{ trans('اختر المطعم') }}">
                                    @foreach ($restaurants as $restaurant)
                                        <option value="{{ $restaurant->id }}" @if($slide->restaurant_id == $restaurant->id) selected @endif> {{ $restaurant->full_name }} </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                        
                    <div class="form-group">
                            <label class="col-lg-3 control-label display-block">  العرض </label>
                            <div class="col-lg-9">
                                <select name="offer_id" class="select-border-color border-warning" >
                                    <optgroup label="{{ trans('اختر العرض') }}">
                                        @foreach ($offers as $offer)
                                            <option value="{{ $offer->id }}" @if($slide->offer_id == $offer->id) selected @endif> {{ $offer->description }} </option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-3 control-label">الظهور فى الشاشاه الرئيسيه</label>
                            <div class="col-lg-9">
                                <select name="is_home" class="select-border-color border-warning">
                                    <optgroup>
                                        <option value="1" @if($slide->is_home==1) selected @endif> السماح بالظهور فى الشاشه الرئيسيه </option>
                                        <option value="0" @if($slide->is_home==0) selected @endif> عدم السماح بالظهور فى الشاشه الرئيسيه </option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    

                    <div class="form-group">
                        <label class="col-lg-3 control-label"> {{ trans('dash.image') }}</label>
                        <div class="col-lg-6">
                            <input type="file" class="file-styled" name="image" >
                        </div>
                        <div class="col-lg-3">
                            <a href="{{ $slide->imageurlorg }}" data-popup="lightbox">
                                <img src="{{ $slide->imageurl }}" height="80" width="90" />
                            </a>
                        </div>
                    </div>



                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.update_and_forword_2_list') }} " />
                        <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.update_and_come_back') }} " />
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->

    </div>

</div>

</div>

@endsection