@extends('dashboard.layout')

@section('script')
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/user_pages_team.js"></script>

    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/datatables_advanced.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')


<div class="panel panel-flat tb_padd">
    <div class="panel-heading">
        <h5 class="panel-title"> {{ trans('الاطباق والوجبات') }} </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>
    
    <table class="table table-condensed table-hover datatable-highlight">
        <thead>
            <tr>
                <th class="text-center"> # </th>
                <th class="text-center"> اسم المطعم </th>
                <th class="text-center">الظهور فى الشاشه الرئيسيه </th>
                <th class="text-center"> العرض </th>
                <th class="text-center"> {{ trans('dash.image') }} </th>
                <th class="text-center"> {{ trans('dash.actions') }} </th>
            </tr>
        </thead>
        <tbody>
            @forelse($slides as $slide)
            <tr id="row_{{ $slide->id }}">
                <td> {{ $slide->id }} </td>
                <td> {{ @App\User::where('id' , $slide->restaurant_id)->first()->full_name }} </td>
                <td> @if($slide->is_home == 1)
                نعم
                    @else
                    لا
                    @endif
                </td>
                <td> {{ @App\Offer::where('id' , $slide->offer_id)->first()->description  }} </td>
                <td>
                    <a href="{{ $slide->imageurlorg }}" data-popup="lightbox">
                        <img src="{{ $slide->imageurl }}" height="100" width="100" />
                    </a>
                </td>
                <td class="text-center">
                    <a href="{{ route('slide_edit',['id' => $slide->id ]) }}" class="btn btn-primary"> <i class="icon-pencil"></i> </a>
                    <a onclick="sweet_delete( ' {{ route('slide_delete').'/'.$slide->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $slide->id }} )" class="btn btn-danger" > <i class="icon-database-remove"></i> </a>
                </td>
            </tr>

            @empty
            @endforelse

        </tbody>
    </table>
</div>

@endsection
