@extends('dashboard.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/form_layouts.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/form_select2.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/plugins/ui/ripple.min.js"></script>

    <!-- /theme JS files -->

@endsection

@section('content')

<div class="row">
    <div class="col-md-6">

        <!-- Basic layout-->
        <form action="{{ route('car_store') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ Form::token() }}
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('اضافه جديد') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.name_ar') }}</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="name_ar" placeholder="{{ trans('dash.name_ar') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.name_en') }}</label>
                        <div class="col-lg-9">
                            <input type="text" name="name_en" class="form-control" placeholder="{{ trans('dash.name_en') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3"> نبذه عن السياره بالعربيه <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="5" cols="5" name="description_ar" class="form-control" required="required" placeholder="{{ trans('dash.app_desc_ar') }}">  </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">  نبذه عن السياره بالانجليزيه <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="5" cols="5" name="description_en" class="form-control" required="required" placeholder="{{ trans('dash.app_desc_ar') }}">  </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> {{ trans('حاله السياره') }} </label>
                        <div class="col-lg-9">
                            <select name="status" class="select-border-color border-warning" >
                                <optgroup label="{{ trans('اختر ') }}">
                                    <option value="new"> جديد </option>
                                    <option value="used"> مستعمل </option>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> {{ trans('القسم') }} </label>
                        <div class="col-lg-9">
                            <select name="category_id" class="select-border-color border-warning" >
                                <optgroup label="{{ trans('اختر القسم') }}">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}"> {{ $category->name_ar.' - '.$category->name_en }} </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> {{ trans('النوع-الفئه') }} </label>
                        <div class="col-lg-9">
                            <select name="type_id" class="select-border-color border-warning" >
                                <optgroup label="{{ trans('اختر الفئه') }}">
                                    @foreach ($types as $type)
                                        <option value="{{ $type->id }}"> {{ $type->name_ar.' - '.$type->name_en }} </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> {{ trans('النوع-الفئه') }} </label>
                        <div class="col-lg-9">
                            <select name="year_model">
                                <option value=''>Select Year</option>
                                <?php
                                for ($year = 1980; $year <= 2020; $year++) {
                                    $selected = (isset($getYear) && $getYear == $year) ? 'selected' : '';
                                    echo "<option value=$year $selected>$year</option>";
                                }
                                ?>
                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"> اضافه صور للسياره</label>
                        <div class="col-lg-9">
                            <input type="file" class="file-styled" name="images[]" multiple>
                            <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                        </div>
                    </div>

                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title"> {{ trans('dash.options') }} </h5>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                </ul>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="row">
                                @foreach($options as $option)
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label class="control-label col-lg-3"> {{ $option->name_ar }} </label>
                                        <div class="col-lg-9">
                                            <div class="checkbox checkbox-switchery switchery-xs">
                                                <label>
                                                    <input type="checkbox" name="options[]" class="switchery" value="{{ $option->id }}" >
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>




                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.add_forword_2_places') }} " />
                        <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.add_and_come_back') }} " />
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->

    </div>


    <div class="col-md-6">
        <div class="panel panel-flat">

                <div class="panel-heading">
                    <h5 class="panel-title"> {{ "المضاف حديثا" }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">

                    <table class="table table-bordered table-hover">
                        <tr class="text-center">
                            <th> {{ trans('dash.name_ar') }} </th>
                            <th> {{ trans('dash.name_en') }} </th>
                            <th> {{ trans('dash.category') }} </th>
                        </tr>
                        @foreach($latest_cars as $car)
                        <tr>
                            <td> {{ $car->name_ar }} </td>
                            <td> {{ $car->name_en }} </td>
                            <td> {{ $car->category->name_ar }} </td>
                        </tr>
                        @endforeach
                    </table>
                </div>



                </div>
        </div>
    </div>


@endsection
