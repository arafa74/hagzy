@extends('dashboard.layout')

@section('script')

    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/user_pages_team.js"></script>

    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/datatables_advanced.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')


    <div class="panel panel-flat tb_padd">
        <div class="panel-heading">
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <center>
            {{ $bunches->links() }}
        </center>
        <table class="table table-condensed table-hover datatable-highlight">
            <thead>
            <tr>
                <th class="text-center"> # </th>
                <th class="text-center"> {{ trans('تفاصيل الباقه') }} </th>
                <th class="text-center"> {{ trans('تفاصيل الباقه باللغه الانجليزيه') }} </th>
                <th class="text-center"> {{ trans('dash.created_at') }} </th>
                <th class="text-center"> تعديل </th>
                <th class="text-center"> حذف </th>
            </tr>
            </tr>
            </thead>
            <tbody>
            @forelse($bunches as $bunch)
                <tr id="row_{{ $bunch->id }}">
                    <td> {{ $bunch->id }} </td>
                    <td> {{ $bunch->name_ar }} </td>
                    <td>  {{ $bunch->name_en }}  </td>
                    <td> {{ $bunch->created_at->format('Y-m-d H:i') }} </td>
                    <td class="text-center">
                        <a title="{{ trans('dash.open') }}" href="{{ route('bunches_edit',['id' => $bunch->id ]) }}" class="btn btn-primary"> <i class="icon-touch"></i> </a>
                    </td>
                    <td class="text-center">    
                        <a onclick="sweet_delete( ' {{ route('bunches_delete').'/'.$bunch->id }} ' , '{{ trans('dash.deleted_msg_confirm') }}' ,{{ $bunch->id }} )" class="btn btn-danger" > <i class="icon-database-remove"></i> </a>
                    </td>
                </tr>
            @empty
            @endforelse

            </tbody>
        </table>
        <center>
            {{ $bunches->links() }}
        </center>
        <br>
    </div>


@endsection
