@extends('dashboard.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_select2.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>
    
    <!-- /theme JS files -->
    
@endsection

@section('content')

<div class="row">
    <div class="col-md-6">

        <!-- Basic layout-->
        <form action="{{ route('user_update') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ Form::token() }}
            <input name="_method" type="hidden" value="PUT">
            <input type="hidden" name="user_id" value="{{ $user->id }}" />
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('dash.add_new_users') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.full_name') }}</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="full_name" value="{{ $user->full_name }}" placeholder="{{ trans('dash.full_name') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.email') }}</label>
                        <div class="col-lg-9">
                            <input type="email" name="email" class="form-control" value="{{ $user->email }}" placeholder="{{ trans('dash.email') }}" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.mobile') }}</label>
                        <div class="col-lg-9">
                            <input type="text" name="mobile" value="{{ $user->mobile }}" class="form-control" placeholder="{{ trans('dash.mobile') }}" >
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-3 control-label"> {{ trans('dash.image') }}</label>
                        <div class="col-lg-9">
                            <input type="file" class="file-styled" name="image">
                            <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> {{ trans('dash.city') }} </label>
                        <div class="col-lg-9">
                            <select name="city_id" class="select-border-color border-warning" >
                                @foreach ($cities as $city)
                                    <option value="{{ $city->id }}" @if($city->id == $user->city_id) selected @endif > {{ $city->name_ar }} </option>
                                @endforeach
                        </select>
                        </div>
                    </div>
                    @if($user->type == "restaurant")
                    <div class="form-group">
                        <label class="col-lg-3 control-label display-block"> تفعيل حساب المطعم </label>
                        <div class="col-lg-9">
                            <select name="active" class="select-border-color border-warning" >
                                
                                    <option value="active" > تفعيل الحساب </option>
                                
                        </select>
                        </div>
                    </div>
                    @endif
                    
                    <div class="form-group">
                        <label class="col-lg-3 control-label"> {{ trans('dash.password') }} </label>
                        <div class="col-lg-9">
                            <input type="password" class="form-control" name="password" placeholder=" {{ trans('dash.password') }} "  />
                        </div>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label class="col-lg-3 control-label"> {{ trans('dash.confirm_password') }} </label>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<input type="password" class="form-control" name="confirm_password" placeholder=" {{ trans('dash.confirm_password') }} "  />--}}
                        {{--</div>--}}
                    {{--</div>--}}



                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.update_and_forword_2_list') }} " />
                        <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.update_and_come_back') }} " />
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->

    </div>


    <div class="col-md-6">



        <div class="panel panel-flat">

                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('dash.image') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body" >
                    <center>
                        <img src="{{ $user->imageurl }}" />
                    </center>
                </div>
        </div>

        <div class="panel panel-flat">

                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('dash.map') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body" >
                    <center>
                        <div class="map-container map-marker-simple"></div>
                    </center>
                </div>
        </div>

        
    </div>

</div>

<script>
$(function() {

// Setup map
function initialize() {
    var place_lat = {{ $user->latitude}};
    var place_lng = {{ $user->longitude}};
    // Set coordinates
    var myLatlng = new google.maps.LatLng(place_lat,place_lng);

    // Options
    var mapOptions = {
        zoom: 16,
        center: myLatlng
    };

    // Apply options
    var map = new google.maps.Map($('.map-marker-simple')[0], mapOptions);
    var contentString = '@if($user->city_id) {{ $user->city->name_ar }} @endif';

    // Add info window
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    // Add marker
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: '@if($user->city_id) {{ $user->city->name_ar }} @endif'
    });

    // Attach click event
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });

};

// Initialize map on window load
google.maps.event.addDomListener(window, 'load', initialize);

});

</script>


@endsection
