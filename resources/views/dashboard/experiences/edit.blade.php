@extends('dashboard.layout')

@section('script')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/core/app.js"></script>
	<script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="{{ url('public/dashboard_assets/material') }}/assets/js/plugins/ui/ripple.min.js"></script>

    <!-- /theme JS files -->

@endsection

@section('content')

<div class="row">
    <div class="col-md-6">

        <!-- Basic layout-->
        <form action="{{ route('experience_update') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ Form::token() }}
            <input type="hidden" name="experience_id" value="{{ $experience->id }}" />
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"> تعديل بيانات المستوى </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.name_ar') }}</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="name_ar" placeholder="{{ trans('dash.name_ar') }}" value="{{ $experience->name_ar }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{ trans('dash.name_en') }}</label>
                        <div class="col-lg-9">
                            <input type="text" name="name_en" class="form-control" placeholder="{{ trans('dash.name_en') }}" value="{{ $experience->name_en }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">عدد النقاط</label>
                        <div class="col-lg-9">
                            <input type="number" class="form-control" name="points" placeholder="عدد النقاط" value="{{ $experience->points }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">عدد تسجيلات الاعجاب</label>
                        <div class="col-lg-9">
                            <input type="number" class="form-control" name="likes" placeholder="عدد تسجيلات الاعجاب" value="{{ $experience->likes }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">عدد الحجوزات</label>
                        <div class="col-lg-9">
                            <input type="number" class="form-control" name="reservations" placeholder="عدد الحجوزات" value="{{ $experience->reservations }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">عدد التقييمات</label>
                        <div class="col-lg-9">
                            <input type="number" class="form-control" name="rates" placeholder="عدد التقييمات" value="{{ $experience->rates }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">لون المستوى المميز</label>
                        <div class="col-lg-9">
                            <input type="color" name="color" class="form-control" placeholder="اللون المميز" value="{{ $experience->color }}" required>
                        </div>
                    </div>



                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="forward" value=" {{ trans('dash.update_and_forword_2_list') }} " />
                        <input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.update_and_come_back') }} " />
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->

    </div>


    <div class="col-md-6">
        <div class="panel panel-flat">




                </div>
        </div>
    </div>

</div>



<script type="text/javascript" src="{{ url('public/dashboard_assets') }}/js/pages/form_select2.js"></script>
@endsection
