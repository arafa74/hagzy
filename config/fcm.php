<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAhjzQrO8:APA91bGFlmenRw2S_zqbytaapgFFjnkKtuaYHMDXBUVqmeJ9iRMb-YNLCTUOn4gL-js42AIL01m3ZQzC8yXTh90guiq4pbobOcjhAfDlCqVMaeXrv97NI8UUFN21XkZwicGWjaiPrpYE'),
        'sender_id' => env('FCM_SENDER_ID', '576545926383'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
