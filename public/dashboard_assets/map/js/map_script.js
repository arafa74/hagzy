jQuery(window).load(function () {

    // PAGE LOADER

    $('.pre-load').stop().animate({ opacity: 0 }, 500, function(){
        $('.pre-load').css({'display':'none'});
        $('body').css({'overflow-y':'auto'});
    });

    // ANIMATION

    Animate_box();
    $(document).scroll(function (){
        Animate_box();
    });

    function Animate_box() {
        var scroll_var = $(this).scrollTop();

        $('.animate-box').each(function (){
            var val_one = $(this).offset().top - $(window).height() + 80;

            if (scroll_var > val_one){
                if($(this).hasClass('left-in')) {
                    $(this).addClass('animated fadeInLeft');
                }else if($(this).hasClass('right-in')) {
                    $(this).addClass('animated fadeInRight');
                }else {
                    $(this).addClass('animated fadeInUp');
                }
            }
        });
    }

});

$(document).ready(function() {

    // WINDOW HEIGHT

    windowHeight();
    $(window).resize(function (){
        windowHeight();
    });

    function windowHeight() {
        if($('footer').length) {
            $('.win-height').css({
                'min-height': $(window).height() - ($('.navbar').height() + $('footer').height() + 10)
            });
            $('.max-height').css({
                'height': $(window).height() - ($('.navbar').height() + $('footer').height() + 10)
            });
        }else {
            $('.win-height').css({
                'min-height': $(window).height()
            });
        }
        $('.mega-sing-up-box').css({
            'min-height': $('.sign-more-box').height() + 100
        });
        $('.dropbox-body').css({
            'max-height': $(window).height() / 2
        });
    }


    // MENU COLLAPSE

    $('.mirror').click(function () {
        $(this).parent().find('.navbar-collapse').removeClass('in');
        $(this).parent().find('.navbar-toggle').addClass('collapsed');
    });


    // SMOOTH SCROLL

    $('.smooth-a, .nav a[href="#apps"]').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 1000);
        return false;
    });


    // FLOATING LABELS

    floatLabel();
    $('.float-label .float-input').blur(function (){
        floatLabel();
    });

    function floatLabel() {
        $('.float-label .float-input').each(function (){
            if($(this).val()){
                $(this).parent().addClass('stay-top');
            }else {
                $(this).parent().removeClass('stay-top');
            }
        });
    }


    // LOGIN + FORGET PASSWORD

    $('.forget-pass').slideUp(0);
    $('a[href="#forgetPass"]').click(function() {
        $('.login-box').slideUp();
        $('.forget-pass').slideDown();
        return false;
    });
    $('.back-to-login').click(function() {
        $('.forget-pass').slideUp();
        $('.login-box').slideDown();
        return false;
    });


    // SIGN UP MORE DETAILS

    $('.add-more-details').click(function() {
        $('.mega-sing-up-box').addClass('view-more-details');
        return false;
    });
    $('.back-to-sign-up').click(function() {
        $('.mega-sing-up-box').removeClass('view-more-details');
        return false;
    });


    // INPUT WITH MAX COUNT

    $('.input-max-width').keyup(function() {
        var currentCount = $(this).val().length;
        var maxCount = parseInt($('.max-label-count .maximum').text());
        $(this).attr('maxlength', maxCount);

        if(currentCount <= maxCount) {
            $('.max-label-count .initial').html(currentCount);
        }
    });


    // UPLOAD FILE

    $('.image-uploader').change(function (event){
        $(this).parents('.images-upload-block').append('<div class="uploaded-block"><img src="'+ URL.createObjectURL(event.target.files[0]) +'" alt="..."><button class="close">&times;</button></div>');
        removeUplodedImage();
    });

    removeUplodedImage();
    function removeUplodedImage() {
        $('.uploaded-block .close').click(function (){
            $(this).parents('.uploaded-block').remove();
        });
    }


    // DRIVERS DROP LIST

    $('.dropbox-body').slideUp(0);
    $('.dropbox-header').click(function() {
        $(this).parent().toggleClass('open-box');
        $('.dropbox-body').slideToggle();
    });


    // PARALLAX

    if($('.parallax-window').length) {
        $('.parallax-window').parallax({
            speed: 0.4
        });
    }


    // TOOLTIP

    $('[data-toggle="tooltip"]').tooltip();

});