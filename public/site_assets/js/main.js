// 
$(window).on("load", function () {
  $("#preloader-first").delay(600).fadeOut()
});

$(document).ready(function () {

  'use strict';

  new WOW().init();

  $('.menu-wrapper').on('click', function () {
    $('.hamburger-menu').toggleClass('animate');
  });

  $(window).scroll(function () {
    if ($(this).scrollTop() > 80) {
      $('.navbar').addClass('fixed-top');
    } else {
      $('.navbar').removeClass('fixed-top');
    }
  });


  // select
  $('.js-example-basic-single').select2({
    minimumResultsForSearch: Infinity
  });

  // wizard
  $(".btn.next").click(function () {
    $(this).parents('.tab-pane').removeClass('show active').next().addClass('show active');
  });

  $('.about-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    fade: true,
    rtl: true
  });


  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav',
    rtl: true
  });
  $('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    false: true,
    focusOnSelect: true,
    rtl: true,
    responsive: [
      {
        breakpoint: 540,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.product').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.product-list',
    rtl: true
  });
  $('.product-list').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.product',
    false: true,
    focusOnSelect: true,
    rtl: true
  });


  $('.recent-added').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    rtl: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true
        }
      }
    ]
  });

  // show password
  $('.img-eyes1').on('click', function () {
    var $inp1 = $(this).parent().find(".input-password");
    $inp1.attr('type') === 'password' ? $inp1.attr('type', 'text') : $inp1.attr('type', 'password');
  });

  $('.all-new').click(function () {
    $('.all-new').addClass("is-active");
    $('.all-new').removeClass("is-active");

  });

  // fancy
  $("a.group").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false
	});

});