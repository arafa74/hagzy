<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table='slides';

    public function getImageurlAttribute()
    {
        $image = Slide::where('id', $this->id)->first()->image;
        if (!$image) {
            return url('storage/app/users/default.png');
        }
        return url('storage/app/slides/org') . '/' . $this->attributes['image'];
    }
}
