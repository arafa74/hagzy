<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCategory extends Model
{
    protected $table = 'user_categories';
    protected $fillable = ['category_id' , 'user_id'];

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
