<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $table = 'suggestions';

    protected $fillable =['name' , 'lat' , 'lng' , 'address',  'mobile' ,'bref' , 'user_id' ];
}
