<?php

function SETTING_VALUE($key = false)
{
    return \App\Model\Setting::where('key', $key)->first()->value;
}

function SEND_SINGLE_SMS($phone,$msg){
    Nexmo::message()->send([
        'to'   => '201277758815',// $phone
        'from' => 'equipments',
        'text' =>  $msg
    ]);
}


function MK_REPORT($key, $text, $process, $user_id = 0)
{
    $report = new \App\Report();
    if ($user_id == 0) {
        $report->user_id = auth()->user()->id;
    }
    $report->key = $key;
    $report->text = $text;
    $report->process = $process;
    $report->save();
}

function faTOen($string)
{
    return strtr($string, array('۰' => '0', '۱' => '1', '۲' => '2', '۳' => '3', '۴' => '4', '۵' => '5', '۶' => '6', '۷' => '7', '۸' => '8', '۹' => '9', '٠' => '0', '١' => '1', '٢' => '2', '٣' => '3', '٤' => '4', '٥' => '5', '٦' => '6', '٧' => '7', '٨' => '8', '٩' => '9'));
}


