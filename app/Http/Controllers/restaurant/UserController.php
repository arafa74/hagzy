<?php

namespace App\Http\Controllers\dashboard;

use App\Ad;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Requests\dashboard\UserRequest;
use App\Like;
use App\Model\City;
use App\Reservation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\PARENT_DASHBOARD;
use Auth;
use Validator;
class UserController extends PARENT_DASHBOARD
{

    public function index()
    {
        $data['users'] = User::where('type' ,'user' )->paginate(10);
        return view('dashboard.user.index', $data);
    }

    public function profile($id)
    {
        $data['user']=  User::find($id);
        $user = $data['user'];
        $data['ads_count'] = Reservation::where('user_id' , $data['user']->id)->count();
        $data['user_reservations'] = Reservation::where('user_id' , $data['user']->id)->get();
        $data['user_favs'] = User::whereHas('like', function($query) use ($user) {
            return $query->where('user_id', $user->id);
        })
            ->get();

        $data['user_favs_count'] = User::whereHas('like', function($query) use ($user) {
            return $query->where('user_id', $user->id);
        })
            ->count();
        return view('dashboard.user.profile',$data);
    }

    public function create()
    {
        $this->data['cities'] = City::orderBy('created_at','DESC')->get();
        $this->data['last_users'] = User::orderBy('created_at','DESC')->where('id','!=',Auth::id())->take(10)->get();
        return view('dashboard.user.create',$this->data);
    }

    public function store(UserRequest $request)
    {
        $image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/users');
        $create = User::create([
            'full_name'=>$request->full_name,
            'email'=>$request->email,
            'mobile'=>$request->mobile,
            'type'=>'user',
            'image'=>$image,
            'city_id'=>$request->city_id,
            'password'=>bcrypt($request->password)
        ]);
        MK_REPORT('dashboard_create_user_account', 'Create New User Account  ' . $create->full_name, $create);
        if ($request->has('forward')) {
            $forward_url = url('dashboard/user');
        } else {
            $forward_url = url('dashboard/user/create');
        }
        return redirect($forward_url);
    }

    public function delete($id)
    {
            $user = User::find($id);
            $user->delete();
        return back();
    }

    public function edit($id)
    {
        $this->data['cities'] = City::all();
        $this->data['user'] = User::find($id);
        $this->data['last_users'] = User::orderBy('created_at','DESC')->where('id','!=',Auth::id())->take(10)->get();
        return view('dashboard.user.edit',$this->data);
    }

    public function update(UserRequest $request)
    {
        $user = User::find($request->user_id);

        $validator=Validator::make($request->all(),[
                'email' => 'unique:users,email,'.$user->id,
                'mobile' => 'unique:users,mobile,'.$user->id,
            ]
            ,
            [
                'mobile.unique' => 'رقم هاتف مستخدم من قبل',
                'email.unique' => 'البريد الالكترونى مستخدم من قبل',
            ]
        );

        if ($request->hasFile('image')) {
            $image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/users');
        }else{
            $image = $user->image;
        }

        $update = User::find($request->user_id)->update([
            'full_name'=>$request->full_name,
            'email'=>$request->email,
            'mobile'=>$request->mobile,
            'image'=>$image,
            'city_id'=>$request->city_id
        ]);

        if ($request->has('passowrd')) {
            if (Hash::check('passowrd',$user->password)) {
                User::find($request->user_id)->update([
                    'password'=>bcrypt($request->password)
                ]);
            }
        }

        MK_REPORT('dashboard_update_user_info', 'update New User info  ' . $user->id, $user);
        if ($request->has('forward')) {
            $forward_url = url('dashboard/user');
        } else {
            $forward_url = url('dashboard/user/edit/'.$request->user_id);
        }
        return redirect($forward_url);
    }

}
