<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\PARENT_DASHBOARD;
use Illuminate\Http\Request;
use App\Http\Controllers\DASHBOARDCONTROLLER;
use App\Http\Controllers\Controller;
use App\Model\AdministrationGroup;
use Validator;
use App\User;
use App\Http\Controllers\ImageController;

class AdministrationController extends PARENT_DASHBOARD
{
    public function __construct()
    {
        $this->data['squence_pages'][trans('dash.administration_groups')] = route('administration_groups');
    }

    public function group()
    {
        $this->data['squence_pages'][trans('dash.administration_groups')] = route('administration_groups');
        $this->data['administration_groups'] = AdministrationGroup::where('permissions', '!=', '*')->get();
        $this->data['table_name'] = trans('dash.administration_groups');
        return view('dashboard.administration.administration_groups', $this->data);
    }

    public function group_create()
    {
        return view('dashboard.administration.group_create');
    }

    public function group_store(Request $request)
    {
        $group_data = [
            'name_ar' => $request->name_ar,
            'perms' => $request->perms,
        ];
        $group_rules = [
            'name_ar' => 'required',
            'perms' => 'required',
        ];
        $validator = Validator::make($group_data, $group_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $administration_group = new AdministrationGroup();
        $administration_group->name_ar = $request->name_ar;
        if ($request->name_en) {
            $administration_group->name_en = $request->name_en;
        }
        if ($request->short_desc_ar) {
            $administration_group->description = $request->short_desc_ar;
        }
        $administration_group->permissions = json_encode($request->perms);
        $administration_group->created_by = auth()->user()->id;
        $administration_group->save();
        MK_REPORT('dashboard_create_group', 'Create Administration Group Name '. $administration_group->name_ar, $administration_group);
        if ($request->back) {
            $forward_url = url('dashboard/administration/group/create');
        } else {
            $forward_url = url('dashboard/administration/group');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }

    public function group_edit($id = 0)
    {
        if (!AdministrationGroup::find($id) && AdministrationGroup::find($id)->permissions == "*") {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $group = AdministrationGroup::find($id);
        $this->data['group'] = $group;
        $this->data['perms'] = json_decode($group->permissions);
        return view('dashboard.administration.group_edit', $this->data);
    }

    public function group_update(Request $request)
    {
        $group_data = [
            'administration_group_id' => $request->administration_group_id,
            'name_ar' => $request->name_ar,
            'perms' => $request->perms,
        ];
        $group_rules = [
            'administration_group_id' => 'required|exists:administration_group,id',
            'name_ar' => 'required',
            'perms' => 'required',
        ];
        $validator = Validator::make($group_data, $group_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $administration_group = AdministrationGroup::find($request->administration_group_id);
        MK_REPORT('dashboard_update_group', 'Update Administration Group Name ' . $administration_group->name_ar, $administration_group);
        $administration_group->name_ar = $request->name_ar;
        if ($request->name_en) {
            $administration_group->name_en = $request->name_en;
        }
        if ($request->short_desc_ar) {
            $administration_group->description = $request->short_desc_ar;
        }
        $administration_group->permissions = json_encode($request->perms);
        $administration_group->created_by = auth()->user()->id;
        $administration_group->save();
        if ($request->back) {
            $forward_url = url('dashboard/administration/group/edit') . '/' . $request->administration_group_id;
        } else {
            $forward_url = url('dashboard/administration/group');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

    public function group_delete($id = 0)
    {
        $administration_group = AdministrationGroup::where('id', $id)->where('permissions', '!=', "*")->first();
        if (!$administration_group) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        MK_REPORT('dashboard_delete_group', 'Delete Administration Group Name ' . $administration_group->name_ar, $administration_group);
        $administration_group->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }

    public function admins()
    {
        $this->data['admins'] = User::where('administration_group_id', '!=', null)->get();
        return view('dashboard.administration.admins', $this->data);
    }
    public function admin_create()
    {
        $this->data['other_admins'] = User::where('administration_group_id', '!=', null)->where('id', '!=', auth()->user()->id)->get();
        $this->data['administration_groups'] = AdministrationGroup::all();
        $this->data['countries'] = \App\Model\Country::with('cities')->orderBy('name_ar')->get();
        return view('dashboard.administration.admin_create', $this->data);
    }
    public function admin_store(Request $request)
    {
        $admin_data = [
            'administration_group_id' => $request->administration_group_id,
            'full_name' => $request->full_name,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'city_id' => $request->city_id,
            'password' => $request->password,
            'confirm_password' => $request->confirm_password,
        ];

        $admin_rules = [
            'administration_group_id' => 'required|exists:administration_group,id',
            'full_name' => 'required',
            'mobile' => 'required|unique:user',
            'email' => 'required|unique:user',
//            'city_id' => 'required|exists:city,id',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password',
        ];
        $validator = Validator::make($admin_data, $admin_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $admin = new User();
        $admin->administration_group_id = $request->administration_group_id;
        $admin->full_name = $request->full_name;
        $admin->mobile = $request->mobile;
        $admin->email = $request->email;
        $admin->city_id = $request->city_id;
        $admin->password = bcrypt($request->password);
        if ($request->has('image')) {
            $image_name = ImageController::upload_single($request->image, 'storage/app/user');
            $admin->image = $image_name;
        }
        $admin->save();
        MK_REPORT('dashboard_create_admin', 'Create New Admin Name ' . $admin->full_name, $admin);
        if ($request->back) {
            $forward_url = url('dashboard/administration/admin/create');
        } else {
            $forward_url = url('dashboard/administration/admins');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }
    public function admin_edit($id = 0)
    {
        $admin = User::where('id', $id)->where('administration_group_id', '!=', null)->first();
        if (!$admin) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['admin'] = $admin;
        $this->data['countries'] = \App\Model\Country::with('cities')->orderBy('name_ar')->get();
        $this->data['other_admins'] = User::where('administration_group_id', '!=', null)->where('id', '!=', auth()->user()->id)->get();
        $this->data['administration_groups'] = AdministrationGroup::all();
        return view('dashboard.administration.admin_edit', $this->data);
    }

    public function admin_update(Request $request)
    {
        if (!$request->admin_id || User::find($request->admin_id)->administration_group_id == null) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $admin_id = $request->admin_id;
        $admin_data = [
            'admin_id' => $request->admin_id,
            'administration_group_id' => $request->administration_group_id,
            'full_name' => $request->full_name,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'city_id' => $request->city_id,
            'password' => $request->password,
            'confirm_password' => $request->confirm_password,
        ];

        $admin_rules = [
            'admin_id' => 'required|exists:user,id',
            'administration_group_id' => 'required|exists:administration_group,id',
            'full_name' => 'required',
            'mobile' => 'required|unique:user,mobile,' . $admin_id,
            'email' => 'required|unique:user,email,' . $admin_id,
//            'city_id' => 'required|exists:city,id',
            // 'password' => 'sometimes|min:6',
            // 'confirm_password' => 'sometimes|same:password',
        ];
        $validator = Validator::make($admin_data, $admin_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $admin = User::find($admin_id);
        MK_REPORT('dashboard_update_admin', 'Update Admin Name ' . $admin->full_name, $admin);
        $admin->administration_group_id = $request->administration_group_id;
        $admin->full_name = $request->full_name;
        $admin->mobile = $request->mobile;
        $admin->email = $request->email;
        $admin->city_id = $request->city_id;
        $admin->password = bcrypt($request->password);
        if ($request->has('image')) {
            $image_name = ImageController::upload_single($request->image, 'storage/app/user');
            $admin->image = $image_name;
        }
        $admin->update();
        if ($request->back) {
            $forward_url = url('dashboard/administration/admin/edit') . '/' . $admin->id;
        } else {
            $forward_url = url('dashboard/administration/admins');
        }
        return redirect($forward_url)
            ->with('swal', trans('dash.date_updated_successfully'))
            ->with('icon', 'success')
            ->with('class', 'alert alert-success')
            ->with('message', trans('dash.date_updated_successfully'));
    }
}
