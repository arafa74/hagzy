<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Slide;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlideController extends Controller
{
    public function __construct()
    {
        $this->data['squence_pages'][trans('معرض الصور')] = route('slides');
    }

    public function edit()
    {
        $this->data['slides'] = Slide::all();
        return view('dashboard.slides.update', $this->data);
    }

    public function update(Request $request)
    {

        if ($request->image_id) {
            foreach ($request->image_id as $img) {
                $image = Slide::find($img);
                $image->delete();
            }
        }
        if ($request->images) {
            foreach ($request->images as $img){
                $image = new Slide();
                $image->image = IMAGE_CONTROLLER::upload_single($img, 'storage/app/slides');
                $image->save();
            }
        }
        if ($request->back) {
            $forward_url = url('dashboard/slides/edit');
        } else {
            $forward_url = url('dashboard');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

}
