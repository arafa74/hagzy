<?php

namespace App\Http\Controllers\dashboard;

use App\Category;
use App\Http\Controllers\IMAGE_CONTROLLER;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use Validator;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->data['squence_pages'][trans('site.News')] = route('news');
    }

    public function index()
    {
        $data['news'] = News::all();
//        dd($data['news']);
        return view('dashboard.news.index', $data);
    }

    public function create()
    {
        $this->data['squence_pages'][trans('اضافه جديد')] = route('news_create');
        $this->data['latest_news'] = News::with('category')->orderBy('id', 'desc')->take(10)->get();
        $this->data['categories'] = Category::all();
        return view('dashboard.news.create', $this->data);
    }

    public function store(Request $request)
    {
        $new_data = [
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
            'title_ar' => $request->title_ar,
            'title_en' => $request->title_en,
            'category_id' => $request->category_id
        ];
        $new_rules = [
            'description_ar' => 'required',
            'description_en' => 'required',
            'title_ar' => 'required',
            'title_en' => 'required',
            'category_id' => 'required|exists:categories,id'
        ];
        $validator = Validator::make($new_data, $new_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $new = new News();
        $new->title_ar = $request->title_ar;
        $new->title_en = $request->title_en;
        $new->description_ar = $request->description_ar;
        $new->description_en = $request->description_en;
        $new->category_id = $request->category_id;
        if ($request->hasFile('image')) {
            $new->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/news');
        }
        $new->save();
        // MK_REPORT('dashboard_create_option', 'Create New City  ' . $new->name_ar, $new);
        if ($request->back) {
            $forward_url = url('dashboard/news/create');
        } else {
            $forward_url = url('dashboard/news');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }

    public function edit($id = 0)
    {
        if (!News::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['new'] = News::find($id);
        $this->data['squence_pages'][trans('تعديل خبر')] = route('option_edit');
        $this->data['latest_news'] = News::with('category')->orderBy('id', 'desc')->take(10)->get();
        $this->data['categories'] = Category::all();
        return view('dashboard.news.edit', $this->data);
    }

    public function update(Request $request)
    {
        $new_data = [
            'title_ar' => $request->title_ar,
            'title_en' => $request->title_en,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'category_id' => $request->category_id,
            'new_id' => $request->new_id
        ];
        $new_rules = [
            'description_en' => 'required',
            'description_ar' => 'required',
            'title_ar' => 'required',
            'title_en' => 'required',
            'new_id' => 'required|exists:news,id'
        ];
        $validator = Validator::make($new_data, $new_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $new = News::find($request->new_id);
        // MK_REPORT('dashboard_update_option', 'Update City ' . $new->name_ar, $new);
        $new->title_ar = $request->title_ar;
        $new->title_en = $request->title_en;
        $new->description_ar = $request->description_ar;
        $new->description_en = $request->description_en;
        $new->category_id = $request->category_id;

        $del_old_image = false;
        if ($request->image) {
            if ($new->image) {
                $del_old_image = true;
                $old_image_name = $new->image;
            }
            $new->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/news');
        }

        $new->update();
        if ($request->back) {
            $forward_url = url('dashboard/news/edit') . '/' . $new->id;
        } else {
            $forward_url = url('dashboard/news');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

    public function delete($id)
    {
        if (!$new = News::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        // MK_REPORT('dashboard_delete_bank_account', 'Delete Bank Account Name ' . $bank->bank_name, $bank);
        $new->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
