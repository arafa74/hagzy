<?php

namespace App\Http\Controllers\restaurant;


use App\Contact;
use App\Dish;
use App\Http\Controllers\PARENT_DASHBOARD;
use App\Http\Requests\api\RateRequest;
use App\Model\City;
use App\Model\Country;
use App\Offer;
use App\Option;
use App\Rate;
use App\Reservation;
use App\User;
use Carbon\Carbon;

class HomeController extends PARENT_DASHBOARD
{
    public function index()
    {

        $this->data['user'] = User::find(auth()->user()->id);
        $user = $this->data['user'];
        $this->data['reservations_counter'] = Reservation::where('restaurant_id' , $user->id)->count();
        $this->data['Dishes_counter'] = Dish::where('user_id' , $user->id)->count();
        $this->data['offers_counter'] = Offer::where('user_id' , $user->id)->count();
        $this->data['rates_counter'] = Rate::where('restaurant_id' , $user->id)->count();


        return view('restaurant.home.index', $this->data);
    }
}
