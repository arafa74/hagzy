<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\PARENT_DASHBOARD;
use App\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
class OptionController extends PARENT_DASHBOARD
{
    public function __construct()
    {
        $this->data['squence_pages'][trans('dash.options')] = route('options');
    }

    public function index()
    {
        $this->data['options'] = Option::all();
        return view('dashboard.options.index', $this->data);
    }

    public function create()
    {
        $this->data['squence_pages'][trans('اضافه جديد')] = route('option_create');
        $this->data['latest_options'] = Option::orderBy('id', 'desc')->take(10)->get();
        return view('dashboard.options.create', $this->data);
    }

    public function store(Request $request)
    {
        $option_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
        ];
        $option_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
        ];
        $validator = Validator::make($option_data, $option_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $option = new Option();
        $option->name_ar = $request->name_ar;
        $option->name_en = $request->name_en;

        $option->save();
        // MK_REPORT('dashboard_create_option', 'Create New City  ' . $option->name_ar, $option);
        if ($request->back) {
            $forward_url = url('dashboard/option/create');
        } else {
            $forward_url = url('dashboard/option');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }

    public function edit($id = 0)
    {
        if (!Option::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['option'] = Option::find($id);
        $this->data['squence_pages'][trans('dash.edit_option')] = route('option_edit');
        $this->data['latest_options'] = Option::orderBy('id', 'desc')->take(10)->get();
        return view('dashboard.options.edit', $this->data);
    }

    public function update(Request $request)
    {
        $option_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'option_id' => $request->option_id
        ];
        $option_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
        ];
        $validator = Validator::make($option_data, $option_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $option = Option::find($request->option_id);
        // MK_REPORT('dashboard_update_option', 'Update City ' . $option->name_ar, $option);
        $option->name_ar = $request->name_ar;
        $option->name_en = $request->name_en;

        $option->update();
        if ($request->back) {
            $forward_url = url('dashboard/option/edit') . '/' . $option->id;
        } else {
            $forward_url = url('dashboard/option');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

    public function delete($id)
    {
        if (!$option = Option::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        // MK_REPORT('dashboard_delete_bank_account', 'Delete Bank Account Name ' . $bank->bank_name, $bank);
        $option->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
