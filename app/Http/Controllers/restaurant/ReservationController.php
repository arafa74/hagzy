<?php

namespace App\Http\Controllers\restaurant;

use App\Http\Controllers\api\NotificationController;
use App\Http\Controllers\FCM_CONTROLLER;
use App\Http\Controllers\PARENT_DASHBOARD;
use App\Reservation;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;

class ReservationController extends PARENT_DASHBOARD
{
    public function index(){
        $data['reservations'] = Reservation::where('restaurant_id' , auth()->user()->id)->get();
        return view('restaurant.reservations.index', $data);
    }

    public function show($id){
        $data['reservation'] = Reservation::find($id);
        return view('restaurant.reservations.view', $data);
    }

    public function delete($id)
    {
        if (!$reservation = Reservation::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $reservation->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }

    public function update(Request $request)
    {
        $type_data = [
            'status' => $request->status,
        ];
        $type_rules = [
            'status' => 'required',
        ];
        $validator = Validator::make($type_data, $type_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $reservation = Reservation::find($request->reservation_id);
        $reservation->status = $request->status;
        $reservation->update();

        if ($request->status == 'accepted'){
            NotificationController::MK_NOTIFY($reservation->user->id , 'accept' ,auth()->user()->full_name .' '.' تم قبول الحجز الخاص بك بواسطه' , 'your reservation accepted by'.' '.auth()->user()->full_name);
            $fcm_data['title'] = SETTING_VALUE('APP_NAME_AR');
            $fcm_data['body'] = auth()->user()->full_name .' '.' تم قبول الحجز الخاص بك بواسطه';
            $fcm_data['restaurant_id'] = $reservation->restaurant_id;
            $fcm_data['key'] = "accept";
            FCM_CONTROLLER::SEND_FCM_NOTIFICATION($reservation->user_id ,SETTING_VALUE('APP_NAME_AR'),auth()->user()->full_name .' '.' تم قبول الحجز الخاص بك بواسطه' ,$fcm_data);
        }else{
            NotificationController::MK_NOTIFY($reservation->user->id , 'reject' ,auth()->user()->full_name .' '.' تم رفض الحجز الخاص بك بواسطه' , 'your reservation rejected by'.' '.auth()->user()->full_name);
            $fcm_data['title'] = SETTING_VALUE('APP_NAME_AR');
            $fcm_data['body'] = auth()->user()->full_name .' '.' تم رفض الحجز الخاص بك بواسطه';
            $fcm_data['restaurant_id'] = $reservation->restaurant_id;
            $fcm_data['key'] = "reject";
            FCM_CONTROLLER::SEND_FCM_NOTIFICATION($reservation->user_id ,SETTING_VALUE('APP_NAME_AR'),auth()->user()->full_name .' '.' تم رفض الحجز الخاص بك بواسطه' ,$fcm_data);
        }


        if ($request->back) {
            $forward_url = url('restaurant/reservations/reservation') . '/' . $reservation->id;
        } else {
            $forward_url = url('restaurant/reservations');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }
}
