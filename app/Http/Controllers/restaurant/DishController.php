<?php

namespace App\Http\Controllers\restaurant;

use App\Category;
use App\Dish;
use App\Http\Controllers\IMAGE_CONTROLLER;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
class DishController extends Controller
{
    public function __construct()
    {
        $this->data['squence_pages'][trans('الاطباق')] = route('restaurant_dishes');
        $this->mainRedirect = 'restaurant.dishes.';
    }

    public function index()
    {

        $this->data['dishes'] = Dish::where('user_id' , auth()->user()->id)->get();
        return view($this->mainRedirect . 'index', $this->data);
    }

    public function show($id)
    {
        $data['dish'] = Dish::find($id);
        return view($this->mainRedirect . 'view', $data);
    }

    public function create()
    {
        $data['categories'] = Category::all();
        return view($this->mainRedirect . 'create', $data);
    }

    public function store(Request $request)
    {
        $valid_data = [
            'name' => $request->name,
            'image' => $request->image ,
            'description' => $request->description,
            'price' => $request->price,
            'category_id' => $request->category_id
        ];
        $valid_rules = [
            'name' => 'required',
            'image' => 'required',
            'category_id' => 'required|numeric|exists:categories,id',
            'price' => 'required|numeric',
            'description' => 'required'
        ];
        $validator = Validator::make($valid_data, $valid_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $dish = new Dish();
        $dish->name = $request->name;
        $dish->price = $request->price;
        $dish->category_id = $request->category_id;
        $dish->description = $request->description;
        $dish->user_id = auth()->user()->id;
        $dish->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/dishes');

        $dish->save();
        if ($request->back) {
            $forward_url = url('restaurant/dishes/create');
        } else {
            $forward_url = url('restaurant/dishes');
        }
        return redirect($forward_url)
            ->with('swal', trans('dash.added_successfully'))
            ->with('icon', 'success')
            ->with('class', 'alert alert-success')
            ->with('message', trans('dash.added_successfully'));
    }

    public function edit($id = 0)
    {
        if (!Dish::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['dish'] = Dish::find($id);
        $this->data['squence_pages']['تعديل بيانات الطبق'] = route('restaurant_dishes_edit');
        $this->data['categories'] = Category::all();
        return view('restaurant.dishes.edit', $this->data);
    }

    public function update(Request $request)
    {
        $valid_data = [
            'name' => $request->name,
            'image' => $request->image ,
            'description' => $request->description,
            'price' => $request->price,
            'category_id' => $request->category_id
        ];
        $valid_rules = [
            'name' => 'required',
            'image' => 'required',
            'category_id' => 'required|numeric|exists:categories,id',
            'price' => 'required|numeric',
            'description' => 'required'
        ];
        $validator = Validator::make($valid_data, $valid_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $dish = Dish::find($request->dish_id);
        $dish->name = $request->name;
        $dish->price = $request->price;
        $dish->category_id = $request->category_id;
        $dish->description = $request->description;
        $dish->user_id = auth()->user()->id;
        $dish->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/dishes');
        $dish->update();
        if ($request->back) {
            $forward_url = url('restaurant/dishes/edit') . '/' . $dish->id;
        } else {
            $forward_url = url('restaurant/dishes');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

    public function delete($id = 0)
    {
        if (!$dish = Dish::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $dish->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
