<?php

namespace App\Http\Controllers\restaurant;

use App\Category;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Controllers\PARENT_DASHBOARD;
use App\Offer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class OfferController extends PARENT_DASHBOARD
{
    public function __construct()
    {
        $this->data['squence_pages'][trans('site.offers')] = route('restaurant_offers');
    }

    public function index()
    {
        $this->data['offers'] = Offer::where('user_id' , auth()->user()->id)->get();
        return view('restaurant.offers.index', $this->data);
    }

    public function create()
    {
        $this->data['squence_pages'][trans('اضافه جديد')] = route('restaurant_offer_create');
        return view('restaurant.offers.create', $this->data);
    }

    public function store(Request $request)
    {
        $offer_data = [
            'description' => $request->description,
            'from' => $request->from,
            'to' => $request->to,
            'personNum' => $request->personNum,
            'discount' => $request->discount,
        ];
        $offer_rules = [
            'description' => 'required',
            'from' => 'required',
            'to' => 'required',
            'discount' => 'required',
            'personNum' => 'required',
        ];
        $validator = Validator::make($offer_data, $offer_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $offer = new Offer();
        $offer->description = $request->description;
        $offer->to = $request->to;
        $offer->from = $request->from;
        $offer->discount = $request->discount;
        $offer->personNum = $request->personNum;
        $offer->user_id = auth()->user()->id;

        $offer->save();
        if ($request->back) {
            $forward_url = url('restaurant/offer/create');
        } else {
            $forward_url = url('restaurant/offer');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }

    public function edit($id = 0)
    {
        if (!Offer::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['offer'] = Offer::find($id);
        $this->data['squence_pages'][trans('تعديل عرض')] = route('restaurant_offer_edit');
        return view('restaurant.offers.edit', $this->data);
    }

    public function update(Request $request)
    {
//        $offer_data = [
//            'description' => $request->description_en,
//            'discount' => $request->description_ar,
//            'personNum' => $request->category_id,
//            'from' => $request->offer_id,
//            'to' => $request->offer_id
//        ];
//        $offer_rules = [
//            'description' => 'required',
//            'discount' => 'required',
//            'personNum' => 'required',
//            'from' => 'required',
//            'to' => 'required',
//        ];
//        $validator = Validator::make($offer_data, $offer_rules);
//        if ($validator->fails()) {
//            return back()->withErrors($validator)->withInput();
//        }
        $offer = Offer::find($request->offer_id);
        if ($request->description){
            $offer->description = $request->description;
        }
        else{
            $offer->description = $offer->description;
        }

        if ($request->discount){
            $offer->discount = $request->discount;
        }else{
            $offer->discount = $offer->discount;
        }

        if ($request->personNum){
            $offer->personNum = $request->personNum;
        }else{
            $offer->personNum = $offer->personNum;
        }
        if ($request->from){
            $offer->from = $request->from;
        }else{
            $offer->from = $offer->from;
        }

        if ($request->to){
            $offer->to = $request->to;
        }else{
            $offer->to = $offer->to;
        }

        $offer->update();
        if ($request->back) {
            $forward_url = url('restaurant/offer/edit') . '/' . $offer->id;
        } else {
            $forward_url = url('restaurant/offer');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

    public function delete($id)
    {
        if (!$offer = Offer::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        // MK_REPORT('dashboard_delete_bank_account', 'Delete Bank Account Name ' . $bank->bank_name, $bank);
        $offer->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
