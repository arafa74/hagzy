<?php

namespace App\Http\Controllers\dashboard;

use App\Car;
use App\CarOption;
use App\Category;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Controllers\PARENT_DASHBOARD;
use App\Image;
use App\Option;
use App\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class CarController extends PARENT_DASHBOARD
{
    public function __construct()
    {
        $this->data['squence_pages'][trans('dash.cars')] = route('cars');
    }

    public function index()
    {
        $this->data['cars'] = Car::all();
        return view('dashboard.cars.index', $this->data);
    }

    public function create()
    {
        $this->data['squence_pages'][trans('اضافه جديد')] = route('car_create');
        $this->data['latest_cars'] = Car::with('category')->orderBy('id', 'desc')->take(10)->get();
        $this->data['categories'] = Category::all();
        $this->data['types'] = Type::all();
        $this->data['options'] = Option::all();
        return view('dashboard.cars.create', $this->data);
    }

    public function store(Request $request)
    {
        $type_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'category_id' => $request->category_id,
            'type_id' => $request->type_id,
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
            'year_model' => $request->year_model,
            'status' => $request->status,

        ];
        $type_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
            'description_ar' => 'required',
            'description_en' => 'required',
            'category_id' => 'required|exists:categories,id',
            'type_id' => 'required|exists:types,id',
            'year_model' => 'required',
            'status' => 'required',
        ];
        $validator = Validator::make($type_data, $type_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $car = new Car();
        $car->name_ar = $request->name_ar;
        $car->name_en = $request->name_en;
        $car->description_ar = $request->description_ar;
        $car->description_en = $request->description_en;
        $car->category_id = $request->category_id;
        $car->type_id = $request->type_id;
        $car->year_model = $request->year_model;
        $car->status = $request->status;

        $car->save();
        if ($request->options != null){
            foreach ($request->options as $option_id){
                $option = new CarOption();
                $option->option_id = $option_id;
                $option->car_id = $car->id;
                $option->save();
            }
        }else{
            $car->delete();
            $type_rules = [
                'options' => 'required',
            ];
            $validator = Validator::make($type_data, $type_rules);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
        }

        if ($request->images != null){
            foreach ($request->images as $img){
                $image = new Image();
                $image->car_id  = $car->id;
                $image->image = IMAGE_CONTROLLER::upload_single($img, 'storage/app/cars');
                $image->save();
            }
        }else{
            $car->delete();
            $type_rules = [
                'images' => 'required',
            ];
            $validator = Validator::make($type_data, $type_rules);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
        }

         MK_REPORT('dashboard_create_option', 'Create New car ' . $car->name_ar , $car);
        if ($request->back) {
            $forward_url = url('dashboard/car/create');
        } else {
            $forward_url = url('dashboard/car');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }

    public function edit($id = 0)
    {
        if (!Car::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['car'] = Car::find($id);
        $this->data['squence_pages'][trans('تعديل سياره')] = route('car_edit');
        $this->data['latest_cars'] = Car::with('category')->with('type')->orderBy('id', 'desc')->take(10)->get();
        $this->data['categories'] = Category::all();
        $this->data['types'] = Type::all();
        $this->data['options'] = Option::all();
        $this->data['checked'] = false;
        foreach ($this->data['options'] as $option){
            if(CarOption::where(['car_id' => $this->data['car']->id , 'option_id' => $option->id ])->exists()){
                $this->data['checked'] = true;
            }
    }
        return view('dashboard.cars.edit', $this->data);
    }

    public function update(Request $request)
    {
        $type_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'category_id' => $request->category_id,
            'type_id' => $request->type_id,
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
        ];
        $type_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
            'description_ar' => 'required',
            'description_en' => 'required',
            'category_id' => 'required|exists:categories,id',
            'type_id' => 'required|exists:types,id',
        ];
        $validator = Validator::make($type_data, $type_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $car = Car::find($request->car_id);
         MK_REPORT('dashboard_update_car', 'Update car ' . $car->name_ar, $car);
        $car->name_ar = $request->name_ar;
        $car->name_en = $request->name_en;
        $car->description_ar = $request->description_ar;
        $car->description_en = $request->description_en;
        $car->category_id = $request->category_id;
        $car->type_id = $request->type_id;

        if ($request->options != null) {
            CarOption::where(['car_id' =>$request->car_id])->delete();
            if ($request->options) {
                foreach ($request['options'] as $option_id) {
                    $option = new CarOption();
                    $option->option_id = $option_id;
                    $option->car_id = $car->id;
                    $option->save();
                }
            }
        }

        if ($request->image_id) {
            foreach ($request->image_id as $img) {
                $image = Image::find($img);
                $image->delete();
            }
        }
        if ($request->images) {
            foreach ($request->images as $img){
                $image = new Image();
                $image->car_id  = $car->id;
                $image->image = IMAGE_CONTROLLER::upload_single($img, 'storage/app/cars');
                $image->save();
            }
        }
        $car->update();
        if ($request->back) {
            $forward_url = url('dashboard/car/edit') . '/' . $car->id;
        } else {
            $forward_url = url('dashboard/car');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

    public function delete($id)
    {
        if (!$car = Car::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        // MK_REPORT('dashboard_delete_bank_account', 'Delete Bank Account Name ' . $bank->bank_name, $bank);
        $car->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
