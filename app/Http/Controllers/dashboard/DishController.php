<?php

namespace App\Http\Controllers\dashboard;

use App\Dish;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DishController extends Controller
{
     public function __construct()
    {
        $this->data['squence_pages'][trans('الاطباق')] = route('dishes');
        $this->mainRedirect = 'dashboard.dishes.';
    }

    public function index()
    {
        $this->data['dishes'] = Dish::all();
        return view($this->mainRedirect . 'index', $this->data);
    }

    public function show($id)
    {
        $data['dish'] = Dish::find($id);
        return view($this->mainRedirect . 'view', $data);
    }

    public function delete($id = 0)
    {
        if (!$dish = Dish::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $dish->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
