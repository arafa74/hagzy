<?php

namespace App\Http\Controllers\dashboard;

use App\Category;
use App\Http\Controllers\PARENT_DASHBOARD;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
class ServiceController extends PARENT_DASHBOARD
{
    public function __construct()
    {
        $this->data['squence_pages']['الخدمات'] = route('services');
    }

    public function index()
    {
        $this->data['services'] = Service::with('category')->get();
        return view('dashboard.services.index', $this->data);
    }

    public function create()
    {
        $this->data['squence_pages'][trans('اضافه خدمه جديده')] = route('service_create');
        $this->data['latest_services'] = Service::with('category')->orderBy('id', 'desc')->take(10)->get();
        $this->data['categories'] = Category::all();
        return view('dashboard.services.create', $this->data);
    }

    public function store(Request $request)
    {
        $service_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'category_id' => $request->category_id
        ];
        $service_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
            'category_id' => 'required|exists:categories,id'
        ];
        $validator = Validator::make($service_data, $service_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $service = new Service();
        $service->name_ar = $request->name_ar;
        $service->name_en = $request->name_en;
        $service->category_id = $request->category_id;


        $service->save();
        // MK_REPORT('dashboard_create_service', 'Create New City  ' . $service->name_ar, $service);
        if ($request->back) {
            $forward_url = url('dashboard/service/create');
        } else {
            $forward_url = url('dashboard/service');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }

    public function edit($id = 0)
    {
        if (!Service::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['service'] = Service::find($id);
        $this->data['squence_pages']['تعديل بيانات الخدمه'] = route('service_edit');
        $this->data['latest_services'] = Service::with('category')->orderBy('id', 'desc')->take(10)->get();
        $this->data['categories'] = Category::all();
        return view('dashboard.services.edit', $this->data);
    }

    public function update(Request $request)
    {
        $service_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'category_id' => $request->category_id,
            'service_id' => $request->service_id
        ];
        $service_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
            'service_id' => 'required|exists:services,id'
        ];
        $validator = Validator::make($service_data, $service_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $service = Service::find($request->service_id);
//         MK_REPORT('dashboard_update_service', 'Update City ' . $service->name_ar, $service);
        $service->name_ar = $request->name_ar;
        $service->name_en = $request->name_en;
        $service->category_id = $request->category_id;

        $service->update();
        if ($request->back) {
            $forward_url = url('dashboard/service/edit') . '/' . $service->id;
        } else {
            $forward_url = url('dashboard/service');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

    public function delete($id)
    {
        if (!$service = Service::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        // MK_REPORT('dashboard_delete_bank_account', 'Delete Bank Account Name ' . $bank->bank_name, $bank);
        $service->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
