<?php

namespace App\Http\Controllers\dashboard;

use App\Experience;
use App\Http\Controllers\PARENT_DASHBOARD;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
class ExperienceController extends PARENT_DASHBOARD
{
    public function __construct()
    {
        $this->data['squence_pages']['مستويات الخبره'] = route('experiences');
    }

    public function index()
    {
        $this->data['experiences'] = Experience::all();
        return view('dashboard.experiences.index', $this->data);
    }

    public function create()
    {
        $this->data['squence_pages'][trans('اضافه مستوى جديد')] = route('experience_create');
        $this->data['latest_experiences'] = Experience::orderBy('id', 'desc')->take(10)->get();
        return view('dashboard.experiences.create', $this->data);
    }

    public function store(Request $request)
    {
        $service_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'points' => $request->points,
            'color' => $request->color,
            'likes' => $request->likes,
            'reservations' => $request->reservations,
            'rates' => $request->rates,
        ];
        $service_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
            'points' => 'required|numeric',
            'color' => 'required',
            'likes' => 'required',
            'reservations' => 'required',
            'rates' => 'required',
        ];
        $validator = Validator::make($service_data, $service_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $experience = new Experience();
        $experience->name_ar = $request->name_ar;
        $experience->name_en = $request->name_en;
        $experience->points = $request->points;
        $experience->color= $request->color;
        $experience->likes= $request->likes;
        $experience->reservations= $request->reservations;
        $experience->rates= $request->rates;


        $experience->save();
        // MK_REPORT('dashboard_create_service', 'Create New City  ' . $service->name_ar, $service);
        if ($request->back) {
            $forward_url = url('dashboard/experiences/create');
        } else {
            $forward_url = url('dashboard/experiences');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }

    public function edit($id = 0)
    {
        if (!Experience::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['experience'] = Experience::find($id);
        $this->data['squence_pages']['تعديل بيانات المستوى'] = route('experience_edit');
        $this->data['latest_services'] = Experience::orderBy('id', 'desc')->take(10)->get();
        return view('dashboard.experiences.edit', $this->data);
    }

    public function update(Request $request)
    {
        $service_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'points' => $request->points,
            'color' => $request->color,
            'likes' => $request->likes,
            'reservations' => $request->reservations,
            'rates' => $request->rates,
        ];
        $service_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
            'points' => 'required|numeric',
            'color' => 'required',
            'likes' => 'required',
            'reservations' => 'required',
            'rates' => 'required',
        ];
        $validator = Validator::make($service_data, $service_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $experience = Experience::find($request->experience_id);
        $experience->name_ar = $request->name_ar;
        $experience->name_en = $request->name_en;
        $experience->points = $request->points;
        $experience->color= $request->color;
        $experience->likes= $request->likes;
        $experience->reservations= $request->reservations;
        $experience->rates= $request->rates;

        $experience->update();
        if ($request->back) {
            $forward_url = url('dashboard/experiences/edit') . '/' . $experience->id;
        } else {
            $forward_url = url('dashboard/experiences');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

    public function delete($id)
    {
        if (!$experience = Experience::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        // MK_REPORT('dashboard_delete_bank_account', 'Delete Bank Account Name ' . $bank->bank_name, $bank);
        $experience->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
