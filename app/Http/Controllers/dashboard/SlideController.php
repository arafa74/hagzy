<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Slide;
use App\User;
use App\Offer;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlideController extends Controller
{
    public function __construct()
    {
        $this->data['squence_pages'][trans('معرض الصور')] = route('slides');
    }

    public function index()
    {
        $this->data['slides'] = Slide::orderBy('id'  , 'DESC')->get();
        return view('dashboard.slides.index', $this->data);
    }
    
    public function create()
    {
        $this->data['offers'] = Offer::all();
        $this->data['restaurants'] = User::where('type' , 'restaurant')->get();
        return view('dashboard.slides.create', $this->data);
    }
    
    public function store(Request $request)
    {
        $valid_data = [
            'offer_id' => $request->offer_id,
            'restaurant_id' => $request->restaurant_id,
            'image' => $request->image,
            'is_home' => $request->is_home,
        ];
        $valid_rules = [
            'offer_id' => 'required',
            'restaurant_id' => 'required',
            'is_home' => 'required',
            'image' => 'required|image'
        ];
        $validator = Validator::make($valid_data, $valid_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $slide = new Slide();
        $slide->offer_id = $request->offer_id;
        $slide->restaurant_id = $request->restaurant_id;
        $slide->is_home = $request->is_home;
        $slide->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/slides');
        $slide->save();
        if ($request->back) {
            $forward_url = url('dashboard/slides/create');
        } else {
            $forward_url = url('dashboard/slides');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }
    
    // public function edit()
    // {
    //     $this->data['slides'] = Slide::all();
    //     return view('dashboard.slides.update', $this->data);
    // }
    
    public function edit($id = 0)
    {
        if (!Slide::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['squence_pages'][trans('تعديل بيانات المعرض')] = route('slide_edit');
        $this->data['offers'] = Offer::all();
        $this->data['restaurants'] = User::where('type' , 'restaurant')->get();
        $this->data['slide'] = Slide::find($id);
        return view('dashboard.slides.update', $this->data);
    }

    public function update(Request $request)
    {
         $valid_data = [
            'offer_id' => $request->offer_id,
            'restaurant_id' => $request->restaurant_id,
            'image' => $request->image,
            'is_home' => $request->is_home,
        ];
        $valid_rules = [
            'offer_id' => 'required',
            'restaurant_id' => 'required',
            'is_home' => 'required',
            // 'image' => 'required|image'
        ];
        $validator = Validator::make($valid_data, $valid_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $slide = Slide::find($request->slide_id);
        $slide->offer_id = $request->offer_id;
        $slide->restaurant_id = $request->restaurant_id;
        $slide->is_home = $request->is_home;
        $del_old_image = false;
        if ($request->image) {
            if ($slide->image) {
                $del_old_image = true;
                $old_image_name = $slide->image;
            }
            $slide->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/slides');
        }
        $slide->update();
        if ($del_old_image) {
            IMAGE_CONTROLLER::delete_image($old_image_name, 'bank');
        }
        if ($request->back) {
            $forward_url = url('dashboard/slides/edit') . '/' . $slide->id;
        } else {
            $forward_url = url('dashboard/slides');
        }
        return redirect($forward_url)
            ->with('swal', trans('dash.date_updated_successfully'))
            ->with('icon', 'success')
            ->with('class', 'alert alert-success')
            ->with('message', trans('dash.edited_successfully'));
    }
    
    public function delete($id = 0)
    {
        if (!$slide = Slide::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $image = $bank->image;
        if ($image) {
            IMAGE_CONTROLLER::delete_image($image, 'slides');
        }
        $slide->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }

}
