<?php

namespace App\Http\Controllers\dashboard;

use App\Category;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Controllers\PARENT_DASHBOARD;
use App\Offer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class OfferController extends PARENT_DASHBOARD
{
    public function __construct()
    {
        $this->data['squence_pages'][trans('site.offers')] = route('offers');
    }

    public function index()
    {
        $this->data['offers'] = Offer::all();
        return view('dashboard.offers.index', $this->data);
    }

    public function create()
    {
        $this->data['squence_pages'][trans('اضافه جديد')] = route('offer_create');
        $this->data['latest_offers'] = Offer::with('category')->orderBy('id', 'desc')->take(10)->get();
        $this->data['categories'] = Category::all();
        return view('dashboard.offers.create', $this->data);
    }

    public function store(Request $request)
    {
        $offer_data = [
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
            'category_id' => $request->category_id
        ];
        $offer_rules = [
            'description_ar' => 'required',
            'description_en' => 'required',
            'category_id' => 'required|exists:categories,id'
        ];
        $validator = Validator::make($offer_data, $offer_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $offer = new Offer();
        $offer->description_ar = $request->description_ar;
        $offer->description_en = $request->description_en;
        $offer->category_id = $request->category_id;
        if ($request->hasFile('image')) {
            $offer->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/offers');
        }
        $offer->save();
        // MK_REPORT('dashboard_create_option', 'Create New City  ' . $offer->name_ar, $offer);
        if ($request->back) {
            $forward_url = url('dashboard/offer/create');
        } else {
            $forward_url = url('dashboard/offer');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }

    public function edit($id = 0)
    {
        if (!Offer::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['offer'] = Offer::find($id);
        $this->data['squence_pages'][trans('تعديل عرض')] = route('option_edit');
        $this->data['latest_offers'] = Offer::with('category')->orderBy('id', 'desc')->take(10)->get();
        $this->data['categories'] = Category::all();
        return view('dashboard.offers.edit', $this->data);
    }

    public function update(Request $request)
    {
        $offer_data = [
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'category_id' => $request->category_id,
            'offer_id' => $request->offer_id
        ];
        $offer_rules = [
            'description_en' => 'required',
            'description_ar' => 'required',
            'offer_id' => 'required|exists:offers,id'
        ];
        $validator = Validator::make($offer_data, $offer_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $offer = Offer::find($request->offer_id);
        // MK_REPORT('dashboard_update_option', 'Update City ' . $offer->name_ar, $offer);
        $offer->description_ar = $request->description_ar;
        $offer->description_en = $request->description_en;
        $offer->category_id = $request->category_id;

        $del_old_image = false;
        if ($request->image) {
            if ($offer->image) {
                $del_old_image = true;
                $old_image_name = $offer->image;
            }
            $offer->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/offers');
        }

        $offer->update();
        if ($request->back) {
            $forward_url = url('dashboard/offer/edit') . '/' . $offer->id;
        } else {
            $forward_url = url('dashboard/offer');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

    public function delete($id)
    {
        if (!$offer = Offer::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        // MK_REPORT('dashboard_delete_bank_account', 'Delete Bank Account Name ' . $bank->bank_name, $bank);
        $offer->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
