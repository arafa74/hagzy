<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PARENT_DASHBOARD;
use App\MessageType;
use App\Type;
use Validator;
use App\Contact;

class UserMessageController extends Controller
{
    public function __construct()
    {
        $this->data['squence_pages']['انواع الشكاوى'] = route('types');
    }

    public function index()
    {
        $this->data['types'] = Type::all();
        return view('dashboard.types.index', $this->data);
    }

    public function create()
    {
        $this->data['squence_pages'][trans('اضافه جديد')] = route('type_create');
        $this->data['latest_types'] = Type::orderBy('id', 'desc')->take(10)->get();
        return view('dashboard.types.create', $this->data);
    }

    public function store(Request $request)
    {
        $type_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
        ];
        $type_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
        ];
        $validator = Validator::make($type_data, $type_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $type = new Type();
        $type->name_ar = $request->name_ar;
        $type->name_en = $request->name_en;

        $type->save();
        // MK_REPORT('dashboard_create_option', 'Create New City  ' . $type->name_ar, $type);
        if ($request->back) {
            $forward_url = url('dashboard/type/create');
        } else {
            $forward_url = url('dashboard/type');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }

    public function edit($id = 0)
    {
        if (!Type::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['type'] = Type::find($id);
        $this->data['squence_pages'][trans('تعديل نوع الشكاوى')] = route('type_edit');
        $this->data['latest_types'] = Type::orderBy('id', 'desc')->take(10)->get();
        return view('dashboard.types.edit', $this->data);
    }

    public function update(Request $request)
    {
        $type_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'type_id' => $request->type_id
        ];
        $type_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
            'type_id' => 'required|exists:types,id'
        ];
        $validator = Validator::make($type_data, $type_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $type = Type::find($request->type_id);
        // MK_REPORT('dashboard_update_option', 'Update City ' . $type->name_ar, $type);
        $type->name_ar = $request->name_ar;
        $type->name_en = $request->name_en;

        $type->update();
        if ($request->back) {
            $forward_url = url('dashboard/type/edit') . '/' . $type->id;
        } else {
            $forward_url = url('dashboard/type');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }

    public function delete($id)
    {
        if (!$type = Type::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        // MK_REPORT('dashboard_delete_bank_account', 'Delete Bank Account Name ' . $bank->bank_name, $bank);
        $type->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }

    public function contacts()
    {
        $data['contacts'] = Contact::orderBy('created_at', 'DESC')->get();
        return view('dashboard.contacts.index', $data);
    }

    public function delete_contact($id = 0)
    {
        if (!$contact = Contact::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $contact->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
