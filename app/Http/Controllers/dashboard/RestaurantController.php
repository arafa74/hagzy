<?php

namespace App\Http\Controllers\dashboard;

use App\Bunch;
use App\Category;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Requests\dashboard\RestaurantRequest;
use App\Http\Requests\dashboard\UserRequest;
use App\Like;
use App\Model\City;
use App\Model\Token;
use App\Option;
use App\Rate;
use App\Reservation;
use App\User;
use App\UserCategory;
use App\UserOption;
// use App\city;
use Illuminate\Http\Request;
use App\Http\Controllers\PARENT_DASHBOARD;
use Auth;
use Validator;
use JWTAuth;


class RestaurantController extends PARENT_DASHBOARD
{
    public function index()
    {
        $data['users'] = User::where('type' ,'restaurant' )->paginate(10);
        return view('dashboard.restaurant.index', $data);
    }

    public function profile($id)
    {
        $data['user']=  User::find($id);
        $user = $data['user'];
        $data['ads_count'] = Reservation::where('restaurant_id' , $data['user']->id)->count();
        $data['user_reservations'] = Reservation::where('restaurant_id' , $data['user']->id)->get();
        $data['user_rates'] = Rate::where('restaurant_id', $user->id)->get();
        $data['user_rates_count'] = Rate::where('restaurant_id', $user->id)->get()->count();
        $data['user_favs_count'] = User::whereHas('like', function($query) use ($user) {
            return $query->where('restaurant_id', $user->id);
        })->count();

        $data['user_favs'] = User::whereHas('like', function($query) use ($user) {
            return $query->where('restaurant_id', $user->id);
        })
            ->get();
        return view('dashboard.restaurant.profile',$data);
    }

    public function create()
    {
        $this->data['options'] = Option::orderBy('created_at','DESC')->get();
        $this->data['bunches'] = Bunch::orderBy('created_at','DESC')->get();
        $this->data['cities'] = City::orderBy('created_at','DESC')->get();
        $this->data['categories'] = Category::orderBy('created_at','DESC')->get();
        $this->data['last_users'] = User::orderBy('created_at','DESC')->where('id','!=',Auth::id())->where('type','restaurant')->take(10)->get();
        return view('dashboard.restaurant.create',$this->data);
    }

    public function store(RestaurantRequest $request)
    {
        $image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/users');
        $user = User::create([
            'full_name'=>$request->full_name,
            'email'=>$request->email,
            'mobile'=>$request->mobile,
            'type'=>'restaurant',
            'image'=>$image,
            'city_id'=>$request->city_id,
            'bunch_id'=>$request->bunch_id,
            'password'=>bcrypt($request->password),
            'active'=>'active',
        ]);

        if ($request->options != null){
            foreach ($request->options as $option_id){
                $option = new UserOption();
                $option->option_id = $option_id;
                $option->user_id = $user->id;
                $option->save();
            }
        }
        if ($request->categories != null){
            foreach ($request->categories as $category_id){
                $category = new UserCategory();
                $category->category_id = $category_id;
                $category->user_id = $user->id;
                $category->save();
            }
        }

        $token = new Token();
        $token->user_id = $user->id;
        $token->fcm = $request->fcm_token;
        $token->device_type = "android";
        $token->jwt = JWTAuth::fromUser($user);
        $token->is_logged_in = 'true';
        $token->ip = $request->ip();
        $token->save();
        MK_REPORT('dashboard_create_user_account', 'Create New User Account  ' . $user->full_name, $user);
        if ($request->has('forward')) {
            $forward_url = url('dashboard/restaurant');
        } else {
            $forward_url = url('dashboard/restaurant/create');
        }
        return redirect($forward_url);
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        return back();
    }

    public function edit($id)
    {
        $this->data['bunches'] = Bunch::orderBy('created_at','DESC')->get();
        $this->data['user'] = User::find($id);
        $this->data['last_users'] = User::orderBy('created_at','DESC')->where('id','!=',Auth::id())->take(10)->get();
        return view('dashboard.restaurant.edit',$this->data);
    }

    public function update(UserRequest $request)
    {
        $user = User::find($request->user_id);

        $validator=Validator::make($request->all(),[
                'email' => 'unique:users,email,'.$user->id,
                'mobile' => 'unique:users,mobile,'.$user->id,
            ]
            ,
            [
                'mobile.unique' => 'رقم هاتف مستخدم من قبل',
                'email.unique' => 'البريد الالكترونى مستخدم من قبل',
            ]
        );

        if ($request->hasFile('image')) {
            $image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/users');
        }else{
            $image = $user->image;
        }

        $update = User::find($request->user_id)->update([
            'full_name'=>$request->full_name,
            'email'=>$request->email,
            'mobile'=>$request->mobile,
            'image'=>$image,
            'city_id'=>$request->city_id,
            'active'=>$request->active,
        ]);

        if ($request->has('passowrd')) {
            if (Hash::check('passowrd',$user->password)) {
                User::find($request->user_id)->update([
                    'password'=>bcrypt($request->password)
                ]);
            }
        }

        MK_REPORT('dashboard_update_user_info', 'update New User info  ' . $user->id, $user);
        if ($request->has('forward')) {
            $forward_url = url('dashboard/restaurant');
        } else {
            $forward_url = url('dashboard/restaurant/edit/'.$request->user_id);
        }
        return redirect($forward_url);
    }
}
