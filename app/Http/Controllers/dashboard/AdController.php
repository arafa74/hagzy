<?php

namespace App\Http\Controllers\dashboard;

use App\Ad;
use App\Http\Controllers\PARENT_DASHBOARD;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdController extends PARENT_DASHBOARD
{
    public function index(){
        $data['ads'] = Ad::paginate(10);
        return view('dashboard.ad.index', $data);
    }
    public function ad($id){
        $data['ad'] = Ad::find($id);
        return view('dashboard.ad.ad', $data);
    }
    public function delete(Request $request ,  $id){
        $data['ad'] = Ad::find($id)->delete();
        $data['ads'] = Ad::paginate(10);

        if ($request->has('forward')) {
            $forward_url = url('dashboard/user');
            return redirect($forward_url);
        }else{
            return redirect()->route('ads' , $data);
        }


    }

}
