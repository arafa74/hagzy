<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\News;
use App\Bunch;
use Validator;


class BunchController extends Controller
{
    
    public function index()
    {
        $data['bunches'] = Bunch::paginate();
        return view('dashboard.bunches.index', $data);
    }

    
    public function create()
    {
        $this->data['squence_pages'][trans('اضافه جديد')] = route('bunches_create');
        $this->data['latest_bunches'] = Bunch::orderBy('id', 'desc')->take(10)->get();
        
        return view('dashboard.bunches.create', $this->data);
    }

    public function store(Request $request)
    {
        $new_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'price' => $request->price,
        ];
        $new_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
            'price' => 'required',
        ];
        $validator = Validator::make($new_data, $new_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $bunch = new Bunch();
        $bunch->name_ar = $request->name_ar;
        $bunch->name_en = $request->name_en;
        $bunch->price = $request->price;
        
        $bunch->save();
        // MK_REPORT('dashboard_create_option', 'Create New City  ' . $new->name_ar, $new);
        if ($request->back) {
            $forward_url = url('dashboard/bunches/create');
        } else {
            $forward_url = url('dashboard/bunches');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.added_successfully'));
    }

    public function edit($id = 0)
    {
        if (!Bunch::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $this->data['bunch'] = Bunch::find($id);
        $this->data['squence_pages'][trans('تعديل ')] = route('bunches_edit');
        $this->data['latest_bunches'] = Bunch::orderBy('id', 'desc')->take(10)->get();
        return view('dashboard.bunches.edit', $this->data);
    }

    public function update(Request $request)
    {
        $new_data = [
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'bunch_id' => $request->bunch_id,
            'price' => $request->price
        ];
        $new_rules = [
            'name_ar' => 'required',
            'name_en' => 'required',
            'price' => 'required',
            'bunch_id' => 'required|exists:bunches,id'
        ];
        $validator = Validator::make($new_data, $new_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $bunch = Bunch::find($request->bunch_id);
        $bunch->name_ar = $request->name_ar;
        $bunch->name_en = $request->name_en;
        $bunch->price = $request->price;


        $bunch->update();
        if ($request->back) {
            $forward_url = url('dashboard/bunches/edit') . '/' . $bunch->id;
        } else {
            $forward_url = url('dashboard/bunches');
        }
        return redirect($forward_url)->with('class', 'alert alert-success')->with('message', trans('dash.edited_successfully'));
    }
    
    public function delete($id)
    {
        if (!$bunch = Bunch::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        // MK_REPORT('dashboard_delete_bank_account', 'Delete Bank Account Name ' . $bank->bank_name, $bank);
        $bunch->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
