<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\PARENT_DASHBOARD;
use App\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationController extends PARENT_DASHBOARD
{
    public function index(){
        $data['reservations'] = Reservation::all();
        return view('dashboard.reservations.index', $data);
    }

    public function show($id){
        $data['reservation'] = Reservation::find($id);
        return view('dashboard.reservations.view', $data);
    }

    public function delete($id)
    {
        if (!$reservation = Reservation::find($id)) {
            return back()->with('class', 'alert alert-danger')->with('message', trans('dash.try_2_access_not_found_content'));
        }
        $reservation->delete();
        return back()->with('class', 'alert alert-success')->with('message', trans('dash.deleted_successfully'));
    }
}
