<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\api\UserCategory;
use App\Http\Resources\Restaurant;
use App\UserOption;
// use App\UserCategory;
use Illuminate\Http\Request;
//use App\Http\Controllers\PARENT_API;
use App\User;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Controllers\PARENT_API;
use App\Model\Follow;
use App\Http\Resources\SingleUser;
use App\Http\Resources\AdvertisesCollection;
use JWTAuth;
use Validator;
class UserControllerController extends PARENT_API
{
    public function profile($id)
    {
        // return userProfile Data
        // Following and follower Counter
        // Advertis Pagination (Such As Response On Home Page [Avertises data] )
        $user=User::find($id);
        if(is_null($user)){
            $this->data['data'] = '';
            $this->data['status'] = "fails";
            $this->data['message'] = '';
            return response()->json($this->data, 405);
        }

        $this->data['data'] = new SingleUser($user);
        $this->data['ads']=new AdvertisesCollection($user->advertises()->paginate(15));
        $this->data['status'] = "ok";
        $this->data['message'] = '';
        return response()->json($this->data, 200);
    }

    public  function updateProfile(Request $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::find($user_data->id);
        $validator=Validator::make($request->all(),[
                'email' => 'unique:user,email,'.$user->id,
                'mobile' => 'unique:user,mobile,'.$user->id,
                // 'options' => 'exists:options,id',
                // 'categories' => 'exists:categories,id',
            ]
            ,
            [
                'mobile.unique' => 'رقم هاتف مستخدم من قبل',
                'email.unique' => 'البريد الالكترونى مستخدم من قبل',
            ]
        );

        if ($validator->fails()) {
            $this->data['data'] = "";
            $this->data['status'] = "validation_errors";
            $this->data['message'] = $validator->messages()->first();
            return response()->json($this->data ,405);
        }
        if($request['name']){
            $user->full_name = $request['name'];
        }else{
            $user->full_name = $user->full_name;
        }
        
        if($request['ar_name']){
            $user->first_name = $request['ar_name'];
        }else{
            $user->first_name = $user->first_name;
        }
        
        if($request['en_name']){
            $user->last_name = $request['en_name'];
        }else{
            $user->last_name = $user->last_name;
        }
        
        
        if($request['extras']){
            $user->extras = $request['extras'];
        }else{
            $user->extras = $user->extras;
        }
        
        if($request['adventages']){
            $user->adventages = $request['adventages'];
        }else{
            $user->adventages = $user->adventages;
        }

        if($request['email']){
            $user->email = $request['email'];
        }else{
            $user->email=$user->email;
        }

        if($request['mobile']){
            $user->mobile = $request['mobile'];
        }else{
            $user->mobile=$user->mobile;
        }

        if($request['latitude']){
            $user->latitude = $request['latitude'];
        }else{
            $user->latitude=$user->latitude;
        }

        if($request['longitude']){
            $user->longitude = $request['longitude'];
        }else{
            $user->longitude=$user->longitude;
        }
        
        
        if($request['password']){
            $user->password = bcrypt($request->password);
        }else{
            $user->password = $user->password;
        }

        if($request['address']){
            $user->address = $request['address'];
        }else{
            $user->address=$user->address;
        }
        if($request['work_time']){
            $user->work_time = $request['work_time'];
        }else{
            $user->work_time =$user->work_time;
        }
        
        if($request['wait_time']){
            $user->bref = $request['wait_time'];
        }else{
            $user->bref =$user->bref;
        }

        if ($request->image) {
            $user->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/users');
        }
        
        if($request->options){
            $options_id = array_column(json_decode($request->options), 'id');
            if ($options_id){
                UserOption::where(['user_id'  => $user->id])->delete();
                foreach ($options_id as $option_id){
                    $userOption = UserOption::create([
                        'option_id' => $option_id,
                        'user_id' => $user->id,
                    ]);
                }
            }
        }


        if($request->categories){
            \App\UserCategory::where(['user_id'  => $user->id])->delete();
            $user->category()->attach(array_column(json_decode($request->categories), 'id'));
        }

        $user->update();
        $this->data['data'] = $user->type == 'user'? new SingleUser($user) :new Restaurant($user);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
    
    public  function updateProfileIos(Request $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::find($user_data->id);
        $validator=Validator::make($request->all(),[
                'email' => 'unique:user,email,'.$user->id,
                'mobile' => 'unique:user,mobile,'.$user->id,
                // 'options' => 'exists:options,id',
                // 'categories' => 'exists:categories,id',
            ]
            ,
            [
                'mobile.unique' => 'رقم هاتف مستخدم من قبل',
                'email.unique' => 'البريد الالكترونى مستخدم من قبل',
            ]
        );

        if ($validator->fails()) {
            $this->data['data'] = "";
            $this->data['status'] = "validation_errors";
            $this->data['message'] = $validator->messages()->first();
            return response()->json($this->data ,405);
        }
        if($request['name']){
            $user->full_name = $request['name'];
        }else{
            $user->full_name = $user->full_name;
        }
        
        if($request['ar_name']){
            $user->first_name = $request['ar_name'];
        }else{
            $user->first_name = $user->first_name;
        }
        
        if($request['en_name']){
            $user->last_name = $request['en_name'];
        }else{
            $user->last_name = $user->last_name;
        }
        
        if($request['extras']){
            $user->extras = $request['extras'];
        }else{
            $user->extras = $user->extras;
        }

        if($request['adventages']){
            $user->adventages = $request['adventages'];
        }else{
            $user->adventages = $user->adventages;
        }

        if($request['email']){
            $user->email = $request['email'];
        }else{
            $user->email=$user->email;
        }

        if($request['mobile']){
            $user->mobile = $request['mobile'];
        }else{
            $user->mobile=$user->mobile;
        }

        if($request['latitude']){
            $user->latitude = $request['latitude'];
        }else{
            $user->latitude=$user->latitude;
        }

        if($request['longitude']){
            $user->longitude = $request['longitude'];
        }else{
            $user->longitude=$user->longitude;
        }
        
        
        if($request['password']){
            $user->password = bcrypt($request->password);
        }else{
            $user->password = $user->password;
        }

        if($request['address']){
            $user->address = $request['address'];
        }else{
            $user->address=$user->address;
        }
        if($request['work_time']){
            $user->work_time = $request['work_time'];
        }else{
            $user->work_time =$user->work_time;
        }
        
        if($request['wait_time']){
            $user->bref = $request['wait_time'];
        }else{
            $user->bref =$user->bref;
        }

        if ($request->image) {
            $user->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/users');
        }
        
        // if($request->options){
        //     $options_id = array_column(json_decode($request->options), 'id');
        //     if ($options_id){
        //         UserOption::where(['user_id'  => $user->id])->delete();
        //         foreach ($options_id as $option_id){
        //             $userOption = UserOption::create([
        //                 'option_id' => $option_id,
        //                 'user_id' => $user->id,
        //             ]);
        //         }
        //     }
        // }


        // if($request->categories){
        //     \App\UserCategory::where(['user_id'  => $user->id])->delete();
        //     $user->category()->attach(array_column(json_decode($request->categories), 'id'));
        // }
        
        
        $options_id = $request->options;
        if ($options_id){
            UserOption::where(['user_id'  => $user->id])->delete();
            foreach ($options_id as $option_id){
                $userOption = UserOption::create([
                    'option_id' => $option_id,
                    'user_id' => $user->id,
                ]);
            }
        }
        if ($request->categories){
             \App\UserCategory::where(['user_id'  => $user->id])->delete();
            $user->category()->attach($request->categories);
        }

        $user->update();
        $this->data['data'] = $user->type == 'user'? new SingleUser($user) :new Restaurant($user);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function toggle_follow($id)
    {
        // check if is_follow or not
        // if follow u will pass data to func _UnFollow else _DoFollow

        $is_follow=Follow::where('user_id',$this->user->id)->where('follower_id',$id)->first();
        if (is_null($is_follow)){
            $this->_DoFollow($id);
            $message=trans('app.follow_successful');
        }else{
            $this->_UnFollow($id);
            $message=trans('app.unfollow_successful');
        }
        $this->data['data'] = '';
        $this->data['status'] = "ok";
        $this->data['message'] = $message;
        return response()->json($this->data, 200);

    }

    public function _DoFollow($id)
    {
        $follow=new Follow();
        $follow->user_id=$this->user->id;
        $follow->follower_id=$id;
        $follow->save();
        return true;

    }

    public function _UnFollow($id)
    {
        $follow=Follow::where('user_id',$this->user->id)->where('follower_id',$id)->first();
        $follow->delete();
        return true;

    }
}
