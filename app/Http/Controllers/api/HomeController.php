<?php

namespace App\Http\Controllers\api;

use App\Category;
use App\Dish;
use App\Like;
use App\Offer;
use App\Service;
use App\Slide;
use App\User;
use App\Rate;
use App\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\PARENT_API;
//use App\Http\Controllers\PARENT_API;
use JWTAuth;
use URL;
class HomeController extends PARENT_API
{
    public function RestaurantHome(){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where('id' , $user_data->id)->first();
        $slides = Slide::where('is_home' , 1)->orderBy('id' , 'DESC')->get();
        $slides_data = [];
        foreach ($slides as $slide){

            $slides_data[] =[
                'image_id'=>$slide->id,
                'restaurant_id'=>$slide->restaurant_id,
                'offer_id'=>$slide->offer_id,
                'image_id'=>$slide->id,
                'imageUrl'=>URL::to('storage/app/slides/org') . '/' .$slide->image,
            ];
        }
        $dishes = Dish::where('user_id' , $user->id)->get();
        $dishes_data = [];
        $name='name_'.app()->getLocale();
        foreach ($dishes as $dish){
            $dishes_data[] =[
                'id'=>$dish->id,
                'name'=>$dish->name,
                'description'=>$dish->description,
                'price'=>$dish->price,
                'category_id'=>$dish->category_id,
                'category_name'=>$dish->category->$name,
                'image'=>URL::to('storage/app/dishes/org') . '/' .$dish->image,
            ];
        }
        $this->data['data'] =  $dishes_data;
        // $this->data['data'] =  ['slides_data'=>$slides_data ,'dishes_data' =>$dishes_data];
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function UserHome(){
        // $user_data = JWTAuth::parseToken()->toUser();
        // $user = User::where('id' , $user_data->id)->first();

        $slides = Slide::where('is_home' , 1)->orderBy('id' , 'DESC')->get();
        $slides_data = [];
        foreach ($slides as $slide){
            $slides_data[] =[
                'image_id'=>$slide->id,
                'restaurant_id'=>$slide->restaurant_id,
                'offer_id'=>$slide->offer_id,
                'imageUrl'=>URL::to('storage/app/slides/org') . '/' .$slide->image,
            ];
        }
        $restaurantsWithOutOffers = User::where('type' ,'restaurant')->doesntHave('offer')->orderBy('id' , 'DESC')->take(3)->get();
        $restaurantsWithOffers    = User::where('type' ,'restaurant')->whereHas('offer')->orderBy('id' , 'DESC')->take(3)->get();
        $restaurantsWithOutOffers_data = [];
        foreach ($restaurantsWithOutOffers as $restaurant){
            $rate_avg = Rate::where( 'restaurant_id' , $restaurant->id )->avg('rate');
            $restaurantsWithOutOffers_data[] =[
                'id'=>$restaurant->id,
                'ar_name'=>$restaurant->first_name?$restaurant->first_name:"",
                'en_name'=>$restaurant->last_name?$restaurant->last_name:"",
                'rates_avrage'=> $rate_avg?(int)$rate_avg:0,
                'image'=>$restaurant->Imageurl,
            ];
        }
        $restaurantsWithOffers_data = [];
        foreach ($restaurantsWithOffers as $restaurant){
            $rate_avg = Rate::where( 'restaurant_id' , $restaurant->id )->avg('rate');
            $restaurantsWithOffers_data[] =[
                'id'=>$restaurant->id,
                'ar_name'=>$restaurant->first_name?$restaurant->first_name:"",
                'en_name'=>$restaurant->last_name?$restaurant->last_name:"",
                'rates_avrage'=> $rate_avg?(int)$rate_avg:0,
                'image'=>$restaurant->Imageurl,
            ];
        }

        $options = Option::orderBy('id' , 'DESC')->get();
        $options_data = [];
        $option_name = 'name_'.app()->getLocale();
        foreach ($options as $option){
            $options_data[] =[
                'id'=>$option->id,
                'name'=>$option->$option_name,
            ];
        }
        $this->data['data'] =  ['options_data'=> $options_data, 'slides_data'=>$slides_data,'restaurantsWithOffers' =>$restaurantsWithOffers_data ,'restaurantsWithOutOffers' =>$restaurantsWithOutOffers_data];
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function all_restaurants_with_offers(Request $request){
        $restaurantsWithOffers    = User::where('type' ,'restaurant')->whereHas('offer')->orderBy('id' , 'DESC')->get();
        $restaurantsWithOffers_data = [];
        foreach ($restaurantsWithOffers as $restaurant){
            $rate_avg = Rate::where('restaurant_id' , $restaurant->id)->avg('rate');
            $offer = Offer::where('user_id' , $restaurant->id)->first();
            $is_like = 0;
            if ($request->headers->has('Authorization')){
                $user_data = JWTAuth::parseToken()->toUser();
                $user = User::where( 'id' , $user_data->id )->first();
                if (Like::where(['user_id' => $user->id , 'restaurant_id' => $restaurant->id])->exists()){
                    $is_like = 1;
                }
            }
            $restaurantsWithOffers_data[] =[
                'id'=>$restaurant->id,
                'ar_name'=>$restaurant->first_name?$restaurant->first_name:"",
                'en_name'=>$restaurant->last_name?$restaurant->last_name:"",
                'extras'=>$restaurant->extras?$restaurant->extras : " ",
                'wait_time'=>$restaurant->wait_time?$restaurant->wait_time : " ",
                'image' => $restaurant->image_url,
                'is_like'=>$is_like,
                'rate_avg'=>(int) $rate_avg,
                'offer'=>$offer->discount,
            ];
        }
        $this->data['data']     =  $restaurantsWithOffers_data;
        $this->data['status']   =  "ok";
        $this->data['message']  =  "";
        return response()->json($this->data, 200);
    }

    public function all_restaurants_with_out_offers(Request $request){
        $restaurantsWithOutOffers = User::where('type' ,'restaurant')->doesntHave('offer')->orderBy('id' , 'DESC')->take(3)->get();
        $restaurantsWithOutOffers_data = [];
        foreach ($restaurantsWithOutOffers as $restaurant){
            $rate_avg = Rate::where('restaurant_id' , $restaurant->id)->avg('rate');
            $offer = Offer::where('user_id' , $restaurant->id)->first();
            $is_like = 0;
            if ($request->headers->has('Authorization')){
                $user_data = JWTAuth::parseToken()->toUser();
                $user = User::where( 'id' , $user_data->id )->first();
                if (Like::where(['user_id' => $user->id , 'restaurant_id' => $restaurant->id])->exists()){
                    $is_like = 1;
                }
            }
            $restaurantsWithOutOffers_data[] =[
                'id'=>$restaurant->id,
                'ar_name'=>$restaurant->first_name?$restaurant->first_name:"",
                'en_name'=>$restaurant->last_name?$restaurant->last_name:"",
                'extras'=>$restaurant->extras?$restaurant->extras : " ",
                'wait_time'=>$restaurant->wait_time?$restaurant->wait_time : " ",
                'image' => $restaurant->image_url,
                'is_like'=>$is_like,
                'rate_avg'=>(int) $rate_avg,
                
            ];
        }
        $this->data['data']     =  $restaurantsWithOutOffers_data;
        $this->data['status']   =  "ok";
        $this->data['message']  =  "";
        return response()->json($this->data, 200);
    }
}
