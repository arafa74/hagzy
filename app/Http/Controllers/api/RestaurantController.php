<?php

namespace App\Http\Controllers\api;

use App\Dish;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\AdDetails;
use App\Like;
use App\Offer;
use App\Rate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use URL;
use JWTAuth;


class RestaurantController extends PARENT_API
{
    public function Restaurant(AdDetails $request){

        $restaurant = User::where( 'id' , $request->restaurant_id )->first();
        $rates_count = Rate::where( 'restaurant_id' , $request->restaurant_id )->count();
        $rates_avg = Rate::where( 'restaurant_id' , $request->restaurant_id )->avg('rate');
        $is_like = 0;
        if ($request->headers->has('Authorization')){
            $user_data = JWTAuth::parseToken()->toUser();
            $user = User::where( 'id' , $user_data->id )->first();
            $like = Like::where(['user_id' => $user->id , 'restaurant_id' => $request->restaurant_id])->exists();
            if (Like::where(['user_id' => $user->id , 'restaurant_id' => $request->restaurant_id])->exists()){
                $is_like = 1;
            }
        }

        $offer_details = "";
        $offer = Offer::where('user_id' , $request->restaurant_id)->orderby('created_at', 'desc')->first();
        if ($offer){
            $offer_details = $offer->description;
        }
        $data = [
            'id'=>$request->restaurant_id,
            'ar_name'=>$restaurant->first_name?$restaurant->first_name:"",
            'en_name'=>$restaurant->last_name?$restaurant->last_name:"",
            'extras'=>$restaurant->extras?$restaurant->extras : " ",
            'adventages'=>$restaurant->adventages?$restaurant->adventages : " ",
            'wait_time'=>$restaurant->bref?$restaurant->bref : " ",
            'user_avatar' => $restaurant->image_url,
            'is_like'=> $is_like,
            'mobile'=>$restaurant->mobile,
            'email'=>$restaurant->email,
            'offer'=>$offer_details,
            'work_time'=>$restaurant->work_time?$restaurant->work_time: "",
            'lat'=>$restaurant->latitude?$restaurant->latitude: "",
            'lan'=>$restaurant->longitude?$restaurant->longitude: "",
            'address'=>$restaurant->address?$restaurant->address: "",
            'rates_count'=>$rates_count,
            'rates_avrage'=>(int) $rates_avg,
        ];


        $rates = Rate::where( 'restaurant_id' , $request->restaurant_id )->orderBy('id', 'desc')->take(4)->get();
        $rates_data = [];
        foreach ($rates as $rate)
            $rates_data []= [
                'id'=>$rate->id,
                'comment'=>$rate->comment,
                'rate'=>$rate->rate,
                'name'=>User::where('id' ,$rate->user_id)->first()->full_name,
                'user_avatar'=>$rate->user->image_url,
                'create_at'=>date_format( $rate->created_at, 'Y/m/d'),
            ];

        $menu = Dish::where( 'user_id' , $request->restaurant_id )->orderBy('id', 'desc')->take(4)->get();
        $dishes = [];
        foreach ($menu as $dish)
            $dishes []= [
                'id'=>$dish->id,
                'name'=>$dish->name,
                'price'=>$dish->price,
                'image' =>URL::to('storage/app/dishes/org').'/'. $dish->image,
        ];

        $this->data['data'] =  ['general_info' => $data  , 'rates'=> $rates_data , 'dishs'=> $dishes ];
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function Menu(AdDetails $request){

        $menu = Dish::where( 'user_id' , $request->restaurant_id )->get();
        $dishes = [];
        foreach ($menu as $dish)
            $dishes []= [
                'id'=>$dish->id,
                'name'=>$dish->name,
                'price'=>$dish->price,
                'image' =>URL::to('storage/app/dishes/org').'/'. $dish->image,
        ];

        $this->data['data'] =  $dishes;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function rates(AdDetails $request){

        $rates = Rate::where( 'restaurant_id' , $request->restaurant_id )->get();
        $data = [];
        foreach ($rates as $rate)
            $data []= [
                'id'=>$rate->id,
                'comment'=>$rate->comment,
                'rate'=>$rate->rate,
                'name'=>User::where('id' ,$rate->user_id)->first()->full_name,
                'user_avatar'=>$rate->user->image_url,
                'create_at'=>date_format( $rate->created_at, 'Y/m/d'),
            ];

        $this->data['data'] =  $data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
}
