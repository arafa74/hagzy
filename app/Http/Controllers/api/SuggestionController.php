<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\PARENT_API;
use App\Http\Requests\SuggestionRequest;
use App\Suggestion;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Validator;
class SuggestionController extends PARENT_API
{
    public function store(SuggestionRequest $request){
//        $user_data = JWTAuth::parseToken()->toUser();
//        $user = User::where('id' , $user_data->id)->first();

        $suggestion = Suggestion::create([
//            'user_id' =>$user->id,
            'name' =>$request->name,
            'lat' =>$request->lat,
            'lng' =>$request->lng,
            'address' =>$request->address,
            'mobile' =>$request->mobile,
            'bref' =>$request->bref,
            ]);
        $this->data['data'] =  $suggestion;
        $this->data['status'] = "ok";
        $this->data['message'] = " added successfully";
        return response()->json($this->data, 200);

    }

    public function show(){
        $suggestions = Suggestion::orderBy('id' , 'DESC')->get();
        $suggestions_data = [];
        foreach ($suggestions as $suggestion){
            $suggestions_data [] = [
                'name' =>$suggestion->name,
                'lat' =>$suggestion->lat,
                'lng' =>$suggestion->lng,
                'address' =>$suggestion->address,
                'mobile' =>$suggestion->mobile,
                'bref' =>$suggestion->bref,
            ];
        }
        $this->data['data'] =  $suggestions_data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);

    }
}
