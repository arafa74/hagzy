<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\PARENT_API;
use App\Http\Requests\OfferDelete;
use App\Http\Requests\OfferUpdate;
use App\Http\Requests\OfferRequest;
use App\Http\Resources\offerResource;
use App\Offer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Controllers\Controller;

class OfferController extends PARENT_API
{
    public function store(OfferRequest $request)
    {
        $user = User::find($request->user()->id);
        $data = [
            'user_id'=>$user->id,
            'description'=>$request->description,
            'name'=>$request->name,
            'price'=>$request->price,
            'discount'=>$request->discount,
            'personNum'=>$request->personNum,
            'from'=>$request->from,
            'to'=>$request->to,
            'image' =>IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/offers'),
        ];
        $offer = Offer::create($data);

        $this->data['data'] = new offerResource($offer);
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.OfferAdded');
        return response()->json($this->data, 200);
    }

    public function offers(Request $request)
    {
        $user = User::find($request->user()->id);
        $offers = Offer::where('user_id' , $user->id)->get();

        $this->data['data'] = offerResource::collection($offers);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }


    public function OfferUpdate(OfferUpdate $request){
        $offer = Offer::find($request->offer_id);
        if (!$offer){
            $this->data['data']=null;
            $this->data['status'] = "fails";
            $this->data['message'] = trans('dash.not_found');
            return response()->json($this->data, 405);
        }
        if ($request->description){
            $offer->description = $request->description;
        }
        if ($request->discount){
            $offer->discount = $request->discount;
        }

        if ($request->name){
            $offer->name = $request->name;
        }
        if ($request->price){
            $offer->price = $request->price;
        }

        if ($request->from){
            $offer->from = $request->from;
        }

        if ($request->to){
            $offer->to = $request->to;
        }
        if ($request->image){
            $offer->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/offers');
        }
        $offer->update();
        $this->data['data'] = new offerResource($offer);
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.DishUpdated');
        return response()->json($this->data, 200);
    }


    public function Offer(OfferDelete $request){
        $offer = Offer::find($request->offer_id);
        if (!$offer){
            $this->data['data']=null;
            $this->data['status'] = "fails";
            $this->data['message'] = trans('dash.not_found');
            return response()->json($this->data, 405);
        }
        
        $this->data['data'] = new offerResource($offer);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function OfferDelete(OfferDelete $request){
        $offer = Offer::where('id' , $request['offer_id'])->first()->delete();

        $this->data['data'] =  "";
        $this->data['status'] = "ok";
        $this->data['message'] =trans( 'app.messages.OfferDeleted');
        return response()->json($this->data, 200);
    }
}
