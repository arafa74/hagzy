<?php

namespace App\Http\Controllers\api;

use App\Like;
use App\Rate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use URL;
class SearchController extends Controller
{
    
    public function filter(Request $request)
    {
        $options_arr = [];
        $categories_arr = [];
        if($request->options){
            $options_arr = array_column(json_decode($request->options), 'id');
        }
        if($request->categories){
            $categories_arr = array_column(json_decode($request->categories), 'id');
        }

        $restaurants = User::where('type', 'restaurant');
            if (count($options_arr) > 0) {
                $restaurants = $restaurants->whereHas('option', function ($query) use ($options_arr) {
                    $query->whereIn('options.id', $options_arr)
                        ->groupBy('user_id')
                        ->havingRaw('COUNT(DISTINCT option_id) = ' . count($options_arr));
                });
            }
            if (count($categories_arr) > 0) {
                $restaurants = $restaurants->whereHas('category', function ($query) use ($categories_arr) {
                    $query->whereIn('categories.id', $categories_arr)
                        ->groupBy('user_id')
                        ->havingRaw('COUNT(DISTINCT category_id) = ' . count($categories_arr));
                });
            }
            
            if ($request->city_id) {
                $restaurants = $restaurants->where('city_id',$request->city_id);
            }
            
            if ($request->lat && $request->lng){
                $restaurants = $restaurants->nearest($request->lat, $request->lng, 500);
            }
            $restaurants = $restaurants->get();

        $restaurants_data = [];
        foreach ($restaurants as $restaurant) {
            $rate_avg = Rate::where('restaurant_id', $restaurant->id)->avg('rate');

            $is_like = 0;
            if ($request->headers->has('Authorization')) {
                $user_data = JWTAuth::parseToken()->toUser();
                $user = User::where('id', $user_data->id)->first();
                if (Like::where(['user_id' => $user->id, 'restaurant_id' => $restaurant->id])->exists()) {
                    $is_like = 1;
                }
            }

            $restaurants_data[] = [
                'id' => $restaurant->id,

                'ar_name'=>$restaurant->first_name?$restaurant->first_name:"",
                'en_name'=>$restaurant->last_name?$restaurant->last_name:"",

                // 'name' => $restaurant->full_name,
                'extras' => $restaurant->extras ? $restaurant->extras : " ",
                'image' => $restaurant->image_url,
                'is_like' => $is_like,
                'rate_avg' => (int)$rate_avg,
            ];
        }

        $this->data['data'] = $restaurants_data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
    
    public function filter_ios(Request $request)
    {
        $options_arr = [];
        $categories_arr = [];
        if($request->options){
            $options_arr = $request->options;
        }
        if($request->categories){
            $categories_arr = $request->categories;
        }

        $restaurants = User::where('type', 'restaurant');
            if (count($options_arr) > 0) {
                $restaurants = $restaurants->whereHas('option', function ($query) use ($options_arr) {
                    $query->whereIn('options.id', $options_arr)
                        ->groupBy('user_id')
                        ->havingRaw('COUNT(DISTINCT option_id) = ' . count($options_arr));
                });
            }
            if (count($categories_arr) > 0) {
                $restaurants = $restaurants->whereHas('category', function ($query) use ($categories_arr) {
                    $query->whereIn('categories.id', $categories_arr)
                        ->groupBy('user_id')
                        ->havingRaw('COUNT(DISTINCT category_id) = ' . count($categories_arr));
                });
            }
            if ($request->lat && $request->lng){
                $restaurants = $restaurants->nearest($request->lat, $request->lng, 500);
            }
            $restaurants = $restaurants->get();

        $restaurants_data = [];
        foreach ($restaurants as $restaurant) {
            $rate_avg = Rate::where('restaurant_id', $restaurant->id)->avg('rate');

            $is_like = 0;
            if ($request->headers->has('Authorization')) {
                $user_data = JWTAuth::parseToken()->toUser();
                $user = User::where('id', $user_data->id)->first();
                if (Like::where(['user_id' => $user->id, 'restaurant_id' => $restaurant->id])->exists()) {
                    $is_like = 1;
                }
            }

            $restaurants_data[] = [
                'id' => $restaurant->id,
                'name' => $restaurant->full_name,
                'extras' => $restaurant->extras ? $restaurant->extras : " ",
                'image' => $restaurant->image_url,
                'is_like' => $is_like,
                'rate_avg' => (int)$rate_avg,
            ];
        }

        $this->data['data'] = $restaurants_data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
    
    public function search(Request $request){

        $restaurants = User::where('full_name','like','%'.$request['key_word'].'%')
            ->where('type' ,'restaurant')
            ->orderBy('created_at','desc')->get();
        $restaurants_data = [];
        foreach ($restaurants as $restaurant){
            $rate_avg = Rate::where('restaurant_id' , $restaurant->id)->avg('rate');

            $is_like = 0;
            if ($request->headers->has('Authorization')){
                $user_data = JWTAuth::parseToken()->toUser();
                $user = User::where( 'id' , $user_data->id )->first();
                if (Like::where(['user_id' => $user->id , 'restaurant_id' => $restaurant->id])->exists()){
                    $is_like = 1;
                }
            }

            $restaurants_data[] =[
                'id'=>$restaurant->id,
                'name'=>$restaurant->full_name,
                'extras'=>$restaurant->extras?$restaurant->extras : " ",
                'image' => $restaurant->image_url,
                'is_like'=>$is_like,
                'rate_avg'=>(int) $rate_avg,
            ];
        }

        $this->data['data']     =  $restaurants_data;
        $this->data['status']   =  "ok";
        $this->data['message']  =  "";
        return response()->json($this->data, 200);
    }
}
