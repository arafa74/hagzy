<?php

namespace App\Http\Controllers\api;

use App\Ad;
use App\Http\Controllers\PARENT_API;
use App\Http\Controllers\FCM_CONTROLLER;
use App\Http\Requests\api\AdDetails;
use App\Http\Resources\AdResource;
use App\Like;
use App\Rate;
use App\User;
use App\AdImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use URL;
class FavController extends PARENT_API
{
    public function Like(AdDetails $request)
    {
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::find($user_data->id);
        if(Like::where(['restaurant_id' => $request->restaurant_id ,'user_id' => $user->id ])->exists()){
            Like::where(['restaurant_id' => $request->restaurant_id ,'user_id' => $user->id ])->delete();
            $this->data['data'] = "";
            $this->data['status'] = "ok";
            $this->data['message'] = trans('auth.Dislike_restaurant');
            return response()->json($this->data, 200);
        }else{
            $like = new Like();
            $like->restaurant_id = $request['restaurant_id'];
            $like->user_id = $user->id;
            $like->save();
            NotificationController::MK_NOTIFY($request['restaurant_id'] , 'like' ,$user->full_name .' ' .'تم الاعجاب بالمطعم الخاص بك بواسطه ' ,'your restaurant was liked by' . ' '.$user->full_name );
            $fcm_data['title'] = SETTING_VALUE('APP_NAME_AR');
            $fcm_data['body'] = $user->full_name .' ' .'تم الاعجاب بالمطعم الخاص بك بواسطه ';
            $fcm_data['restaurant_id'] = $like->restaurant_id;
            $fcm_data['key'] = "like";
            FCM_CONTROLLER::SEND_SINGLE_NOTIFICATION($like->restaurant_id , SETTING_VALUE('APP_NAME_AR') ,$user->full_name .' ' .'تم الاعجاب بالمطعم الخاص بك بواسطه ' , $fcm_data);

            $this->data['data'] = "";
            $this->data['status'] = "ok";
            $this->data['message'] = trans('auth.like_restaurant');
            return response()->json($this->data, 200);
        }
    }

    public function getMyLikes()
    {

        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::find($user_data->id);
        $user = User::find($user->id);
        if(!$user){
            $msg='User cannot be found';
            $this->data['data'] =  "";
            $this->data['status'] = "ok";
            $this->data['message'] = $msg;
            return response()->json($this->data);
        }
        $likes = $user->like()->get();
        $ads_data =[];
        foreach ($likes as $like){
            $rate_avg = Rate::where('restaurant_id' , $like->restaurant->id)->avg('rate');
                $ads_data [] = [
                    'id'=>$like->id,
                    'ar_name'=>$like->restaurant->first_name?$like->restaurant->first_name:"",
                    'en_name'=>$like->restaurant->last_name?$like->restaurant->last_name:"",
                    'restaurant_id'=>$like->restaurant->id,
                    'extras'=>$like->restaurant->extras?$like->restaurant->extras : " ",
                    'rate_avg'=>(int) $rate_avg,
                    'image'=> $like->restaurant->image_url,
                ];
            }
        $this->data['data'] =  $ads_data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
}
