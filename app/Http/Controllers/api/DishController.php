<?php

namespace App\Http\Controllers\api;

use App\Dish;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\DishRequest;
use App\Http\Requests\DishDelete;
use App\Http\Requests\DishUpdate;
use App\Http\Resources\DishResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class DishController extends PARENT_API
{
    public function store(DishRequest $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where('id' , $user_data->id)->first();
        $dish = Dish::create([
            'user_id' =>$user->id,
            'category_id' =>$request->category_id,
            'name' =>$request->name,
            'description' =>$request->description,
            'price' =>$request->price,
            'image' =>IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/dishes'),
        ]);
        $this->data['data'] =  $dish;
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.DishAdded');
        return response()->json($this->data, 200);
    }

    public function DishUpdate(DishUpdate $request){
        $dish = Dish::find($request->dish_id);
        if (!$dish){
            $this->data['data']=null;
            $this->data['status'] = "fails";
            $this->data['message'] = trans('dash.not_found');
            return response()->json($this->data, 405);
        }
        if ($request->name){
            $dish->name = $request->name;
        }
        if ($request->description){
            $dish->description = $request->description;
        }
        if ($request->price){
            $dish->price = $request->price;
        }
        if ($request->category_id){
            $dish->category_id = $request->category_id;
        }
        if ($request->image){
            $dish->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/dishes');
        }
        $dish->update();

        $this->data['data'] = $dish;
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.DishUpdated');
        return response()->json($this->data, 200);
    }

    public function DishDelete(DishDelete $request){
        $dish = Dish::where('id' , $request['dish_id'])->first()->delete();

        $this->data['data'] =  "";
        $this->data['status'] = "ok";
        $this->data['message'] =trans( 'app.messages.DishDeleted');
        return response()->json($this->data, 200);
    }

    public function Dish(DishDelete $request){
        $dish = Dish::find($request->dish_id);
        if (!$dish){
            $this->data['data']=null;
            $this->data['status'] = "fails";
            $this->data['message'] = trans('dash.not_found');
            return response()->json($this->data, 405);
        }
        
        $this->data['data'] = new DishResource($dish);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
}
