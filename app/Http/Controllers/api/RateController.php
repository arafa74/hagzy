<?php

namespace App\Http\Controllers\api;

use App\AppRate;
use App\Experience;
use App\Http\Requests\api\RateRequest;
use App\Http\Requests\RestaurantRateRequest;
use App\Http\Resources\RateResource;
use App\Like;
use App\Rate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
class RateController extends Controller
{
    public function AppRate(RateRequest $request)
    {
        $user = User::where('id', $request->user()->id)->first();

        $rate = AppRate::create([
            'user_id' =>         $user->id,
            'comment' =>           $request->comment,
            'rate' =>            $request->rate,
        ]);
        $this->data['data'] =  new RateResource($rate);
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.rate');
        return response()->json($this->data, 200);
    }

    public function RestaurantRate(RestaurantRateRequest $request)
    {
        $user = User::where('id', $request->user()->id)->first();

        $rate = Rate::create([
            'user_id' =>         $user->id,
            'restaurant_id' =>   $request->restaurant_id,
            'comment' =>         $request->comment,
            'rate' =>            $request->rate,
        ]);
        $this->data['data'] =  new RateResource($rate);
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.rate');
        return response()->json($this->data, 200);
    }
    
     public function MyRates(Request $request){
        $user = User::where('id', $request->user()->id)->first();
        
        $rates = Rate::where( 'user_id' , $user->id )->get();
        $data = [];
        foreach ($rates as $rate)
            $data []= [
                'id'=>$rate->id,
                'comment'=>$rate->comment,
                'rate'=>$rate->rate,
                'name'=>User::where('id' ,$rate->restaurant_id)->first()->full_name,
                'user_avatar'=>User::where('id', $rate->restaurant_id)->first()->image_url,
                'create_at'=>date_format( $rate->created_at, 'Y/m/d'),
            ];

        $this->data['data'] =  $data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function MyRestaurantRates(Request $request){
        $user = User::where('id', $request->user()->id)->first();
        
        $rates = Rate::where( 'restaurant_id' , $user->id )->get();
        $data = [];
        foreach ($rates as $rate)
            $data []= [
                'id'=>$rate->id,
                'comment'=>$rate->comment,
                'rate'=>$rate->rate,
                'name'=>User::where('id' ,$rate->user_id)->first()->full_name,
                'user_avatar'=>User::where('id', $rate->user_id)->first()->image_url,
                'create_at'=>date_format( $rate->created_at, 'Y/m/d'),
            ];

        $this->data['data'] =  $data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
    
    public function experiences_level(Request $request){

        $experiences = Experience::all();
        $data = [];
        $id=0;
        if ($request->header('Authorization')){
            $user_data = JWTAuth::parseToken()->toUser();
            $user = User::where( 'id' , $user_data->id )->first();
            $likes_count = Like::where('user_id' , $user->id)->count();
            $rates_count = Rate::where('user_id' , $user->id)->count();
            $reservations_count = \App\Reservation::where('user_id' , $user->id)->count();

            foreach ($experiences as $experience){
                if ($likes_count >= $experience->likes && $rates_count >= $experience->rates && $reservations_count >= $experience->reservations){
                    $id = $experience->id;
                }
            }

        }

        foreach ($experiences as $experience){
        $name = 'name_'.app()->getLocale();
        $data []= [
            'id'=>$experience->id,
            'name'=>$experience->$name,
            'color'=>$experience->color,
            'points'=>$experience->points,
            'likes'=>$experience->likes,
            'rates'=>$experience->rates,
            'reservations'=>$experience->reservations,
            'my_level'=>$experience->id==$id?1:0,
        ];

    }

        $this->data['data'] =  $data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
}
