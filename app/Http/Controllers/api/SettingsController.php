<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\PARENT_API;
use App\Model\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends PARENT_API
{
    public function Privacy()
    {
        $data = "";
        if ($this->lang == 'ar'){
            $data = Setting::where('key' ,'PRIVACY_POLICY_AR')->first()->value;
        }elseif ($this->lang == 'en'){
            $data = Setting::where('key' ,'PRIVACY_POLICY_EN')->first()->value;
        }
        $this->data['data'] =  $data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function aboutApp()
    {
        $data = "";
        if ($this->lang == 'ar'){
            $data = Setting::where('key' ,'ABOUT_AR')->first()->value;
        }elseif ($this->lang == 'en'){
            $data = Setting::where('key' ,'ABOUT_EN')->first()->value;
        }
        $this->data['data'] =  $data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function Terms()
    {
        $data = "";
        if ($this->lang == 'ar'){
            $data = Setting::where('key' ,'TERMS_AR')->first()->value;
        }elseif ($this->lang == 'en'){
            $data = Setting::where('key' ,'TERMS_EN')->first()->value;
        }
        $this->data['data'] =  $data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
    
    public function ContactUs()
    {
       
        $contacts_data [] = [
            'facebook'=>    Setting::where('key' , 'FACEBOOK_URL' )->first()->value  ,
            'twitter'=>     Setting::where('key' , 'TWITTER_URL' )->first()->value  ,
            'email'=>       Setting::where('key' , 'FORMAL_EMAIL' )->first()->value  ,
            'snapcaht'=>    Setting::where('key' , 'SNAPCHAT_URL' )->first()->value  ,
            'mobile'=>     Setting::where('key' , 'MOBILE' )->first()->value  ,
            'mobile'=>     Setting::where('key' , 'MOBILE' )->first()->value  ,
            'instagram'=>     Setting::where('key' , 'INSTAGRAM_URL' )->first()->value  ,
        ];
        
        $this->data['data'] =  $contacts_data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
}
