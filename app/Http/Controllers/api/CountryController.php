<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\PARENT_API;
use App\Model\Country;
use App\Http\Resources\CountryCollection;

class CountryController extends PARENT_API
{
    public function all(){
        $countries=Country::all();
            $this->data['data'] = new CountryCollection($countries);
            $this->data['status'] = "ok";
            $this->data['message'] = '';
            return response()->json($this->data, 405);
    }
}
