<?php

namespace App\Http\Controllers\api;

use App\Bank;
use App\Commission;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\AddCommission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use URL;
class BankController extends PARENT_API
{
    public function getBanks(){
        $banks = Bank::all();
        $banks_data= [];
        foreach ($banks as $bank){
            $banks_data []= [
              'id'=>$bank->id,
              'iban_number'=>$bank->id,
              'account_number'=>$bank->id,
              'image'=>URL::to('storage/app/banks/50x50') . '/'.$bank->image,
            ];
        }
        $this->data['data'] =  $banks_data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }


    public function commission(AddCommission $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::find($user_data->id);

        IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/commissions');

        $commission = Commission::create([
            'user_id'=>$user->id,
            'bank_id'=>$request->bank_id,
            'account_number'=>$request->account_number,
            'account_name'=>$request->account_name,
            'amount'=>$request->amount,
            'image'=>IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/commissions'),
            'message'=>$request->message,
        ]);

        $this->data['data'] =  new \App\Http\Resources\api\Commission($commission);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
}
