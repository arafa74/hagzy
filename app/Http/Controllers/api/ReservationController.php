<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\PARENT_API;
use App\Http\Controllers\FCM_CONTROLLER;
use App\Http\Requests\AcceptReservation;
use App\Http\Requests\Reservation;
use App\Http\Requests\ReservationDetails;
use App\Http\Resources\ReservationsList;
use App\Http\Resources\ReservationsDetails;
use App\User;
use App\Rate;
use App\Model\Token;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use DB;
use Carbon\Carbon;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
class ReservationController extends PARENT_API
{
    private function doBeforeReservation($user) {
        $reservations = \App\Reservation::where('user_id' , $user->id)->whereDate('created_at', Carbon::today())
              ->count();
        if ($reservations >= 3 ){
            $data['data'] =  "";
            $data['status'] = "ok";
            $data['message'] = trans('app.messages.MaxReservationsNum');
            return $data;
        }
        return false;
    }
    
    private function doBeforeReservation2($user) {
        $reservations = \App\Reservation::where('user_id' , $user->id)->whereTime('created_at', '>', now()->subMinutes(30))
              ->groupBy(\DB::raw('HOUR(created_at)'))
              ->count();
        if ($reservations >= 1 ){
            $data['data'] =  "";
            $data['status'] = "ok";
            $data['message'] = trans('app.messages.LastReservations');
            return $data;
        }
        return false;
        
        
    }
    
    public function store(Reservation $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where('id' , $user_data->id)->first();
        $returned_data = $this->doBeforeReservation($user);
        if($returned_data){
            return response()->json($returned_data, 401);
        }
        
        // $returned_data2 = $this->doBeforeReservation2($user);
        // if($returned_data2){
        //     return response()->json($returned_data2, 401);
        // }
        
        $reservation = \App\Reservation::create([
            'user_id' =>$user->id,
            'clientsNum' =>$request->clientsNum,
            'childrenNum' =>$request->childrenNum,
            'reservation_day' =>$request->reservation_day,
            'reservation_time' =>$request->reservation_time,
            'restaurant_id' =>$request->restaurant_id,
            'name' =>$request->name,
            'mobile' =>$request->mobile,
            'email' =>$request->email,
            'message' =>$request->message,
            'status' =>"hanging",
            'code' =>rand('1111' , '9999'),
        ]);
        NotificationController::MK_NOTIFY($request->restaurant_id , 'reservarion' ,$user->full_name .' '.'لديك حجز جديد بواسطه' , 'new reservation by'.' '.$user->full_name);
        $fcm_data['title'] = SETTING_VALUE('APP_NAME_AR');
        $fcm_data['body'] = $user->full_name .' '.' لديك حجز جديد من';
        $fcm_data['restaurant_id'] = $reservation->restaurant_id;
        $fcm_data['key'] = "reservation";
        FCM_CONTROLLER::SEND_SINGLE_NOTIFICATION($reservation->restaurant_id ,SETTING_VALUE('APP_NAME_AR'),$user->full_name .' '.'لديك حجز جديد من' ,$fcm_data);
        
        $this->data['data'] =  new \App\Http\Resources\Reservation($reservation);
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.SuccessfullyReserved');
        return response()->json($this->data, 200);

    }

    public function reservations(){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where( 'id' , $user_data->id )->first();
        $reservations = \App\Reservation::where(['restaurant_id' => $user->id , 'status' => 'hanging' ])->get();
        $this->data['data'] =  ReservationsList::collection($reservations);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);

    }

    public function reservationDetails(ReservationDetails $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where( 'id' , $user_data->id )->first();
        $reservation = \App\Reservation::find($request->reservation_id);
        $this->data['data'] =  new ReservationsDetails($reservation);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);

    }

    public function cancel_reservation(AcceptReservation $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where('id', $user_data->id)->first();

        $reservation = \App\Reservation::where('id' , $request->reservation_id)->first();
        $reservation->status = "cancel_from_client";
        $reservation->cancel_reason = $request->reason;
        $reservation->update();
        $this->data['data'] =  "";
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.CanceledFromClient');
        return response()->json($this->data, 200);
    }
    
    public function accept(AcceptReservation $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where('id', $user_data->id)->first();

        $reservation = \App\Reservation::where('id' , $request->reservation_id)->first();
        $reservation->status = "accepted";
        $reservation->update();
        NotificationController::MK_NOTIFY($reservation->user->id , 'accept' ,$user->full_name .' '.' تم قبول الحجز الخاص بك بواسطه' , 'your reservation accepted by'.' '.$user->full_name);
        $fcm_data['title'] = SETTING_VALUE('APP_NAME_AR');
        $fcm_data['body'] = $user->full_name .' '.' تم قبول الحجز الخاص بك بواسطه';
        $fcm_data['restaurant_id'] = $reservation->restaurant_id;
        $fcm_data['key'] = "accept";
        FCM_CONTROLLER::SEND_SINGLE_NOTIFICATION($reservation->user_id ,SETTING_VALUE('APP_NAME_AR'),$user->full_name .' '.' تم قبول الحجز الخاص بك بواسطه' ,$fcm_data);


        $this->data['data'] =  "";
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.ReservationAccepted');
        return response()->json($this->data, 200);
    }

    public function finishByProvider(AcceptReservation $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where('id', $user_data->id)->first();

        $reservation = \App\Reservation::where('id' , $request->reservation_id)->first();
        $reservation->status = "finish_from_provider";
        $reservation->update();
        NotificationController::MK_NOTIFY($reservation->user->id , 'finish' ,$user->full_name .' '.' تم انهاء الحجز الخاص بك بواسطه' , 'your reservation finished by'.' '.$user->full_name);
        $fcm_data['title'] = SETTING_VALUE('APP_NAME_AR');
        $fcm_data['body'] = $user->full_name .' '.' تم انهاء الحجز الخاص بك بواسطه';
        $fcm_data['restaurant_id'] = $reservation->restaurant_id;
        $fcm_data['key'] = "accept";
        FCM_CONTROLLER::SEND_SINGLE_NOTIFICATION($reservation->user_id ,SETTING_VALUE('APP_NAME_AR'),$user->full_name .' '.' تم انهاء الحجز الخاص بك بواسطه' ,$fcm_data);


        $this->data['data'] =  "";
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.ReservationFinished');
        return response()->json($this->data, 200);
    }

    public function reject(AcceptReservation $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where('id', $user_data->id)->first();

        $reservation = \App\Reservation::where('id' , $request->reservation_id)->first();
        $reservation->status = "canceled";
        $reservation->update();
        NotificationController::MK_NOTIFY($reservation->user->id , 'reject' ,$user->full_name .' '.' تم رفض الحجز الخاص بك بواسطه' , 'your reservation rejected by'.' '.$user->full_name);
        $fcm_data['title'] = SETTING_VALUE('APP_NAME_AR');
        $fcm_data['body'] = $user->full_name .' '.' تم رفض الحجز الخاص بك بواسطه';
        $fcm_data['restaurant_id'] = $reservation->restaurant_id;
        $fcm_data['key'] = "reject";
        FCM_CONTROLLER::SEND_SINGLE_NOTIFICATION($reservation->user->id ,SETTING_VALUE('APP_NAME_AR'),$user->full_name .' '.' تم رفض الحجز الخاص بك بواسطه' ,$fcm_data);
        

        $this->data['data'] =  "";
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.ReservationRejected');
        return response()->json($this->data, 200);
    }

    public function RejectedReservations(){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where( 'id' , $user_data->id )->first();
        $reservations = \App\Reservation::where(['restaurant_id' => $user->id , 'status' => 'canceled' ])->get();
        $this->data['data'] =  ReservationsList::collection($reservations);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function AcceptedReservations(){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where( 'id' , $user_data->id )->first();
        $reservations = \App\Reservation::where(['restaurant_id' => $user->id])->where('status' , 'accepted')->get();
        $this->data['data'] =  ReservationsList::collection($reservations);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);

    }

    public function finishedReservations(){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where( 'id' , $user_data->id )->first();
        $reservations = \App\Reservation::where(['restaurant_id' => $user->id])->where('status' , 'finish_from_provider')->get();
        $this->data['data'] =  ReservationsList::collection($reservations);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);

    }

    // ===========CLIENT RESERVATIONS==========

    public function clientReservations(){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where( 'id' , $user_data->id )->first();
        $reservations = \App\Reservation::where(['user_id' => $user->id , 'status' => 'hanging' ])->get();        
        $this->data['data'] =  ReservationsList::collection($reservations);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);

    }

    public function clientRejectedReservations(){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where( 'id' , $user_data->id )->first();
        $reservations = \App\Reservation::where(['user_id' => $user->id , 'status' => 'canceled' ])->get();
        $this->data['data'] =  ReservationsList::collection($reservations);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function clientAcceptedReservations(){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where( 'id' , $user_data->id )->first();
        $reservations = \App\Reservation::where(['user_id' => $user->id])->where('status' , 'accepted')->get();
        $this->data['data'] =  ReservationsList::collection($reservations);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);

    }

    public function clientFinishedReservations(){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where( 'id' , $user_data->id )->first();
        $reservations = \App\Reservation::where(['user_id' => $user->id])->where('status' , 'finish_from_provider')->get();
        $this->data['data'] =  ReservationsList::collection($reservations);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);

    }
}
