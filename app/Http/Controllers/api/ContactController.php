<?php

namespace App\Http\Controllers\api;

use App\Contact;
use App\Type;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends PARENT_API
{
    public  function contact_us(ContactUs $request){
        $data = [
            'type_id'=>$request->type_id ,
            'title'=>$request->title ,
            'details'=>$request->details
        ];
        $contact = Contact::create($data);
        $this->data['data'] = new \App\Http\Resources\ContactUs($contact);
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.contactSent');
        return response()->json($this->data, 200);
    }


    public function types(){
        $name='name_'.app()->getLocale();
        $types = Type::all();
        $types_data= [];
        foreach ($types as $type){
            $types_data []= [
              'id'=>$type->id,
             'type'=>$type->$name
            ];
        }
        $this->data['data'] =  $types_data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
}
