<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\SendMessageRequest;
use App\Http\Resources\ChatResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Http\Requests\Api\Chat\SendMessageRequest;
use App\Room;
use App\Message;
use App\User;
use DB;

class ChatController extends PARENT_API
{

    public function all(Request $request)
    {
        $user = User::find($request->user()->id);
        $conversations = Room::where(function ($query) use ($user) {
                $query->orWhere('sender_id', $user->id);
                $query->orWhere('receiver_id', $user->id);
            })->orderBy('updated_at', 'DESC')->get();
        $conversations_data =[];
            foreach ($conversations as $conversation) {
                $name='name_'.app()->getLocale();
                $conversations_data [] = [
                  'id'=>$conversation->id   ,
                  'sender_id'=>$conversation->sender_id   ,
                  'receiver_id'=>$conversation->receiver_id   ,
                  'city'=>$conversation->receiver->city->$name   ,
                  'receiver_name'=>$conversation->sender->full_name   ,
                  'last_message'=>$conversation->last_message
                ];
            }

        $this->data['data'] =  $conversations_data;
        $this->data['status'] = "ok";
        $this->data['message'] = trans('');
        return response()->json($this->data, 200);
    }

//    public function show(Request $request)
//    {
//
//        $room = Room::find($request->room_id);
//        if ($room == null){
//            $this->data['data'] =  " ";
//            $this->data['status'] = "false";
//            $this->data['message'] = trans('room_id required');
//            return response()->json($this->data, 401);
//        }
//        if (!User::find($request->room)){
//            $this->data['data'] =  " ";
//            $this->data['status'] = "false";
//            $this->data['message'] = trans('room_not_found .. try anther room_id');
//            return response()->json($this->data, 404);
//        }
//        try {
//            $messages = Message::where('room_id' , $room->id)->get();
//
//            $this->data['data'] =  $conversation != null ? ChatResource::collection($conversation->ChatMessages()->orderBy('created_at', 'DESC')->get()) : [];
//            $this->data['status'] = "ok";
//            $this->data['message'] = trans('auth.something_went_wrong_please_try_again');
//            return response()->json($this->data, 200);
//
//        } catch (Exception $e) {
//
//            $this->data['data'] =  " ";
//            $this->data['status'] = "ok";
//            $this->data['message'] = trans('auth.something_went_wrong_please_try_again');
//            return response()->json($this->data, 200);
//
//
////            return response()->json(['status' => 'false', 'message' => trans('auth.something_went_wrong_please_try_again'), 'data' => null], 401);
//        }
//    }
    public function show(Request $request, $receiver_id = null)
    {
        if ($receiver_id == null)
            return response()->json(['status' => 'false', 'message' => trans('app.receiver_id_required'), 'data' => null], 401);
        if (!User::find($receiver_id))
            return response()->json(['status' => 'false', 'message' => trans('app.user_not_found'), 'data' => null], 404);
        try {
            $user = User::find($request->user()->id);
            $receiver = User::find($receiver_id);
            $conversation = Room::where(function ($query) use ($user) {
                $query->orWhere('sender_id', $user->id);
                $query->orWhere('receiver_id', $user->id);
            })->where(function ($query) use ($receiver) {
                $query->orWhere('sender_id', $receiver->id);
                $query->orWhere('receiver_id', $receiver->id);
            })->first();
            return response()->json([
                'status' => 'true',
                'message' => '',
                'data' => $conversation != null ? ChatResource::collection($conversation->ChatMessages()->orderBy('created_at', 'DESC')->get()) : [],
            ], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'false', 'message' => trans('auth.something_went_wrong_please_try_again'), 'data' => null], 401);
        }
    }
    public function send(SendMessageRequest $request)
    {
        DB::beginTransaction();
        try {
            $sender = User::find($request->user()->id);
            $receiver = User::find($request->receiver_id);
            $conversation = Room::where(function ($query) use ($sender) {
                $query->orWhere('sender_id', $sender->id);
                $query->orWhere('receiver_id', $sender->id);
            })->where(function ($query) use ($receiver) {
                $query->orWhere('sender_id', $receiver->id);
                $query->orWhere('receiver_id', $receiver->id);
            })->first();
            if ($conversation) {
                $conversation->update([
                    'sender_id' => $request->user()->id,
                    'receiver_id' => $request->receiver_id,
                    'message_type' => $request->type,
                    'last_message' => ($request->type == 'image') ? 'image sent' : $request->message,
                ]);
            } else {
                $conversation = Room::create([
                    'sender_id' => $request->user()->id,
                    'receiver_id' => $request->receiver_id,
                    'type' => $request->type,

                    'ad_id' => $request->ad_id,
                    'last_message' => $request->message,
                ]);

            }
            $chat = Message::create([
                'sender_id' => $request->user()->id,
                'receiver_id' => $request->receiver_id,
                'room_id' => $conversation->id,
                'type' => $request->type,
                'message' => $request->message,
            ]);
            // =========================== Notification ===========================
            // $title = trans('app.fcm.new_chat_message');

            // $fcm_data = [];
            // $fcm_data['title'] = $title;

            // if ($request->message_type == 'image') {
            //     $body_ar = 'قام ' . $request->user()->username . ' بإرسال صورة إليك.';
            //     $body_en = $request->user()->username . ' sent an image for you';
            // } else if ($request->message_type == 'voice_message') {
            //     $body_ar = 'قام ' . $request->user()->username . ' بإرسال ملف صوتي إليك.';
            //     $body_en = $request->user()->username . ' sent a voice message for you';
            // } else if ($request->message_type == 'location') {
            //     $body_ar = 'قام ' . $request->user()->username . ' بإرسال إحداثيات إليك.';
            //     $body_en = $request->user()->username . ' sent a location for you';
            // } else {
            //     $body_ar = 'قام ' . $request->user()->username . ' بإرسال رسالة إليك.';
            //     $body_en = $request->user()->username . ' sent a message for you';
            // }
            // $body = app()->getLocale() == 'ar' ? $body_ar : $body_en;

            // $fcm_data['key'] = 'new_chat_message';
            // $fcm_data['body'] = $body;
            // $fcm_data['msg_type'] = $request->message_type;
            // $fcm_data['sender_id'] = $request->user()->id;
            // $fcm_data['msg_sender'] = $request->user()->username;
            // $fcm_data['message'] = $chat->message_value;
            // $fcm_data['sender_logo'] = $request->user()->profile_image;
            // $fcm_data['order_id'] = $order->id;
            // $fcm_data['time'] = $chat->created_at->diffforhumans();

            // if (Device::where('user_id', $conversation->receiver_id)->exists()) {
            //     NotificationController::SEND_SINGLE_STATIC_NOTIFICATION($conversation->receiver_id, $title, $body, $fcm_data, (60 * 20));
            // }
            // =========================== Notification ===========================
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $this->data['data'] =  "";
            $this->data['status'] = "ok";
            $this->data['message'] = trans('app.messages.something_went_wrong_please_try_again');
            return response()->json($this->data, 401);
        }
        $this->data['data'] =  new ChatResource($chat);
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.sent_successfully');
        return response()->json($this->data, 200);
    }
}
