<?php

namespace App\Http\Controllers\api;

use App\Ad;
use App\AdImage;
use App\Category;
use App\Http\Requests\api\AdDetails;
use App\Http\Requests\api\AddNewAd;
use App\Http\Requests\api\RateRequest;
use App\Http\Resources\AdResource;
use App\Http\Resources\RateResource;
use App\Rate;
use App\Service;
use App\User;
use URL;
use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Controllers\PARENT_API;

use JWTAuth;
class AdController extends PARENT_API
{
    public function categories_with_services()
    {
        $name = 'name_'.app()->getLocale();
        $categories = Category::all();
        $categories_data=[];
        foreach ($categories as $category) {
            $services = Service::where('category_id' , $category->id)->get();
            $services_data = [];
            foreach ($services as $service) {
                $services_data[]=[
                    'id'=>$service->id,
                    'name'=>$service->$name
                ];
            }
//            if(count($services) > 0){
            $categories_data[]=[
                    'id'=>$category->id,
                    'name'=>$category->$name,
                    'services'=>$services_data,
                ];
//            }

        }

        $this->data['data'] = $categories_data;
        $this->data['status'] = "ok";
        $this->data['message'] = '';
        return response()->json($this->data, 200);
    }

    public function store(AddNewAd $request){
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where('id' , $user_data->id)->first();
        $ad = Ad::create([
            'user_id' =>$user->id,
            'category_id' =>$request->category_id,
            'service_id' =>$request->service_id,
            'title' =>$request->title,
            'description' =>$request->description,
            'year_model' =>$request->year_model,
            'price' =>$request->price,
            'start_date' =>$request->start_date,
            'end_date' =>$request->end_date,
            'lat' =>$request->lat,
            'lng' =>$request->lng,
            'address' =>$request->address,
            'status' =>" ",
            ]);
        foreach ($request->images as $image){
            AdImage::create([
                'ad_id' => $ad->id,
                'image' => IMAGE_CONTROLLER::upload_single($image, 'storage/app/ad_images'),
                    ]
            );
        }

        $this->data['data'] =  [];
        $this->data['status'] = "ok";
        $this->data['message'] = "تم اضافه الاعلان بنجاح";
        return response()->json($this->data, 200);

    }

    public function MyAds(){
        $name = 'name_'.app()->getLocale();

        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::where('id' , $user_data->id)->first();

        $ads = Ad::where('user_id' , $user->id)->get();
        $ads_data =[];
        foreach ($ads as $ad){
            $ads_data [] = [
                'id'=>$ad->id,
                'price'=>$ad->price,
                'title'=>$ad->title,
                'city_name'=>$ad->user->city->$name,
                'year_model'=>$ad->year_model,
                'image'=> URL::to('storage/app/ad_images/org') . '/' .AdImage::where('ad_id' , $ad->id)->first()->image,
            ];
        }

        $this->data['data'] =  $ads_data;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function AdDetails(AdDetails $request){
        $ad = Ad::where('id' , $request['ad_id'])->first();


        $this->data['data'] =  new AdResource($ad);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function AdDelete(AdDetails $request){
        $ad = Ad::where('id' , $request['ad_id'])->first()->delete();

        $this->data['data'] =  "";
        $this->data['status'] = "ok";
        $this->data['message'] =trans( 'app.messages.AdDeleted');
        return response()->json($this->data, 200);
    }

    public function rate(RateRequest $request)
    {
        $user = User::where('id', $request->user()->id)->first();

        $rate = Rate::create([
            'user_id' =>         $user->id,
            'ad_id' =>           $request->ad_id,
            'rate' =>            $request->rate,
        ]);
        $this->data['data'] =  new RateResource($rate);
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.rate');
        return response()->json($this->data, 200);
    }
}
