<?php

namespace App\Http\Controllers\api;

use App\Bunch;
use App\Category;
use App\Http\Requests\api\ActiveRequest;
use App\Http\Requests\api\RegisterRestaurant;
use App\Http\Requests\api\RegisterRestaurantIos;
use App\Http\Resources\Restaurant;
use App\Model\City;
use App\Option;
use App\Payment;
use App\UserCategory;
use App\UserOption;
use Illuminate\Http\Request;
use App\Http\Controllers\IMAGE_CONTROLLER;
use App\Http\Controllers\PARENT_API;
use App\Http\Resources\SingleUser;
use App\User;
use App\Model\Token;
use App\Http\Requests\api\RegisterRequest;
use App\Http\Requests\api\LoginRequest;
use JWTAuth;
use Nexmo\Laravel\Facade\Nexmo;
use App\Http\Requests\SocialRegister;
use Validator;

class AuthController extends PARENT_API
{
    public function social(SocialRegister $request){
        $user = User::where('social_id' , $request->social_id)->first();
        if ($user){
            $this->data['data'] = new SingleUser($user);
            $this->data['status'] = "ok";
            $this->data['message'] = '';
            return response()->json($this->data, 200);
        }else{
            $user = User::where('email' , $request->email)->first();
            if (is_null($user)){
                $user=new User();
                $user->social_id=$request->social_id;
                $user->social_type=$request->social_type;
                $user->full_name=$request->name;
                $user->email=$request->email;
                $user->type='user';

                if ($request->image) {
                    $user->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/users');
                }

                if ($request->latitude) {
                    $user->latitude = $request->latitude;
                }

                if ($request->longitude) {
                    $user->longitude = $request->longitude;
                }

                $user->lang = "ar";
                $user->code = rand(100000, 999999);
                $user->save();
                $token = new Token();
                $token->user_id = $user->id;
                $token->fcm = $request->fcm_token;
                $token->device_type = $request->header('os');
                $token->jwt = JWTAuth::fromUser($user);
                $token->is_logged_in = 'true';
                $token->ip = $request->ip();
                $token->save();

            }else{
                $user->social_id = $request->social_id;
                $user->update();
            }
            $this->data['data'] = new SingleUser($user->fresh());
            $this->data['status'] = "ok";
            $this->data['message'] = '';
            return response()->json($this->data, 200);
        }
    }

    public function signUpUser(RegisterRequest $request){
        $user=new User();
        $user->mobile=$request->mobile;
        $user->full_name=$request->name;
        $user->email=$request->email;
        $user->type='user';

        $user->password = bcrypt($request->password);
        if ($request->image) {
            $user->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/users');
        }

        if ($request->latitude) {
            $user->latitude = $request->latitude;
        }

        if ($request->longitude) {
            $user->longitude = $request->longitude;
        }

        $user->lang = "ar";
        $user->code = rand(100000, 999999);
        $user->save();
//        SEND_SINGLE_SMS($request->mobile , 'your activation code is' . $user->code);
        $token = new Token();
        $token->user_id = $user->id;
        $token->fcm = $request->fcm_token;
        $token->device_type = $request->header('os');
        $token->jwt = JWTAuth::fromUser($user);
        $token->is_logged_in = 'true';
        $token->ip = $request->ip();
        $token->save();
        $this->data['data'] = new SingleUser($user);
        $this->data['status'] = "ok";
        $this->data['message'] = '';
        return response()->json($this->data, 200);

    }

    public function signUpRestaurant(RegisterRestaurant $request){

        $user=new User();
        $user->mobile=$request->mobile;
        $user->full_name=$request->ar_name;
        $user->first_name=$request->ar_name;
        $user->last_name=$request->en_name;
        $user->email=$request->email;
        $user->type='restaurant';
        $user->active='wait_admin_confirm';
        $user->password = bcrypt($request->password);

        if ($request->image) {
            $user->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/users');
        }

        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;
        $user->address = $request->address;
        $user->payment_id = $request->payment_id;
        $user->bunch_id = $request->bunch_id;
        $user->city_id = $request->city_id;


        $user->lang = "ar";
        $user->code = rand(100000, 999999);
        $user->save();
//        SEND_SINGLE_SMS($request->mobile , 'your activation code is' . $user->code);
        $token = new Token();
        $token->user_id = $user->id;
        $token->fcm = $request->fcm_token;
        $token->device_type = $request->header('os');
        $token->jwt = JWTAuth::fromUser($user);
        $token->is_logged_in = 'true';
        $token->ip = $request->ip();
        $token->save();

        $options_id = $request->options;
        if ($options_id){
            foreach ($options_id as $option_id){
                $userOption = UserOption::create([
                    'option_id' => $option_id,
                    'user_id' => $user->id,
                ]);
            }
        }
        if ($request->categories){
            $user->category()->attach($request->categories);
        }

        $this->data['data'] = trans("app.messages.wait_admin_to_active_account");
        $this->data['status'] = "ok";
        $this->data['message'] = '';
        return response()->json($this->data, 200);

    }
    
    public function signUpRestaurantIos(RegisterRestaurantIos $request){

        $user=new User();
        $user->mobile=$request->mobile;
        $user->full_name=$request->ar_name;
        $user->first_name=$request->ar_name;
        $user->last_name=$request->en_name;
        $user->email=$request->email;
        $user->type='restaurant';
        $user->active='wait_admin_confirm';
        $user->password = bcrypt($request->password);

        if ($request->image) {
            $user->image = IMAGE_CONTROLLER::upload_single($request->image, 'storage/app/users');
        }

        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;
        $user->address = $request->address;
        $user->payment_id = $request->payment_id;
        $user->bunch_id = $request->bunch_id;
        $user->city_id = $request->city_id;


        $user->lang = "ar";
        $user->code = rand(100000, 999999);
        $user->save();
//        SEND_SINGLE_SMS($request->mobile , 'your activation code is' . $user->code);
        $token = new Token();
        $token->user_id = $user->id;
        $token->fcm = $request->fcm_token;
        $token->device_type = $request->header('os');
        $token->jwt = JWTAuth::fromUser($user);
        $token->is_logged_in = 'true';
        $token->ip = $request->ip();
        $token->save();

        $options_id = $request->options;
        if ($options_id){
            foreach ($options_id as $option_id){
                $userOption = UserOption::create([
                    'option_id' => $option_id,
                    'user_id' => $user->id,
                ]);
            }
        }
        if ($request->categories){
            $user->category()->attach($request->categories);
        }

        $this->data['data'] = trans("app.messages.wait_admin_to_active_account");
        $this->data['status'] = "ok";
        $this->data['message'] = '';
        return response()->json($this->data, 200);

    }

    public function ActiveAccount(ActiveRequest $request){
        $user = User::where('code' , $request->code)->first();
        $user->active = 'active';
        $user->code = "";
        $user->update();
        $this->data['data'] = new SingleUser($user);
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }

    public function signin(LoginRequest $request){

        $perms['mobile'] = $request->mobile;
        $perms['password'] = $request->password;
        $user = User::where('mobile' , $request->mobile)->first();
        if (!$user) {
            $this->data['data'] = null;
            $this->data['status'] = "fails";
            $this->data['message'] = "user not found";
            return response()->json($this->data, 401);
        }
        
        elseif($user->active == 'wait_admin_confirm'){
            $this->data['data'] = null;
            $this->data['status'] = "fails";
            $this->data['message'] = "برجاء انتظار موافقه الاداره";
            return response()->json($this->data, 401);
        }else{
            if (!$token = JWTAuth::attempt($perms)) {
            $this->data['data'] = null;
            $this->data['status'] = "fails";
            $this->data['message'] = trans('auth.failed_login');
            return response()->json($this->data, 401);
            }
            $logged_user = auth()->user();
            if ($request->latitude && $request->longitude) {
                $logged_user->latitude = $request->latitude;
                $logged_user->longitude = $request->longitude;
            }
            $logged_user->update();
    
            $logged_user_token = $logged_user->token;
            $logged_user_token->jwt = $token;
            $logged_user_token->is_logged_in = "true";
    //        if ($request->fcm_token && $request->device_type) {
            $logged_user_token->fcm = $request->fcm_token;
            $logged_user_token->device_type = $request->header('os');
    //        }
            $logged_user_token->ip = $request->ip();
            $logged_user_token->update();
            if ($logged_user->type == 'user'){
                $this->data['data'] = new SingleUser($logged_user);
                $this->data['status'] = "ok";
                $this->data['message'] = "";
                return response()->json($this->data, 200);
            }else{
                $this->data['data'] = new Restaurant($logged_user);
                $this->data['status'] = "ok";
                $this->data['message'] = "";
                return response()->json($this->data, 200);
            }    
        }
        
        

    }

    public function categories()
    {
        $name = 'name_'.app()->getLocale();
        $categories = Category::all();
        $categories_data=[];
        foreach ($categories as $category) {
            $categories_data[]=[
                'id'=>$category->id,
                'name'=>$category->$name,
            ];
        }

        $this->data['data'] = $categories_data;
        $this->data['status'] = "ok";
        $this->data['message'] = '';
        return response()->json($this->data, 200);
    }

    public function options()
    {
        $name = 'name_'.app()->getLocale();
        $options = Option::all();
        $options_data=[];
        foreach ($options as $option) {
            $options_data[]=[
                'id'=>$option->id,
                'name'=>$option->$name,
            ];
        }

        $this->data['data'] = $options_data;
        $this->data['status'] = "ok";
        $this->data['message'] = '';
        return response()->json($this->data, 200);
    }

    public function bunches()
    {
        $name = 'name_'.app()->getLocale();
        $bunches = Bunch::all();
        $bunches_data=[];
        foreach ($bunches as $bunch) {
            $bunches_data[]=[
                'id'=>$bunch->id,
                'name'=>$bunch->$name,
                'price'=>$bunch->price?$bunch->price:0,
            ];
        }

        $this->data['data'] = $bunches_data;
        $this->data['status'] = "ok";
        $this->data['message'] = '';
        return response()->json($this->data, 200);
    }

    public function payments()
    {
        $name = 'name_'.app()->getLocale();
        $payments = Payment::all();
        $payments_data=[];
        foreach ($payments as $payment) {
            $payments_data[]=[
                'id'=>$payment->id,
                'name'=>$payment->$name,
            ];
        }
        $this->data['data'] = $payments_data;
        $this->data['status'] = "ok";
        $this->data['message'] = '';
        return response()->json($this->data, 200);
    }
    
    public function Logout(Request $request)
    {
        $user_data = JWTAuth::parseToken()->toUser();
        $user = User::find($user_data->id);
        if(!$user){
            $msg='User Not Found' ;
            $this->data['data'] = [];
            $this->data['status'] = "ok";
            $this->data['message'] = $msg;
            return response()->json($this->data, 200);
        }
        $tokens = Token::where('user_id' , $user->id)->get();
        foreach ($tokens as $token){
            $token->fcm =null;
            $token->update();    
        }

        $this->data['data'] = [];
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.messages.success_logout');
        return response()->json($this->data, 200);

    }
    
    public function postForgotPassword(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'mobile'=>'required'
        ]);
        if ($validator->passes()) {
            $user = User::where('mobile',$request['mobile'])->first();
            if(!$user){
                $this->data['data'] = "";
                $this->data['status'] = "validation_errors";
                $this->data['message'] = "user is not found";
                return response()->json($this->data);
            }
            $user->code = rand(000000, 999999);
//            SEND_SINGLE_SMS($request->mobile , 'your verification code is' . $user->code);
            $user->save();


            $this->data['data'] = "";
            $this->data['status'] = "ok";
            $this->data['message'] = "تم ارسال الكود بنجاح الى الجوال الخاص بك";
            return response()->json($this->data , 200);

        } else {

            foreach ((array)$validator->errors() as $value){

                if (isset($value['mobile'])) {
                    $msg=$validator->errors()->first();
                    $this->data['data'] = "";
                    $this->data['status'] = "ok";
                    $this->data['message'] = $msg;
                    return response()->json($this->data);
                }

            }

        }
    }

    public function postChangePassword(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'code'=>'required',
            'new_password'=>'required|min:6'
        ]);
        if ($validator->passes()) {
            $user = User::where('code',$request->code)->first();
            if(!$user){
                $msg='user cannot be found' ;
                return response()->json([
                    'data'=>null,
                    'status'=>'validation_errors',
                    'message'=>$msg
                ]);
            }
            $user->password = bcrypt($request['new_password']);
            $user->code = null ;
            $user->update() ;

            $msg = 'تم تغيير كلمه المرور بنجاح';
            $this->data['data'] = new SingleUser($user);
            $this->data['status'] = "ok";
            $this->data['message'] = $msg;
            return response()->json($this->data , 200);

        } else {

            foreach ((array)$validator->errors() as $value){

                if (isset($value['new_password'])) {
                    $msg=$validator->errors()->first();
                    $this->data['data'] = "";
                    $this->data['status'] = "validation_errors";
                    $this->data['message'] = $msg;
                    return response()->json($this->data);
                }elseif (isset($value['code'])) {
                    $msg=$validator->errors()->first();
                    $this->data['data'] = "";
                    $this->data['status'] = "validation_errors";
                    $this->data['message'] = $msg;
                    return response()->json($this->data);
                }

                else{
                    $msg='لم يتم تغير كلمه المرور' ;
                    $this->data['data'] = "";
                    $this->data['status'] = "validation_errors";
                    $this->data['message'] = $msg;
                    return response()->json($this->data);
                }

            }

        }
    }

    public function cities()
    {
            $name = 'name_'.app()->getLocale();
            $cities = City::all();
            $cities_data=[];
            foreach ($cities as $citie) {
                $cities_data[]=[
                    'id'=>$citie->id,
                    'name'=>$citie->$name,
                ];
            }
    
            $this->data['data'] = $cities_data;
            $this->data['status'] = "ok";
            $this->data['message'] = '';
            return response()->json($this->data, 200);
        }

}
