<?php

namespace App\Http\Requests\api;

use App\Http\Requests\REQUEST_API_PARENT;
use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Parent_;

class RegisterRestaurant extends REQUEST_API_PARENT
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ar_name'=>'required|string',
            'en_name'=>'required|string',
            'mobile'=>'required|unique:user,mobile',
            'email'=>'required|email|unique:user,email',
            'password'=>'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'address'=>'required',
            'image'=>'nullable|image',
            'options.*' => 'exists:options,id',
            'categories' => 'required',
            'categories.*' => 'exists:categories,id',
            'payment_id' => 'exists:payments,id',
            'bunch_id' => 'required|exists:bunches,id',
            'city_id' => 'required|exists:city,id',

        ];
    }

    public function messages()
    {
        return [
            'categories.*.exists'  => 'unknown categories ',
            'options.*.exists'  => 'unknown options',
        ];
    }

    public function getValidatorInstance(){
        $data = $this->all();
        $data['categories'] = array_column(json_decode($data['categories']), 'id');
        $data['options'] = array_column(json_decode($data['options']), 'id');
        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }
}
