<?php

namespace App\Http\Requests\api;

//use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\REQUEST_API_PARENT;

class AddNewAd extends REQUEST_API_PARENT

{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|string',
            'description'=>'required|string',
            'year_model'=>'required',
            'price'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'category_id'=>'required|numeric|exists:categories,id',
            'service_id'=>'required|numeric|exists:services,id',
            'images'=>'required'
        ];
    }
}
