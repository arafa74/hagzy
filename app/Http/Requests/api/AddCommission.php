<?php

namespace App\Http\Requests\api;

use App\Http\Requests\REQUEST_API_PARENT;
use Illuminate\Foundation\Http\FormRequest;

class AddCommission extends REQUEST_API_PARENT
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_id'=> 'required|exists:banks,id',
            'account_name'=> 'required',
            'account_number'=> 'required',
            'amount'=> 'required',
            'image'=> 'required',
            'message'=> 'required',
        ];
    }
}
