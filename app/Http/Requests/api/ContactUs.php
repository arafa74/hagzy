<?php

namespace App\Http\Requests\api;

use App\Http\Requests\REQUEST_API_PARENT;

class ContactUs extends REQUEST_API_PARENT
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'name' => 'required',
           'type_id' => 'required|exists:types,id',
            'details' => 'required',
//            'mobile' => 'required',
        //    'title' => 'required',
        ];
    }
}
