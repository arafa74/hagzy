<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OfferDelete extends REQUEST_API_PARENT
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offer_id'=>'required|numeric|exists:offers,id',
        ];
    }
}
