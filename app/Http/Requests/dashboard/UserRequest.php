<?php

namespace App\Http\Requests\dashboard;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return array();
                }
            case 'POST':
                {
                    return array(
                        'full_name'=>'required',
                        'email'=>'required|email|unique:user',
                        'mobile'=>'required|min:9|unique:user',
                        'image'=>'required|mimes:jpeg,bmp,png,gif,jpg|max:5000',
//                        'city_id'=>'required|numeric',
                        'password'=>'required',

                    );
                }
            case 'PUT':
                {
                    return array(
                        'full_name'=>'required',
                        'email'=>'required|email',
                        'mobile'=>'required|min:9',
                        'image'=>'mimes:jpeg,bmp,png,gif,jpg|max:5000',
//                        'city_id'=>'required|numeric',
//                        'password'=>'confirmed',
                    );
                }
            case 'PATCH':

        }
    }
}
