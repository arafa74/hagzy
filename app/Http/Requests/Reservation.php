<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Reservation extends REQUEST_API_PARENT
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'clientsNum'=>'required',
            'childrenNum'=>'required',
            'reservation_day'=>'required',
            'reservation_time'=>'required',
            'name'=>'required',
            'mobile'=>'required',
            'email'=>'required',
            'restaurant_id'=>'required|numeric|exists:user,id',
            // 'message'=>'required',
        ];
    }
}
