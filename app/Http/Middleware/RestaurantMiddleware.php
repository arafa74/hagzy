<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class RestaurantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && auth()->user()->type == 'restaurant') {
            return $next($request);
        }
        return redirect(route('restaurant_login'))->with('class', 'alert alert-warning')->with('message', trans('dash.login_firstly'));
    }
}
