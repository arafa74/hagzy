<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContactUs extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name='name_'.app()->getLocale();
        return [
            'id'=>$this->id,
//            'name'=>$this->name,
//            'mobile'=>$this->mobile,
           'type'=>$this->type->$name,
        //    'title'=>$this->title,
            'details'=>$this->details,
        ];
    }
}
