<?php

namespace App\Http\Resources;

//use App\Category;
use App\Http\Resources\api\UserCategory;
use App\UserOption;
use Illuminate\Http\Resources\Json\JsonResource;

class Restaurant extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'ar_name'=>$this->first_name?$this->first_name:"",
            'en_name'=>$this->last_name?$this->last_name:"",
            'social_id'=>$this->social_id?$this->social_id:"",
            'social_type'=>$this->social_type?$this->social_type:"",
            'email'=>$this->email,
            'mobile'=>$this->mobile,
            'image'=>$this->Imageurl,
            'type'=>$this->type,
            'jwt'=>$this->token->jwt,
            'lat'=>$this->latitude,
            'lng'=>$this->latitude,
            'address'=>$this->address,
            'wait_time'=>$this->bref?$this->bref:" ",
            'extras'=>$this->extras?$this->extras:" ",
            'work_time'=>$this->work_time?$this->work_time:" ",
            'categories' => UserCategory::collection($this->category),
            'options' => \App\Http\Resources\api\UserOption::collection($this->option),
        ];
    }
}
