<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReservationsDetails extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'user_id' => $this->user->id,
            'user_name' => $this->user->full_name,
            'restaurant_ar_name'=>$this->restaurant->first_name,
            'restaurant_en_name'=>$this->restaurant->last_name,
            'restaurant_id' => (int) $this->restaurant_id,
            'clientNum' => $this->clientsNum,
            'childrenNum' => $this->childrenNum?$this->childrenNum:0 ,
            'reservation_day' => $this->reservation_day,
            'reservation_time' => $this->reservation_time,
            'message' => $this->message?$this->message:" ",
            'name' => $this->name?$this->name:$this->user->full_name,
            'email' => $this->email?$this->email:$this->user->email,
            'mobile' => $this->mobile?$this->mobile:$this->user->mobile,
            'code' => $this->code,
            'status' => $this->status,
        ];
    }
}
