<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DishResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name='name_'.app()->getLocale();
        return [
            'id' => $this->id,
            'name'=> $this->name,
            'description' => $this->description,
            'price'=> $this->price,
            'category_id' => $this->category_id,
            'category_name' => $this->category->$name,
            'image'=> $this->Imageurl,
            'created_at'=> date('d-m-Y', strtotime($this->created_at))
            ,
        ];
    }
}
