<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use URL;
class ImageResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
                'id'=>$this->id,
                'Image'=>URL::to('storage/app/ad_images/org').'/'.$this->image
             ];
    }
}
