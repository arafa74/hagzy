<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'conversation_id' => $this->room_id,
            'message_type' => $this->type,
            'message_position' => $this->sender_id == $request->user()->id ? 'current' : 'other',
            'sender_data' => [
                'id' => $this->sender_id,
                'username' => $this->Sender->full_name,
                'profile_image' => $this->Sender->Imageurl,
            ],
            'message' => $this->message_value,
            'created_at' => $this->created_at->diffforhumans(),
        ];
    }
}
