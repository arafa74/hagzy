<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class offerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $disc = (int) $this->discount;
        return [
            'id' => $this->id,
            'user_id'=> $this->user_id,
            'description' => $this->description,
            'name'=> $this->name?$this->name:"",
            'discount'=> $this->discount,
            'price'=>(int) $this->price,
            'priceAfterDiscount'=> $this->price - ($disc * $this->price)/100,
            'personNum' => $this->personNum,
            'to' => $this->to,
            'from' => $this->from,
            'image'=> $this->Imageurl,
            'created_at'=> date('d-m-Y', strtotime($this->created_at))
            ,
        ];
    }
}
