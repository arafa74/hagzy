<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SingleUser extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'=>$this->id,
            'name'=>$this->full_name,
            'mobile'=>$this->mobile,
            'email'=>$this->email,
            'image'=>$this->Imageurl,
            'type'=>$this->type,
            'jwt'=>$this->token->jwt,
        ];
    }
}
