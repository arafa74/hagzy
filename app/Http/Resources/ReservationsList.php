<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Rate;
class ReservationsList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $rate_avg = Rate::where( 'restaurant_id' , $this->restaurant->id )->avg('rate');
        return [
                'id' => $this->id,
                'restaurant_id' => $this->restaurant_id,
                'restaurant_ar_name'=>$this->restaurant->first_name,
                'restaurant_en_name'=>$this->restaurant->last_name,
                'restaurant_lat'=>$this->restaurant->latitude?$this->restaurant->latitude:"",
                'restaurant_lng'=>$this->restaurant->longitude?$this->restaurant->longitude:"",
                'user_name' => $this->user->full_name,
                'mobile' => $this->mobile?$this->mobile:$this->user->mobile,
                'restaurant_avatar' => $this->restaurant->image_url,
                'user_avatar' => $this->user->image_url,
                'code' => $this->code,
                'clientsNum' => $this->clientsNum,
                'childrenNum' => $this->childrenNum ? $this->childrenNum : 0,
                'reservation_date' => $this->reservation_day,
                'reservation_time' => $this->reservation_time,
                'status' => $this->status,
                'rate_avg' => $rate_avg?(int)$rate_avg:0,
        ];
    }
}
