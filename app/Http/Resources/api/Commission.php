<?php

namespace App\Http\Resources\api;
use URL;
use Illuminate\Http\Resources\Json\JsonResource;

class Commission extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' =>$this->id,
          'bank_id' =>$this->bank_id,
          'bank_name' =>$this->bank->name,
          'account_number' =>$this->account_number,
          'account_name' =>$this->account_name,
          'amount' =>$this->amount,
            'Image'=>URL::to('storage/app/commissions/50x50').'/'.$this->image
        ];
//        return parent::toArray($request);
    }
}
