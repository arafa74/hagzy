<?php

namespace App\Http\Resources;

use App\Category;
use App\Rate;
use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use App\Room;
use App\State;
use App\Car_state;
use App\Jop_type;
use App\Oli;
use App\Mover;
use App\Service;
use JWTAuth;
use Carbon\Carbon;
use Tymon\JWTAuth\Exceptions\JWTException;

class AdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $start = Carbon::parse($this->start_date);
        $end = Carbon::parse($this->end_date);
        $days_count =$start ->diffInDays($end);
        $rate_avg = Rate::where('ad_id' , $this->id)->avg('rate');
        $name='name_'.app()->getLocale();
        return [
                'id'        =>$this->id,
                'user_id'        =>$this->user->id,
                'user_name'        =>$this->user->full_name,
                'mobile'        =>$this->user->mobile,
                'lat'        =>$this->lat?$this->lat : "",
                'lng'        =>$this->lng?$this->lng : "",
                'address'        =>$this->address?$this->address : "",
                'city'        =>$this->user->city->$name,
                'service_id'        =>Service::where('id'  , $this->service_id)->first()->id,
                'category_id'        =>Category::where('id'  , $this->category_id)->first()->id,
                'title'      => $this->title,
                'description'      => $this->description,
                'year_model' => $this->year_model,
                'price'   => $this->price,
                'days_count'     => $days_count,
                'rate_avg'     => (int) $rate_avg,

                'images' => ImageResource::collection($this->ad_image),
            ];
    }
}
