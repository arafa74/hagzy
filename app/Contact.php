<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $fillable = [
        'title',
        'details',
        'type_id',
    ];


    
    public function type()
    {
        return $this->belongsTo(Type::class);
    }
}
