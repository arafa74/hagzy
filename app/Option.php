<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'options';

    public function car()
    {
        return $this->belongsToMany(Car::class);
    }

}
