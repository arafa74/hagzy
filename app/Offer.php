<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $table ='offers';
    protected $fillable =['user_id' , 'description' ,'image', 'name' , 'price', 'discount' , 'personNum' , 'from' , 'to'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getImageurlAttribute()
    {
        $image = Offer::where('id', $this->id)->first()->image;
        if (!$image) {
            return url('storage/app/offers/default.png');
        }
        return url('storage/app/offers/org') . '/' . $this->attributes['image'];
    }
    public function getImageurlorgAttribute()
    {
        $image = Offer::where('id', $this->id)->first()->image;
        if (!$image) {
            return url('storage/app/offers/default.png');
        }
        return url('storage/app/offers/org') . '/' . $this->attributes['image'];
    }
}
