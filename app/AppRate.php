<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppRate extends Model
{
    protected $table = 'app_rates';
    protected $fillable = ['user_id' , 'comment' , 'rate'];
}
