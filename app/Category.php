<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $appends = ['imageurl', 'imageurlorg'];

    public function getImageurlAttribute()
    {
        if (!$this->attributes['image']) {
            return url('storage/app/categories/default.png');
        }
        return url('storage/app/categories/org') . '/' . $this->attributes['image'];
    }
    public function getImageurlorgAttribute()
    {
        if (!$this->attributes['image']) {
            return url('storage/app/categories/default.png');
        }
        return url('storage/app/categories/org') . '/' . $this->attributes['image'];
    }

    public function dish(){
        return $this->hasMany(Dish::class,'category_id');
    }

}
