<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOption extends Model
{
    protected $table = 'user_options';
    protected $fillable = ['option_id' , 'user_id'];

    public function option(){
        return $this->belongsTo(Option::class);
    }

}
