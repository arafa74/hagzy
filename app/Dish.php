<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $table = 'dishes';

    protected $fillable = ['user_id' , 'name' , 'image' , 'description' , 'price' , 'category_id'];

    public function getImageurlAttribute()
    {
        $image = Dish::where('id', $this->id)->first()->image;
        if (!$image) {
            return url('storage/app/dishes/default.png');
        }
        return url('storage/app/dishes/org') . '/' . $this->attributes['image'];
    }
    public function getImageurlorgAttribute()
    {
        $image = Dish::where('id', $this->id)->first()->image;
        if (!$image) {
            return url('storage/app/dishes/default.png');
        }
        return url('storage/app/dishes/org') . '/' . $this->attributes['image'];
    }


    public function user(){
        return $this->belongsTo(User::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
}
