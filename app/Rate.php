<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rates';

    protected $fillable = [
        'user_id' , 'restaurant_id' , 'comment' , 'rate'
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }

}
