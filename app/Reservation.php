<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'reservations';
    protected $fillable = [
        'user_id' ,
        'clientsNum' ,
        'childrenNum' ,
        'reservation_day' ,
        'reservation_time' ,
        'restaurant_id' ,
        'name' ,
        'mobile' ,
        'email' ,
        'message' ,
        'status',
        'code'
    ];

    public function user()
    {
        return $this->belongsTo(User::class ,'user_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(User::class , 'restaurant_id');
    }
}
