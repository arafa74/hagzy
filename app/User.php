<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use App\Model\City;
use App\Model\Advertise;
use App\Model\Follow;
use DB;

class User extends Authenticatable
{
    // use Notifiable;

    protected $table = "user";
    protected $fillable = [
        'administration_group_id',
        'administration_group_id',
        'email',
        'mobile',
        'type',
        'identitity_number',
        'refeer_code',
        'full_name',
        'password',
        'latitude',
        'longitude',
        'gender',
        'lang',
        'active',
        'code',
        'num_try_active',
        'banned',
        'ban_reason',
        'online',
        'image',
        'city_id',
        'nationality_id',
        'first_name',
        'last_name',
        'date_of_birth',
        'available',
        'wallet',
        'total_collection_money',
        'address',
        'bio',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($user) {
            $user->uuid = Str::uuid();
        });
    }
    public function token()
    {
        return $this->hasOne('App\Model\Token');
    }

    public function admin_group()
    {
        if ($this->attributes['administration_group_id'] != null) {
            return $this->belongsTo('App\Model\AdministrationGroup', 'administration_group_id');
        }
        return false;
    }

    public function getImageurlAttribute()
    {
        $image = User::where('id', $this->id)->first()->image;
        if (!$image) {
            return url('storage/app/users/default.png');
        }
        return url('storage/app/users/org') . '/' . $this->attributes['image'];
    }
    public function getImageurlorgAttribute()
    {
        $image = User::where('id', $this->id)->first()->image;
        if (!$image) {
            return url('storage/app/users/default.png');
        }
        return url('storage/app/users/org') . '/' . $this->attributes['image'];
    }

    public function notifications()
    {
        return $this->hasMany('App\Model\Notification');
    }

    public function city(){
        return $this->belongsTo(City::class,'city_id');
    }

    public function like(){
        return $this->hasMany(Like::class);
    }

//    public function like(){
//        return $this->hasMany(Like::class);
//    }

    public function rates(){
        return $this->hasMany(Rate::class);
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'user_categories', 'user_id', 'category_id')->withTimestamps();
    }

    public function option()
    {
        return $this->belongsToMany(Option::class, 'user_options', 'user_id', 'option_id')->withTimestamps();
    }

    public function dish(){
        return $this->hasMany(Dish::class);
    }

    public function reservation(){
        return $this->hasMany(Reservation::class);
    }

    public function rate(){
        return $this->hasMany(Rate::class);
    }

    public function offer(){
        return $this->hasMany(Offer::class);
    }
    
    public function scopeNearest($query, $lat, $lng, $space_search_by_kilos)
    {
        $lat = (float)$lat;
        $lng = (float)$lng;
        $space_search_by_kilos = (double)$space_search_by_kilos;
        return $query->select(DB::raw("*,
            (6378.10 * ACOS(COS(RADIANS($lat))
            * COS(RADIANS(latitude))
            * COS(RADIANS($lng) - RADIANS(longitude))
            + SIN(RADIANS($lat))
            * SIN(RADIANS(latitude)))) AS distance"))
            ->having('distance', '<=', $space_search_by_kilos)
            ->orderBy('distance', 'asc');
    }

}
