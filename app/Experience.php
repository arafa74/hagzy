<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $table = 'experiences';

    protected $fillable = ['name_ar','name_en','points','color' , 'likes', 'reservations', 'rates'];
}
